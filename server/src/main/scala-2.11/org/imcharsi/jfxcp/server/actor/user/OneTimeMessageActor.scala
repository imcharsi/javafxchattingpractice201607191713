/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.user

import akka.actor.{ Actor, ActorRef, Terminated }
import akka.pattern.ask
import org.imcharsi.jfxcp.server.actor.auth.IdLookupActor

import scala.collection.immutable.Queue
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.util.Success

/**
 * Created by i on 7/23/16.
 */
class OneTimeMessageActor(idLookupActorRef: ActorRef, userWorkerActorRef: ActorRef, userNo: Int, userId: String) extends Actor {
  // 아래의 map 는 사실상 queue 처럼 쓴다.
  // web socket actor 가 갱신될 때만 queue 의 내용을 처음부터 끝까지 순서대로 web socket actor 로 전달하고
  // 그 외에 web socket actor 가 현재 있는 상태에서는 추가로 오는 내용만 web socket actor 로 전달하고 queue 끝에 쌓기만 한다.
  // web socket actor 로부터 응답을 받으면 받은 응답에 해당하는 내용을 queue 에서 지운다.
  // 항상 순서에 맞게 응답이 온다고 가정하면 안 된다.
  // 따라서, 응답을 받을 때마다 쌓여있던 전체 message 중에서 받은 응답에 해당하는 message 만 지워야 한다.
  // 그래서, Queue 가 아니라 LinkedHashMap 을 쓴다.
  // http://www.scala-lang.org/files/archive/api/2.11.8/#scala.collection.mutable.LinkedHashMap
  // 정의에 따라, LinkedHashMap 은 hash map 이면서 iterate 를 할 때 입력한 순서대로 반복한다.
  // todo user no 가 string hash code 가 아닌 다른 유일한 값을 사용하도록 해야 한다.
  private[actor] val queue: mutable.LinkedHashMap[(Int, Int), (ActorRef, OneTimeMessageActor.OneTimeMessageActorQueueRequest)] = mutable.LinkedHashMap.empty
  private[actor] var messageCounter: Int = 0
  private[actor] var webSocketActorRef: Option[ActorRef] = None

  override def postStop(): Unit = {
    super.postStop()
    webSocketActorRef.foreach(context.unwatch(_))
    webSocketActorRef = None
  }

  def receiveOneTimeMessageCTO(message: OneTimeMessageActor.OneTimeMessageCTO): Unit = {
    // (1)
    // 이와 같이 해야 한다. 아니면 상태에 관해 동기화 문제가 생긴다.
    val allocatedMessageCounter = messageCounter
    messageCounter = messageCounter + 1
    val askActorRef = sender()
    // 여기까지는 동기화 방식으로 실행해야 하고
    // 아래부터는 비동기식으로 해도 된다.
    // 상대방 onetime message actor 로 전달하고 응답을 받는 과정까지 전부 하나로 해야 한다.
    // route 에서 기다리고 있기 때문이다. route 의 ask pattern 인 sender 를 상태로서 달아둘 방법이 없다.
    // 한편, 이 과정 전체의 실행 결과를 상태로서 남겨야 할 필요가 없기 때문에, fire-and-forget 방식으로 해도 된다.
    idLookupActorRef.ask(IdLookupActor.Lookup(message.id))(10.seconds)
      .flatMap {
        case IdLookupActor.LookupResult(Some(targetUserWorkerActorRef)) =>
          context.system.actorSelection(targetUserWorkerActorRef.path / UserWorkerActor.nameOneTimeMessageActor).resolveOne(10.seconds)

      }(Implicits.global)
      .flatMap {
        case targetOneTimeMessageActorRef =>
          targetOneTimeMessageActorRef.ask((self, OneTimeMessageActor.ScheduleOneTimeMessageOTO(userNo, allocatedMessageCounter, userId, message.chat)))(10.seconds) // (2)
      }(Implicits.global)
      .onComplete {
        case Success(OneTimeMessageActor.ScheduledOneTimeMessageOTO(userNo, msgNo)) => // (3)
          askActorRef.tell(OneTimeMessageActor.ScheduledOneTimeMessageOTC(userNo, msgNo), self) // (4)
        case _ =>
          askActorRef.tell(OneTimeMessageActor.FailedOneTimeMessageOTC, self) // (4)
      }(Implicits.global)
    userWorkerActorRef.tell(UserWorkerActor.RefreshTimeout, self)
  }

  // 이 단계는 상대방 OneTimeMessageActor 가 일방 OneTimeMessageActor 로부터 message 를 받았을 때의 처리이다.
  def receiveScheduleOneTimeMessageOTO(talkingOneTimeMessageActorRef: ActorRef, message: OneTimeMessageActor.ScheduleOneTimeMessageOTO): Unit = {
    queue += (((message.userNo, message.msgNo), (talkingOneTimeMessageActorRef, message)))
    sender().tell(OneTimeMessageActor.ScheduledOneTimeMessageOTO(message.userNo, message.msgNo), self) // (3)
    // 만약 web socket actor 가 등록되지 않았다면 아무것도 하지 않고, 할 필요도 없다.
    // 언젠가 web socket actor 가 등록되면 쌓아놨던 message 를 한꺼번에 보내면 되기 때문이다.
    webSocketActorRef.foreach(_.tell(WebSocketActor.QueueOneTimeMessageOTW(message.senderId, message.chat, message.userNo, message.msgNo), self)) // (5)
  }

  def receiveTransferredOneTimeMessageWTO(message: WebSocketActor.TransferredOneTimeMessageWTO): Unit = {
    queue
      .remove(message.userNo, message.msgNo)
      .map(_._1)
      .foreach(_.tell(OneTimeMessageActor.ScheduleOneTimeMessageAckOTO(message.userNo, message.msgNo), self)) // (7)
  }

  def receiveScheduleOneTimeMessageAckOTO(message: OneTimeMessageActor.ScheduleOneTimeMessageAckOTO): Unit = {
    queue += (((message.userNo, message.msgNo), (self, message)))
    webSocketActorRef.foreach(_.tell(WebSocketActor.QueueOneTimeMessageAckOTW(message.userNo, message.msgNo), self)) // (8)
  }

  def receiveTransferredOneTimeMessageAckWTO(message: WebSocketActor.TransferredOneTimeMessageAckWTO): Unit =
    queue.remove(message.userNo, message.msgNo) // (9)

  def batchPush(): Unit = {
    webSocketActorRef
      .foreach(
        _.tell(
          WebSocketActor.BatchQueueToWebSocketActor(
            queue.foldLeft(Queue[WebSocketActor.WebSocketActorQueueRequest]()) {
              case (q, (_, (_, message @ OneTimeMessageActor.ScheduleOneTimeMessageOTO(userNo, msgNo, id, chat)))) => // (5)
                q.enqueue(WebSocketActor.QueueOneTimeMessageOTW(message.senderId, message.chat, message.userNo, message.msgNo))
              case (q, (_, (_, message @ OneTimeMessageActor.ScheduleOneTimeMessageAckOTO(userNo, msgNo)))) => // (5)
                q.enqueue(WebSocketActor.QueueOneTimeMessageAckOTW(message.userNo, message.msgNo))
            }
          ), self
        )
      )
  }

  override def receive: Receive = {
    case a @ OneTimeMessageActor.OneTimeMessageCTO(_, _) =>
      receiveOneTimeMessageCTO(a)
    case (talkingOneTimeMessageActorRef: ActorRef, a @ OneTimeMessageActor.ScheduleOneTimeMessageOTO(_, _, _, _)) =>
      receiveScheduleOneTimeMessageOTO(talkingOneTimeMessageActorRef, a)
    case a @ WebSocketActor.TransferredOneTimeMessageWTO(_, _) =>
      receiveTransferredOneTimeMessageWTO(a)
    case a @ OneTimeMessageActor.ScheduleOneTimeMessageAckOTO(_, _) =>
      receiveScheduleOneTimeMessageAckOTO(a)
    case a @ WebSocketActor.TransferredOneTimeMessageAckWTO(_, _) =>
      receiveTransferredOneTimeMessageAckWTO(a)
    case Terminated(terminated) if webSocketActorRef.contains(terminated) =>
      webSocketActorRef = None
    case UserWorkerActor.BroadcastWebSocketActor(actorRef) =>
      // 새 web socket actor 가 만들어지면 헌 web socket actor 는 지워지게 되는데,
      // 만약 broadcast 가 Terminated 보다 먼저 도착하게 된다면
      // Terminated 의 조건식인 webSocketActorRef 를 바꾸게 되어
      // 헌 web socket actor 에 관한 Terminated 는 처리되지 않게 되고 따라서 dead-letters 로 가게 된다.
      // 따라서, 아래와 같이 unwatch 를 해야 한다.
      webSocketActorRef.foreach(context.unwatch(_))
      context.watch(actorRef)
      webSocketActorRef = Some(actorRef)
      batchPush()
  }
}

object OneTimeMessageActor {

  sealed trait OneTimeMessageActorRequest

  sealed trait OneTimeMessageActorResponse

  sealed trait OneTimeMessageActorQueueRequest

  case class OneTimeMessageCTO(id: String, chat: String) extends OneTimeMessageActorRequest

  // userNo 는, web socket actor 와 onetime message actor 사이에서, 전송완료한 queue 를 지우기 위한 용도로 쓴다.
  // id 는 message 를 보내는 사용자의 id 를 나타낸다. 받는 사용자의 id 를 나타내지 않는다.
  // 중간에 경로설정을 할 필요도 없기 때문에 받는 사용자의 onetime message actor 를 찾은 이후부터는,
  // 받는 사용자의 id 는 더 이상 필요하지 않다.
  case class ScheduleOneTimeMessageOTO(userNo: Int, msgNo: Int, senderId: String, chat: String) extends OneTimeMessageActorRequest with OneTimeMessageActorQueueRequest

  case class ScheduledOneTimeMessageOTO(userNo: Int, msgNo: Int) extends OneTimeMessageActorResponse

  case class ScheduledOneTimeMessageOTC(userNo: Int, msgNo: Int) extends OneTimeMessageActorResponse

  // 상대방 onetime message actor 를 찾는 데 실패했을 때, client 로 보내는 응답이다.
  case object FailedOneTimeMessageOTC extends OneTimeMessageActorResponse

  case class ScheduleOneTimeMessageAckOTO(userNo: Int, msgNo: Int) extends OneTimeMessageActorRequest with OneTimeMessageActorQueueRequest

  /**
   * 아주 복잡한데, 이와 같이 해야 한다. 더 단순한 방법을 못찾겠다.
   * 이와 같은 구성의 의도는, 다음과 같다.
   * 사용자a 가 사용자b 로 일회용 대화 입력을 마친다.
   * 이어서 사용자a 가 보낸 일회용 대화가 사용자b 의 onetime message actor 로 전달됐음을 확인하는 응답을 받는다.
   * 이어서 사용자b 의 onetime message actor 와 web socket actor 가 사용자b 로 일회용 대화를 전달하고 사용자b 로 부터, 전달받았음을 응답을 받는다.
   * 사용자b 의 onetime message actor 는 사용자b 가 일회용 대화를 잘 전달받았음을 사용자a 의 onetime message actor 로 전달한다.
   * 사용자a 의 onetime message actor 와 web socket actor 는 사용자a 에게, 사용자b 에 대한 자신의 일회용 대화 전달이 성공했음을 알린다.
   * 이와 같은 구성에 따르면 사용자a 화면에는 3번의 변화가 있게 되는데,
   * 일회용 대화 입력을 마친 시점에서 자신의 화면에 입력을 마침을 나타내는 표시가 뜬다.
   * 사용자b 의 onetime message actor 로부터 응답을 받았을 때, 일회용 대화의 전송이 준비되었음을 나타내는 표시가 뜬다.
   * 사용자b 가 대화를 전달받았음을 나타내는 표시가 뜬다.
   * client-a ->(1) OTMA-a
   * OTMA-a ->(2) OTMA-b
   * OTMA-b ->(3) OTMA-a
   * OTMA-a ->(4) client-a
   * OTMA-b ->(5) WSA-b
   * WSA-b ->(6) OTMA-b
   * OTMA-b ->(7) OTMA-a
   * OTMA-a ->(8) WSA-a
   * WSA-a ->(9) OTMA-a
   *
   * CTO Client To OneTimeMessageActor
   * OTO OneTimeMessageActor To OneTimeMessageActor
   * OTC OneTimeMessageActor To Client
   * OTW OneTimeMessageActor To WebSocketActor
   * WTO WebSocketActor To OneTimeMessageActor
   *
   * (1) OneTimeMessageActor.OneTimeMessageCTO
   * (2) OneTimeMessageActor.ScheduleOneTimeMessageOTO
   * (3) OneTimeMessageActor.ScheduledOneTimeMessageOTO
   * (4) OneTimeMessageActor.ScheduledOneTimeMessageOTC
   * (5) WebSocketActor.QueueOneTimeMessageOTW
   * (6) WebSocketActor.TransferredOneTimeMessageWTO
   * (7) OneTimeMessageActor.ScheduleOneTimeMessageAckOTO
   * (8) WebSocketActor.QueueOneTimeMessageAckOTW
   * (9) WebSocketActor.TransferredOneTimeMessageAckWTO
   */
}
