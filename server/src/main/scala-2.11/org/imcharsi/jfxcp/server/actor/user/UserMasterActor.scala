/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.user

import akka.actor.{ Actor, ActorRef, Props }
import org.imcharsi.jfxcp.server.actor.user.UserMasterActor.CreatedUserWorkerActor

import scala.concurrent.duration.FiniteDuration
import scala.util.Try

/**
 * Created by i on 7/23/16.
 */
class UserMasterActor(authenticateActorRef: ActorRef, idLookupActorRef: ActorRef, roomMasterActorRef: ActorRef, beforeInitTimeout: FiniteDuration, afterInitTimeout: FiniteDuration, firstTimeout: FiniteDuration) extends Actor {
  override def receive: Receive = {
    case a @ UserMasterActor.CreateUserWorkerActor(sessionKey) =>
      Try(context.actorOf(Props(new UserWorkerActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, beforeInitTimeout, afterInitTimeout, firstTimeout)), sessionKey.toString))
        .map(actorRef => CreatedUserWorkerActor(Some(actorRef)))
        .recover { case _ => CreatedUserWorkerActor(None) }
        .foreach { resultMessage => sender().tell(resultMessage, self) }
  }
}

object UserMasterActor {

  sealed trait UserMasterActorRequest

  sealed trait UserMasterActorResponse

  case class CreateUserWorkerActor(sessionKey: Long) extends UserMasterActorRequest

  case class CreatedUserWorkerActor(actorRef: Option[ActorRef]) extends UserMasterActorResponse

}
