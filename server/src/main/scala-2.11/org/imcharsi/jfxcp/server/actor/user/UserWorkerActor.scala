/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.user

import akka.actor._
import akka.pattern.ask
import akka.pattern.pipe
import akka.routing.{ BroadcastRoutingLogic, Router }
import org.imcharsi.jfxcp.common.roommaster.RoomMaster
import org.imcharsi.jfxcp.server.actor.auth.AuthenticateActor
import org.imcharsi.jfxcp.server.actor.room.RoomAttendingMasterActor

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.util.Try

/**
 * Created by i on 7/23/16.
 */
class UserWorkerActor(
    authenticateActorRef: ActorRef,
    idLookupActorRef:     ActorRef,
    roomMasterActorRef:   ActorRef,
    beforeInitTimeout:    FiniteDuration,
    afterInitTimeout:     FiniteDuration,
    firstTimeout:         FiniteDuration
) extends Actor {
  // 인증 단계에서만 쓸 last sender 이다.
  private[actor] var lastSenderWhileAuthenticate: Option[ActorRef] = None
  private[actor] var oneTimeMessageActorRef: Option[ActorRef] = None
  private[actor] var roomAttendingMasterActorRef: Option[ActorRef] = None
  private[actor] var webSocketActorRef: Option[ActorRef] = None
  private[actor] var userNo: Option[Int] = None
  private[actor] var broadcaster: Router = Router(BroadcastRoutingLogic())
  // 그 외에, 접속해 있는 동안 바뀌지 않을 몇가지 자료를 미리 달고 있으면 유용하다.
  // 생각나는대로 넣기.
  private[actor] var userId: Option[String] = None
  private[actor] var cancellableTimeout: Option[Cancellable] = None

  // 종료는 따로 신경쓸 필요없다. 대화방 퇴장의 경우 room attending worker actor 에서 종료처리를 하면 되고
  // 그 외 나머지는 따로 할 것이 없다. 또한 각 관련 actor 는 전부 계층을 이루고 있기 때문에 parent 가 종료하면 children 은 전부 따라서 종료한다.
  // 또한 인증해제는, authenticate actor 에서 user worker actor 를 watch 해서 하기.

  private def scheduleTimeout(timeout: FiniteDuration): Unit = {
    cancellableTimeout.foreach(_.cancel())
    cancellableTimeout = Some(context.system.scheduler.scheduleOnce(timeout, self, UserWorkerActor.InactivityTimeout)(Implicits.global))
  }

  override def preStart(): Unit = {
    super.preStart()
    scheduleTimeout(beforeInitTimeout)
  }

  override def postStop(): Unit = {
    super.postStop()
    cancellableTimeout.foreach(_.cancel())
    cancellableTimeout = None
  }

  def receiveAuthenticateRequest: Receive = {
    case a @ UserWorkerActor.Authenticate(id, password, force) =>
      lastSenderWhileAuthenticate = Some(sender())
      userId = Some(id)
      authenticateActorRef
        .tell(AuthenticateActor.Authenticate(id, password, force), self)
      context.become(receiveAuthenticateResponse.orElse(receiveInactivityTimeout))
  }

  def receiveAuthenticateResponse: Receive = {
    case a @ AuthenticateActor.Authenticated(Some(userNo)) =>
      lastSenderWhileAuthenticate.foreach(_.tell(UserWorkerActor.Authenticated(true), self))
      lastSenderWhileAuthenticate = None
      this.userNo = Some(userNo)
      context.become(receiveInitRequestResponse.orElse(receiveInactivityTimeout))
    case a @ AuthenticateActor.Authenticated(None) =>
      lastSenderWhileAuthenticate.foreach(_.tell(UserWorkerActor.Authenticated(false), self))
      lastSenderWhileAuthenticate = None
      userId = None
      context.stop(self)
  }

  def receiveInitRequestResponse: Receive = {
    case a @ UserWorkerActor.Init =>
      // actor 이름을 너무 짧게 썼는데, 상관없다. 기계가 경로를 찾는 데 걸리는 시간을 줄이기 위함이다.
      // 이름은 상수로 쓰면 된다.
      val tryOneTimeMessageActorRef =
        userNo
          .flatMap(userNo => userId.map(userId => Props(new OneTimeMessageActor(idLookupActorRef, self, userNo, userId))))
          .flatMap(props => Try(context.actorOf(props, UserWorkerActor.nameOneTimeMessageActor)).toOption)
      val tryRoomAttendingMasterActorRef =
        userId
          .map(userId => Props(new RoomAttendingMasterActor(roomMasterActorRef, self, firstTimeout, userId)))
          .flatMap(props => Try(context.actorOf(props, UserWorkerActor.nameRoomAttendingMasterActor)).toOption)

      (tryOneTimeMessageActorRef, tryRoomAttendingMasterActorRef) match {
        case (a @ Some(newOneTimeMessageActorRef), b @ Some(newRoomAttendingMasterActorRef)) =>
          oneTimeMessageActorRef = a
          roomAttendingMasterActorRef = b
          sender().tell(UserWorkerActor.Inited(true), self)
          scheduleTimeout(afterInitTimeout)
          context.become(receiveMain)
          // onetime message actor 는 web socket actor 가 만들어졌을 때
          // broadcast 로 web socket actor ref 를 구할 뿐 직접 구하지 않는다.
          // onetime message actor 는, 갱신된 web socket actor ref 를 받을 때마다 여태까지 쌓아뒀던 message 를
          // 전부 다시 보내고, 이어서 오는 message 는 쌓으면서 web socket actor 로 하나씩 보낸다.
          // web socket actor 는, message 를 하나씩 보내고 응답을 받으면 받은 응답을 onetime message actor 로 보내고,
          // onetime message actor 는 응답을 받으면 쌓아뒀던 message 중 받은 응답에 대응하는 message 를 지운다.
          // onetime message actor 에서 쌓아두는 message 는 web socket actor 를 통해서 보낸 message 이고, client 가 잘 받았음을 확인해야 하는 message 이다.
          // room attending master actor 가 web socket actor 가 바뀌었음을 알림받으면
          // room attending master actor 가 다시 children 으로 web socket actor 가 바뀌었음을 알려준다.
          // room attending worker actor 가 만들어지는 시점에서는 room attending master actor 가 알고 있는 web socket actor ref 를 알려준다.
          broadcaster = broadcaster
            .addRoutee(newOneTimeMessageActorRef)
            .addRoutee(newRoomAttendingMasterActorRef)
        case (Some(actorRef), None) =>
          context.stop(actorRef)
          sender().tell(UserWorkerActor.Inited(false), self)
          context.stop(self)
        case (None, Some(actorRef)) =>
          context.stop(actorRef)
          sender().tell(UserWorkerActor.Inited(false), self)
          context.stop(self)
        case (None, None) =>
          sender().tell(UserWorkerActor.Inited(false), self)
          context.stop(self)
      }
  }

  def receiveCreateWebSocketActorRequestResponse: Receive = {
    case a @ UserWorkerActor.CreateWebSocketActor =>
      oneTimeMessageActorRef.flatMap { x =>
        roomAttendingMasterActorRef.flatMap { y =>
          Try(context.actorOf(Props(new WebSocketActor(x, y, firstTimeout)))).toOption
        }
      } match {
        case Some(actorRef) =>
          // 새 web socket actor 를 만들었을 때만 헌 web socket actor 를 종료한다.
          webSocketActorRef.foreach { actorRef => context.stop(actorRef) }
          webSocketActorRef = Some(actorRef)
          sender().tell(UserWorkerActor.CreatedWebSocketActor(webSocketActorRef), self)
          if (!broadcaster.routees.isEmpty)
            broadcaster.route(UserWorkerActor.BroadcastWebSocketActor(actorRef), self)
        case None =>
          // web socket actor 는 만들기를 실패해도, user worker actor 를 종료하지 않는다.
          sender().tell(UserWorkerActor.CreatedWebSocketActor(None), self)
      }
  }

  // 대화방에 참석하는 주체는 room attending worker actor 로 하기.
  // route 에서 user worker actor 로 room attending master actor 를 구한 뒤,
  // 대화방에 참석할 것을 요청하여 처리 결과를 참/거짓으로 알려주면 된다. actor ref 를 route 단계로 알려줄 필요는 없는데, 쓸 일이 없기 때문이다.
  // 대화를 전달할 때는, /user/roomAttending/1 과 같은 경로로 post 로 전달한다.
  // 한편, 대화방 참석은, web socket actor 가 종료해도 퇴장하지 않고 남겨두는 것으로 하기.
  // web socket actor 가 종료한 뒤 쌓여있던 대화내용은 재접속할 때 한꺼번에 보이도록 하기.
  // 이와 같이 중간에 web socket 이 짤렸다가 다시 접속했을 때는 사용자 화면에서는
  // 대화방 재입장 같은 절차를 거치지 않아야 하고, 그럴 필요도 없다.
  // 이 기능은 어느 정도 연습이 된 듯 하다.

  def receiveWebSocketActorTerminated: Receive = {
    case a @ Terminated(terminated) if webSocketActorRef.contains(terminated) =>
      webSocketActorRef = None
  }

  def receiveCreateRoomCTS: Receive = {
    case a @ RoomMaster.CreateRoomCTS(_, _) =>
      userId.foreach { userId =>
        pipe(roomMasterActorRef.ask(a.copy(userId))(10.seconds))(Implicits.global).to(sender())
      }
  }

  def receiveInactivityTimeout: Receive = {
    case UserWorkerActor.InactivityTimeout => context.stop(self)
    case UserWorkerActor.RefreshTimeout => scheduleTimeout(afterInitTimeout)
  }

  def receiveMain: Receive =
    receiveCreateWebSocketActorRequestResponse
      .orElse(receiveWebSocketActorTerminated)
      .orElse(receiveCreateRoomCTS)
      .orElse(receiveInactivityTimeout)

  override def receive: Receive = receiveAuthenticateRequest.orElse(receiveInactivityTimeout)
}

object UserWorkerActor {

  sealed trait UserWorkerActorRequest

  sealed trait UserWorkerActorResponse

  case class Authenticate(id: String, password: String, force: Boolean) extends UserWorkerActorRequest

  case class Authenticated(result: Boolean) extends UserWorkerActorResponse

  case object Init extends UserWorkerActorRequest

  case class Inited(result: Boolean) extends UserWorkerActorRequest

  case object CreateWebSocketActor extends UserWorkerActorRequest

  case class CreatedWebSocketActor(webSocketActorRef: Option[ActorRef]) extends UserWorkerActorRequest

  // impo FindOneTimeMessageActorRef/FoundOneTimeMessageActorRef 이 기능은 만들 때는 인식하지 못했지만 필요가 없다.
  // 자세한 설명은 TestOneTimeMessageActor.about OneTimeMessageCTO 에 있다.

  case class BroadcastWebSocketActor(actorRef: ActorRef) extends UserWorkerActorResponse

  case object InactivityTimeout extends UserWorkerActorRequest

  case object RefreshTimeout extends UserWorkerActorRequest

  // onetime message actor/room attending master actor 는 user worker actor 로 물어서 찾지 않고
  // 직접 위치를 만들어서 찾는다. 각 actor 의 이름을 상수로 정했다. user worker actor ref 만 selection/resolve 로 찾은 뒤 이 경로에 상수를 더해서 찾으면 된다.

  val nameOneTimeMessageActor = "otma"
  val nameRoomAttendingMasterActor = "rama"
}
