/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.route

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.model.HttpEntity.ChunkStreamPart
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ ExceptionHandler, Route }
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.stream.actor.{ ActorPublisher, ActorSubscriber }
import akka.stream.scaladsl.{ Flow, Sink, Source }
import akka.util.ByteString
import com.softwaremill.session.SessionDirectives._
import com.softwaremill.session.SessionOptions._
import com.softwaremill.session._
import org.imcharsi.jfxcp.common.onetimemessage.OneTimeMessage
import org.imcharsi.jfxcp.common.onetimemessage.OneTimeMessage._
import org.imcharsi.jfxcp.common.room.Room
import org.imcharsi.jfxcp.common.roomattending.RoomAttending
import org.imcharsi.jfxcp.common.roomattending.RoomAttending.RoomAttendingRequestCTS
import org.imcharsi.jfxcp.common.roomattendingmaster.RoomAttendingMaster
import org.imcharsi.jfxcp.common.roomattendingmaster.RoomAttendingMaster.RoomAttendingMasterRequestCTS
import org.imcharsi.jfxcp.common.roommaster.RoomMaster
import org.imcharsi.jfxcp.common.roommaster.RoomMaster.RoomMasterRequestCTS
import org.imcharsi.jfxcp.server.actor.user.{ OneTimeMessageActor, UserMasterActor, UserWorkerActor }
import spray.json._

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.Random

/**
 * Created by i on 7/20/16.
 */
object RouteTemplate {
  def tryLogin(userMasterActorRef: ActorRef, sessionKey: Long, id: String, password: String, force: Boolean): Future[HttpResponse] = {
    // 전체 과정이 복잡한데, 비동기성을 유지하면서 더 단순하게 하는 방법 없나.
    // user master actor 에게, user worker actor 를 만들도록 요청한다.
    userMasterActorRef.ask(UserMasterActor.CreateUserWorkerActor(sessionKey))(10.seconds)
      .flatMap {
        case UserMasterActor.CreatedUserWorkerActor(Some(userWorkerActorRef)) =>
          userWorkerActorRef.ask(UserWorkerActor.Authenticate(id, password, force))(10.seconds)
            .flatMap {
              // 인증결과가 참이면
              case UserWorkerActor.Authenticated(true) =>
                // user worker actor 가 초기화를 하도록 요청한다.
                userWorkerActorRef
                  .ask(UserWorkerActor.Init)(10.seconds)
                  .map {
                    // 초기화결과도 참이면
                    case UserWorkerActor.Inited(true) => HttpResponse(StatusCodes.OK)
                  }(Implicits.global)
              // 아래의 경우를 명시적으로 구분하는 이유는 단순히 인증을 실패했을 때와 인증이 아닌 다른 이유로 실패했을 때를 구분하기 위해서이다.
              case UserWorkerActor.Authenticated(false) => Future(HttpResponse(StatusCodes.Unauthorized))(Implicits.global)
            }(Implicits.global)
      }(Implicits.global)
      .recover {
        case _ => HttpResponse(StatusCodes.InternalServerError)
      }(Implicits.global)
  }

  def verifyUserWorkerActor(userMasterActorRef: ActorRef, sessionKey: Long)(implicit actorSystem: ActorSystem): Future[HttpResponse] = {
    // 일부러 예외를 복구하지 않았다. 아래에서 보는 것처럼, 예외가 던져지도록 해야 두번째 route 를 실행할 수 있다.
    actorSystem.actorSelection(userMasterActorRef.path / sessionKey.toString)
      .resolveOne(10.seconds)
      .map(_ => HttpResponse(StatusCodes.OK))(Implicits.global)
  }

  def routeLogin(userMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, sessionManager: SessionManager[Long]): Route =
    path("login") {
      post {
        requiredSession(oneOff, usingCookies) { sessionKey =>
          handleExceptions(
            ExceptionHandler {
              case _: Throwable =>
                extractCredentials {
                  case Some(BasicHttpCredentials(id, password)) =>
                    complete(tryLogin(userMasterActorRef, sessionKey, id, password, false))
                  case _ =>
                    complete(StatusCodes.Unauthorized)
                }
            }
          )(complete(verifyUserWorkerActor(userMasterActorRef, sessionKey)))
        } ~ provide(Random.nextLong()) { sessionKey =>
          setSession(oneOff, usingCookies, sessionKey) {
            extractCredentials {
              case Some(BasicHttpCredentials(id, password)) =>
                complete(tryLogin(userMasterActorRef, sessionKey, id, password, false))
              case _ =>
                complete(StatusCodes.Unauthorized)
            }
          }
        }
      }
    }

  // 처음 구상은 HttpResponse 가 결과였는데, 검사하기가 불편해서 Flow 가 결과가 되도록 했다.
  def tryWebSocket(userMasterActorRef: ActorRef, sessionKey: Long)(implicit actorSystem: ActorSystem): Future[Flow[Message, Message, _]] =
    actorSystem.actorSelection(userMasterActorRef.path / sessionKey.toString)
      .resolveOne(10.seconds)
      // 매번 새로 만들어야 한다. 이번에 들어온 tcp 연결을 앞서 web socket 으로 만들었던 tcp 연결과 잇는 것은 가능하지 않다.
      // 쉽게 말해서 두개의 tcp 연결을 하나로 묶는 것은 가능하지 않다.
      .flatMap(_.ask(UserWorkerActor.CreateWebSocketActor)(10.seconds))(Implicits.global)
      .map {
        case UserWorkerActor.CreatedWebSocketActor(Some(webSocketActorRef)) =>
          Flow.fromSinkAndSource(
            Flow[Message]
              .flatMapConcat {
                // WebSocketActor.scala file 에서도 같은 설명을 썼는데,
                // impo TextMessage.Strict 로 오는 자료와 TextMessage.Streamed 로 오는 자료 각각이 모두 같은 성격의 자료라고 가정한다.
                // 이와 같은 가정에 맞지 않은 용법이 있지 않도록 해야 한다.
                case TextMessage.Strict(string) => Source.single(string)
                case TextMessage.Streamed(stream) => stream
                case _ => Source.empty
              }
              .to(Sink.fromSubscriber(ActorSubscriber[String](webSocketActorRef))),
            Source.fromPublisher(ActorPublisher[Message](webSocketActorRef))
          )
      }(Implicits.global)

  def routeWebSocket(userMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, sessionManager: SessionManager[Long]): Route =
    // 보통의 url 에서는 이와 같이 stream 으로 응답을 만들 수 없다.
    // stream 은 stream 의 끝이 있기 전까지는 계속해서 자료가 오고감을 전제하는데,
    // 보통의 http 에서 요청은 1회성이기 때문이다.
    path("webSocket") {
      requiredSession(oneOff, usingCookies) { sessionKey =>
        extractUpgradeToWebSocket { upgrade =>
          complete(
            tryWebSocket(userMasterActorRef, sessionKey)
              .map(upgrade.handleMessages(_))(Implicits.global)
              .recover { case x => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
          )
        }
      }
    }

  def tryLogout(userMasterActorRef: ActorRef, sessionKey: Long)(implicit actorSystem: ActorSystem): Future[HttpResponse] = {
    actorSystem.actorSelection(userMasterActorRef.path / sessionKey.toString)
      .resolveOne(10.seconds)
      .map {
        case actorRef: ActorRef =>
          // postStop lifecycle 에서 종료처리를 하도록 하기.
          // 쓰겠다는걸 쓰지말라고 강제할 방법은 있어도 그만 쓰겠다는걸 그만 쓰지말라고 강제할 방법은 없다.
          // 그만 쓰겠다고 하면 무조건 그만 쓰도록 하기.
          actorSystem.stop(actorRef)
          HttpResponse(StatusCodes.OK)
        case _ => HttpResponse(StatusCodes.InternalServerError)
      }(Implicits.global)
      .recover { case x => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
  }

  def routeLogout(userMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, sessionManager: SessionManager[Long]): Route =
    path("logout") {
      put {
        session(oneOff, usingCookies) { sessionResult =>
          sessionResult.toOption match {
            case Some(sessionKey) =>
              // 이 부분이 이상하다. UserWorkerActor 가 실제로 종료되었는지 여부에 상관없이 session 을 무조건 지운다.
              // 그런데, 이것을 논하는 것은 크게 의미가 없는 듯하다.
              // 실제로 UserWorkerActor 를 종료하는 작업을 실패했으면 없다고 보면 되기 때문이다.
              // 그보다는 session 은 지워졌는데 UserWorkerActor 는 남아있는 문제에 대한 답을 찾아야 한다.
              // 이 문제는 시간초과로 풀어야 할 듯 하다.
              // http://doc.akka.io/docs/akka/snapshot/scala/scheduler.html
              // setReceiveTimeout 으로 하면 모든 message 에 대해 시간초과가 취소되기 때문에
              // 특정 message 에 대해서만 시간초과취소를 하려면 scheduleOnce 로 해야 한다.
              invalidateSession(oneOff, usingCookies) {
                complete(tryLogout(userMasterActorRef, sessionKey))
              }
            case None =>
              complete(StatusCodes.Unauthorized)
          }
        }
      }
    }

  def tryOneTimeMessage(userMasterActorRef: ActorRef, sessionKey: Long, oneTimeMessageRequestCTO: OneTimeMessageRequestCTO)(implicit actorSystem: ActorSystem, actorMaterializer: ActorMaterializer): Future[HttpResponse] = {
    actorSystem.actorSelection(userMasterActorRef.path / sessionKey.toString / UserWorkerActor.nameOneTimeMessageActor).resolveOne(10.seconds)
      .flatMap {
        case oneTimeMessageActorRef =>
          Future.successful(oneTimeMessageRequestCTO)
            .flatMap {
              case OneTimeMessage.OneTimeMessageCTO(id, chat) =>
                oneTimeMessageActorRef.ask(OneTimeMessageActor.OneTimeMessageCTO(id, chat))(10.seconds)
            }(Implicits.global)
            .map {
              case OneTimeMessageActor.ScheduledOneTimeMessageOTC(userNo, msgNo) =>
                HttpResponse(
                  StatusCodes.OK,
                  entity = HttpEntity(
                    ContentTypes.`application/json`,
                    OneTimeMessage.ScheduledOneTimeMessageOTC(userNo, msgNo)
                    .asInstanceOf[OneTimeMessage.OneTimeMessageResponseOTC]
                    .toJson
                    .prettyPrint
                  )
                )
              case OneTimeMessageActor.FailedOneTimeMessageOTC =>
                HttpResponse(
                  StatusCodes.InternalServerError, // todo 다른 적당한 오류 없나.
                  entity = HttpEntity(
                    ContentTypes.`application/json`,
                    OneTimeMessage.FailedOneTimeMessageOTC
                    .asInstanceOf[OneTimeMessage.OneTimeMessageResponseOTC]
                    .toJson
                    .prettyPrint
                  )
                )
            }(Implicits.global)
        // 아래의 경우, NotFound 가 비슷한 의미인 것 같지만, NotFound 는 해당 url 이 없다는 의미이고
        // 이 경우는 처리를 실패했다는 의미이므로 다른 오류를 써야 한다. 역시 적당한 오류가 필요하다.
        //        case UserWorkerActor.FoundOneTimeMessageActorRef(None) => Future(HttpResponse(StatusCodes.InternalServerError))(Implicits.global)
      }(Implicits.global)
      .recover { case _ => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
  }

  def routeOneTimeMessage(userMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, sessionManager: SessionManager[Long], actorMaterializer: ActorMaterializer): Route = {
    import OneTimeMessage._
    // 이와 같이 경로를 정해도 되는데, 이유는 보내는 사용자를 session 으로 구분할 수 있기 때문이다.
    path("oneTimeMessage") {
      post {
        requiredSession(oneOff, usingCookies) { sessionKey =>
          entity(as[OneTimeMessage.OneTimeMessageRequestCTO]) { oneTimeMessageRequestCTO =>
            complete(tryOneTimeMessage(userMasterActorRef, sessionKey, oneTimeMessageRequestCTO))
          }
        }
      }
    }
  }

  def tryCreateRoom(userMasterActorRef: ActorRef, sessionKey: Long, roomMasterRequestCTS: RoomMasterRequestCTS)(implicit actorSystem: ActorSystem, actorMaterializer: ActorMaterializer): Future[HttpResponse] = {
    actorSystem.actorSelection(userMasterActorRef.path / sessionKey.toString).resolveOne(10.seconds)
      .flatMap { actorRef =>
        roomMasterRequestCTS match {
          case request @ RoomMaster.CreateRoomCTS(_, _) =>
            actorRef.ask(request)(10.seconds)
          case _ =>
            Future.failed(new Exception)
        }
      }(Implicits.global)
      .map {
        case result @ RoomMaster.CreatedRoomSTC(Some(_)) =>
          HttpResponse(
            StatusCodes.OK,
            entity = HttpEntity(
              ContentTypes.`application/json`,
              Source.single(result.asInstanceOf[RoomMaster.RoomMasterResponseSTC].toJson.prettyPrint)
                .map(ByteString(_))
            )
          )
        case result @ RoomMaster.CreatedRoomSTC(_) =>
          HttpResponse(StatusCodes.BadRequest)
      }(Implicits.global)
      .recover { case _ => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
  }

  def routeCreateRoom(userMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, sessionManager: SessionManager[Long], actorMaterializer: ActorMaterializer): Route = {
    import RoomMaster._
    // id lookup actor 를 거쳐서 session-key 로 id 를 찾는 기능을 두는 것이 과연 편한가.
    // user worker actor 가 대화방 개설 명령을 room actor 로 중개하도록 하는 것은 어떤가.
    // 대화방 만들기는 /room 으로 하는 것이 좋겠다.
    path("room") {
      post {
        requiredSession(oneOff, usingCookies) { sessionKey =>
          entity(as[RoomMaster.RoomMasterRequestCTS]) { roomMasterRequestCTS =>
            complete(tryCreateRoom(userMasterActorRef, sessionKey, roomMasterRequestCTS))
          }
        }
      }
    }
  }

  def tryEnterRoom(userMasterActorRef: ActorRef, sessionKey: Long, roomAttendingMasterRequestCTS: RoomAttendingMasterRequestCTS)(implicit actorSystem: ActorSystem): Future[HttpResponse] = {
    actorSystem.actorSelection(userMasterActorRef.path / sessionKey.toString / UserWorkerActor.nameRoomAttendingMasterActor)
      .resolveOne(10.seconds)
      .flatMap { actorRef => actorRef.ask(roomAttendingMasterRequestCTS)(10.seconds) }(Implicits.global)
      .map {
        case result @ RoomAttendingMaster.EnteredRoomSTC(Some(_)) =>
          HttpResponse(
            StatusCodes.OK,
            entity = HttpEntity(
              ContentTypes.`application/json`,
              Source.single(result.asInstanceOf[RoomAttendingMaster.RoomAttendingMasterResponseSTC].toJson.prettyPrint)
                .map(ByteString(_))
            )
          )
        case result @ RoomAttendingMaster.EnteredRoomSTC(_) =>
          HttpResponse(StatusCodes.BadRequest)
      }(Implicits.global)
      .recover { case _ => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
  }

  def routeEnterRoom(userMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, sessionManager: SessionManager[Long], actorMaterializer: ActorMaterializer): Route = {
    import RoomAttendingMaster._
    // 대화방 들어가기는 /roomAttending 으로 들어가고
    // 대화/퇴장은 /roomAttending/m 으로 하는 것이 좋겠다.
    path("roomAttending") {
      post {
        entity(as[RoomAttendingMaster.RoomAttendingMasterRequestCTS]) { roomAttendingMasterRequestCTS =>
          requiredSession(oneOff, usingCookies) { sessionKey =>
            complete(tryEnterRoom(userMasterActorRef, sessionKey, roomAttendingMasterRequestCTS))
          }
        }
      }
    }
  }

  def tryRoomAttendingPingPong(userMasterActorRef: ActorRef, sessionKey: Long, raaId: Int)(implicit actorSystem: ActorSystem): Future[HttpResponse] = {
    // room attending actor 가 있다는 자체가 대화방에 참가중이라는 뜻이다.
    actorSystem.actorSelection(userMasterActorRef.path / sessionKey.toString / UserWorkerActor.nameRoomAttendingMasterActor / raaId.toString)
      .resolveOne(10.seconds)
      .map(_ => HttpResponse(StatusCodes.OK))(Implicits.global)
      .recover { case _ => HttpResponse(StatusCodes.BadRequest) }(Implicits.global)
  }

  def routeRoomAttendingPingPong(userMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, sessionManager: SessionManager[Long], actorMaterializer: ActorMaterializer): Route = {
    path("roomAttending" / IntNumber / "ping") { raaId =>
      get {
        requiredSession(oneOff, usingCookies) { sessionKey =>
          complete(tryRoomAttendingPingPong(userMasterActorRef, sessionKey, raaId))
        }
      }
    }
  }

  def tryVariousRequest(userMasterActorRef: ActorRef, sessionKey: Long, roomAttendingRequestCTS: RoomAttendingRequestCTS, raaId: Int)(implicit actorSystem: ActorSystem): Future[HttpResponse] = {
    actorSystem.actorSelection(userMasterActorRef.path / sessionKey.toString / UserWorkerActor.nameRoomAttendingMasterActor / raaId.toString)
      .resolveOne(10.seconds)
      .flatMap { actorRef =>
        roomAttendingRequestCTS match {
          case request @ RoomAttending.ChatCTS(_) =>
            actorRef.ask(request)(10.seconds)
              .map(_ => HttpResponse(StatusCodes.OK))(Implicits.global)
          case request @ RoomAttending.GrantOperatorCTS(_) =>
            actorRef.ask(request)(10.seconds)
              .map {
                case RoomAttending.GrantedOperatorSTC(true) => HttpResponse(StatusCodes.OK)
                case _ => HttpResponse(StatusCodes.BadRequest)
              }(Implicits.global)
          case request @ RoomAttending.RevokeOperatorCTS(_) =>
            actorRef.ask(request)(10.seconds)
              .map {
                case RoomAttending.RevokedOperatorSTC(true) => HttpResponse(StatusCodes.OK)
                case _ => HttpResponse(StatusCodes.BadRequest)
              }(Implicits.global)
          case _ =>
            Future.successful(HttpResponse(StatusCodes.BadRequest))
        }
      }(Implicits.global)
      .recover { case _ => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
  }

  def routeVariousRequest(userMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, sessionManager: SessionManager[Long], actorMaterializer: ActorMaterializer): Route = {
    import RoomAttending._
    // 대화방 들어가기는 /roomAttending 으로 들어가고
    // 대화/퇴장은 /roomAttending/m 으로 하는 것이 좋겠다.
    // 앞에 user 를 붙일 필요는 없겠는데, 이유는 session-key 로 구분할 것이기 때문이다.
    // 대상을 구분하기 위해 user 경로가 필요할 수는 있어도 자신을 구분하기 위해서는 필요없다.
    path("roomAttending" / IntNumber) { raaId =>
      post {
        entity(as[RoomAttending.RoomAttendingRequestCTS]) { roomAttendingRequestCTS =>
          requiredSession(oneOff, usingCookies) { sessionKey =>
            complete(tryVariousRequest(userMasterActorRef, sessionKey, roomAttendingRequestCTS, raaId))
          }
        }
      }
    }
  }

  def tryLeaveRoom(userMasterActorRef: ActorRef, sessionKey: Long, raaId: Int, roomAttendingRequestCTS: RoomAttendingRequestCTS)(implicit actorSystem: ActorSystem): Future[HttpResponse] = {
    actorSystem.actorSelection(userMasterActorRef.path / sessionKey.toString / UserWorkerActor.nameRoomAttendingMasterActor / raaId.toString)
      .resolveOne(10.seconds)
      .flatMap { actorRef =>
        roomAttendingRequestCTS match {
          case RoomAttending.LeaveRoomCTS =>
            actorRef.ask(RoomAttending.LeaveRoomCTS)(10.seconds)
          case _ => ???
        }
      }(Implicits.global)
      .map {
        case result @ RoomAttending.LeavedRoomSTC(true) =>
          HttpResponse(
            StatusCodes.OK,
            entity = HttpEntity(
              ContentTypes.`application/json`,
              Source.single(result.asInstanceOf[RoomAttending.RoomAttendingResponseSTC].toJson.prettyPrint)
                .map(ByteString(_))
            )
          )
        case result @ RoomAttending.LeavedRoomSTC(_) =>
          HttpResponse(StatusCodes.BadRequest)
      }(Implicits.global)
      .recover { case _ => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
  }

  def routeLeaveRoom(userMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, sessionManager: SessionManager[Long], actorMaterializer: ActorMaterializer): Route = {
    path("roomAttending" / IntNumber) { raaId =>
      entity(as[RoomAttending.RoomAttendingRequestCTS]) { roomAttendingRequestCTS =>
        delete {
          requiredSession(oneOff, usingCookies) { sessionKey =>
            complete(tryLeaveRoom(userMasterActorRef, sessionKey, raaId, roomAttendingRequestCTS))
          }
        }
      }
    }
  }

  def tryListRoom(roomMasterActorRef: ActorRef): Future[HttpResponse] = {
    roomMasterActorRef.ask(RoomMaster.ListRoomCTS)(10.seconds)
      .map {
        case a @ RoomMaster.ListedRoomSTC(list) =>
          // 이 기능은 이와 같이 해도 되겠다.
          // 특히, 자료가 많을 경우 유용하다.
          HttpResponse(
            StatusCodes.OK,
            entity = HttpEntity.Chunked(
              ContentTypes.`application/json`,
              Source(list).map(_.toJson.compactPrint).map(ByteString(_)).map(ChunkStreamPart(_))
            )
          )
      }(Implicits.global)
      .recover { case _ => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
  }

  def routeListRoom(roomMasterActorRef: ActorRef)(implicit sessionManager: SessionManager[Long]): Route = {
    path("room") {
      get {
        requiredSession(oneOff, usingCookies) { _ =>
          complete(tryListRoom(roomMasterActorRef))
        }
      }
    }
  }

  def tryGetRoomStatus(roomMasterActorRef: ActorRef, roomNo: Int)(implicit actorSystem: ActorSystem): Future[HttpResponse] = {
    actorSystem.actorSelection(roomMasterActorRef.path / roomNo.toString)
      .resolveOne(10.seconds)
      .flatMap(_.ask(Room.GetRoomStatusCTS)(10.seconds))(Implicits.global)
      .map {
        case r @ Room.GotRoomStatusSTC(_, _, _, _) =>
          import DefaultJsonProtocol._
          // 인원수가 많지 않으면 손해인데, 많으면 이익이다.
          // 기능 연습을 목적으로 했을 뿐 딱히 이익볼려고 이와 같이 하는 것이 아니다.
          HttpResponse(
            StatusCodes.OK,
            entity =
              HttpEntity.Chunked(
                ContentTypes.`application/json`,
                Room.toJson(r).map(ChunkStreamPart(_))
              )
          )
      }(Implicits.global)
      .recover { case _ => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
  }

  def routeGetRoomStatus(roomMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, sessionManager: SessionManager[Long]) = {
    path("room" / IntNumber) { roomNo =>
      get {
        requiredSession(oneOff, usingCookies) { _ =>
          complete(tryGetRoomStatus(roomMasterActorRef, roomNo))
        }
      }
    }
  }

  def finalRoute(userMasterActorRef: ActorRef, roomMasterActorRef: ActorRef)(implicit actorSystem: ActorSystem, actorMaterializer: ActorMaterializer, sessionManager: SessionManager[Long]): Route =
    routeLogin(userMasterActorRef) ~
      routeLogout(userMasterActorRef) ~
      routeWebSocket(userMasterActorRef) ~
      routeOneTimeMessage(userMasterActorRef) ~
      routeCreateRoom(userMasterActorRef) ~
      // impo 일반적인 경로가 뒤로 가고 구체적인 경로가 앞으로 오도록 해야 한다.
      routeRoomAttendingPingPong(userMasterActorRef) ~
      routeVariousRequest(userMasterActorRef) ~
      routeLeaveRoom(userMasterActorRef) ~
      routeEnterRoom(userMasterActorRef) ~
      routeGetRoomStatus(roomMasterActorRef) ~
      routeListRoom(roomMasterActorRef)
}
