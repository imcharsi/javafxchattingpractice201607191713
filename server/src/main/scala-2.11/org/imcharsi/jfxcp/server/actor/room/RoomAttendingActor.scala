/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.room

import akka.actor._
import akka.pattern.ask
import org.imcharsi.jfxcp.common.roomattending.RoomAttending
import org.imcharsi.jfxcp.server.actor.user.{ UserWorkerActor, WebSocketActor }

import scala.collection.immutable.Queue
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.util.Success

/**
 * Created by i on 8/1/16.
 */
class RoomAttendingActor(
    roomMasterActorRef:          ActorRef,
    userWorkerActorRef:          ActorRef,
    roomAttendingMasterActorRef: ActorRef,
    timeout:                     FiniteDuration,
    roomNo:                      Int,
    raaId:                       Int,
    userId:                      String
) extends Actor {
  private[room] var roomActorRef: Option[ActorRef] = None
  private[room] var askActorRef: Option[ActorRef] = None
  private[room] var cancellableTimeout: Option[Cancellable] = None
  private[room] var queue: mutable.LinkedHashMap[(Int, Int), WebSocketActor.WebSocketActorQueueRequest] = mutable.LinkedHashMap()

  // 처음부터 web socket actor ref 를 전달하지 않고 EnterRoom 이 성공하면 BroadcastWebSocketActor 로 전달하기.
  // client 로부터 직접 입력을 받는 단계가 다르다.
  // EnterRoom 은 room attending master actor 가 client 로부터 입력을 받아서 room attending actor 로 입력을 중개한다.
  // EnterRoom 을 제외한 LeaveRoom 등은 room attending actor 가 직접 client 로부터 입력을 받는다.

  override def preStart(): Unit = {
    super.preStart()
    cancellableTimeout = Some(context.system.scheduler.scheduleOnce(timeout, self, RoomAttendingActor.InitTimeout)(Implicits.global))
  }

  override def postStop(): Unit = {
    super.postStop()
    cancelTimeout()
  }

  private def cancelTimeout(): Unit = {
    cancellableTimeout.foreach(_.cancel())
    cancellableTimeout = None
  }

  def receiveEnterRoom: Receive = {
    case RoomAttendingActor.EnterRoom =>
      // 왜 이와 같이 번거롭게 EnteredRoom 을 받는 단계를 거치는가.
      // ask + future 안에서 context.become 을 하지 않기 위함이다.
      cancelTimeout()
      askActorRef = Some(sender())
      // 왜 self.path.name 을 쓰는가. 중복이 있기 때문이다.
      // EnterRoom 에서 인자를 받을 경우,
      // room attending master actor 에서 room attending actor 를 만들 때 쓰는 이름과 중복이 되는데
      // 문제는 서로 다른 이름을 쓰지 못하도록 강제할 방법이 없다.
      // 그래서 actor 를 만들 때 쓴 이름을 그대로 쓸 수 있다면, EnterRoom 에서 다른 대화방 번호가 주어지지 않도록 막을 수 있다.
      context.system.actorSelection(roomMasterActorRef.path / roomNo.toString)
        .resolveOne(10.seconds)
        .onComplete {
          case Success(roomActorRef) => roomActorRef.tell(RoomActor.EnterRoom(userId), self)
          case _ => self.tell(RoomActor.EnteredRoom(false), self)
        }(Implicits.global)
      context.become(receiveEnteredRoom)
  }

  def receiveInitTimeout: Receive = {
    case RoomAttendingActor.InitTimeout =>
      context.stop(self)
  }

  def receiveEnteredRoom: Receive = {
    case RoomActor.EnteredRoom(true) =>
      askActorRef.foreach(_.tell(RoomAttendingActor.EnteredRoom(true), self))
      askActorRef = None
      roomActorRef = Some(sender())
      roomActorRef.foreach(context.watch(_))
      context.become(
        receiveMain
          .orElse(receiveLeaveRoom)
          .orElse(receiveTerminateRoomActor)
      )
      // 대화방에 입장할 때 user worker actor 의 시간초과를 갱신한다.
      userWorkerActorRef.tell(UserWorkerActor.RefreshTimeout, self)
    case RoomActor.EnteredRoom(false) =>
      askActorRef.foreach(_.tell(RoomAttendingActor.EnteredRoom(false), self))
      askActorRef = None
      context.stop(self)
  }

  def receiveLeaveRoom: Receive = {
    case RoomAttending.LeaveRoomCTS =>
      cancellableTimeout = Some(context.system.scheduler.scheduleOnce(timeout, self, RoomActor.LeavedRoom(false))(Implicits.global))
      askActorRef = Some(sender())
      roomActorRef.foreach(_.tell(RoomActor.LeaveRoom, self))
      context.become(receiveLeavedRoom)
  }

  def receiveLeavedRoom: Receive = {
    case RoomActor.LeavedRoom(result) =>
      cancelTimeout()
      askActorRef.foreach(_.tell(RoomAttending.LeavedRoomSTC(result), self))
      askActorRef = None
      roomActorRef.foreach(context.unwatch(_))
      roomActorRef = None
      context.stop(self)
      // 대화방에서 퇴장할 때 user worker actor 의 시간초과를 갱신한다.
      userWorkerActorRef.tell(UserWorkerActor.RefreshTimeout, self)
  }

  def receiveTerminateRoomActor: Receive = {
    // room actor 가 stop 으로 종료할 때, postStop 에서 보낸 message 가 먼저 처리되고
    // 이어서 Terminated 가 처리된다.
    // 그래서 message 가 최소한 한번은 web socket actor 로 전달되는 것이 확실한데,
    // web socket 이 message 를 전달한 뒤 보낸 응답은 dead-letters 로 가게 된다.
    // room actor 가 stop 으로 종료하는 경우에는 이 종료에 관한 message 가 전달된다는 보장이 없다.
    case Terminated(terminated) if roomActorRef.contains(terminated) =>
      context.stop(self)
  }

  def receiveMain: Receive = {
    case wsMsg @ WebSocketActor.QueueRoomAnnounceRTW(roomNo, msgNo, _) =>
      roomAttendingMasterActorRef.forward(wsMsg.copy(roomNo = raaId))
    case wsMsg @ WebSocketActor.QueueRoomChatRTW(roomNo, msgNo, _, _) =>
      roomAttendingMasterActorRef.forward(wsMsg.copy(roomNo = raaId))
    case wsMsg @ WebSocketActor.QueueRoomStatusChangedRTW(roomNo, msgNo) =>
      roomAttendingMasterActorRef.forward(wsMsg.copy(roomNo = raaId))
    case WebSocketActor.TransferredRoomMessageWTR(roomNo, msgNo) =>
      queue -= ((roomNo, msgNo))
    case c @ RoomAttending.ChatCTS(s) =>
      // 대화를 보낼 때는, 일단 자신이 참가한 대화방의 room attending actor 로 대화를 보내고
      // room attending actor 는 room actor 로 대화를 보낸 후
      // room actor 가 broadcast 해주는 QueueRoomChatRTW 를 client 가 받으면 화면에 대화를 찍는 식으로 한다.
      // client 가 대화를 보내는 단계에서는 화면에 아무것도 나타나지 않는다.
      // 이 명령에 대한 응답결과가 필요없다. route 단계에서 이 room attending actor 를 찾았는지 여부만 확인하면 된다.
      // 이와 같이 하지 않고 직접 room actor 로 대화를 전달할 수 있게 한다면
      // 대화방에 있지 않아도 대화를 보낼 수 있는 문제가 생긴다. 참가자만 대화방에서 대화를 할 수 있어야 한다.
      roomActorRef.foreach { x =>
        // 여기서 한가지 이상한 점은, RoomAttending.ChatCTS 로 왔는데, RoomActor.Chatted 로 나간다는 점이다.
        // 왜 이렇게 했는가. ChatCTS 에 대한 응답은 단순히 HTTP OK 로 하기로 구상을 했기 때문이다.
        // Chatted 는 응답을 하는 데 쓸 뭔가가 필요했기 때문에 넣을 뿐이고 다른 의미는 없다.
        // route 단계에서는 응답으로서 받은 Chatted 는 무시된다.
        // 반환값을 무시할거면 왜 만들었는가. room actor 가 RoomActor.Chat 를 받았다는 확인이 필요했기 때문이다.
        // 없는 대화방에 대화를 전송하려 하면 ask 에 의해 오류가 나타난다. 단순히 tell 로 했다면 dead-letters 로 가게 될 뿐
        // 없는 대화방으로 대화를 전송하려 했다는 사실확인은 되지 않는다.
        akka.pattern.pipe(x.ask(RoomActor.Chat(userId, s))(10.seconds))(Implicits.global).to(sender())
      }
      userWorkerActorRef.tell(UserWorkerActor.RefreshTimeout, self)
    case c @ RoomAttending.GrantOperatorCTS(granteeId) =>
      roomActorRef.foreach { x =>
        akka.pattern.pipe(
          x.ask(RoomActor.GrantOperator(userId, granteeId))(10.seconds)
            .map { case RoomActor.GrantedOperator(b) => RoomAttending.GrantedOperatorSTC(b) }(Implicits.global)
        )(Implicits.global).to(sender())
      }
      userWorkerActorRef.tell(UserWorkerActor.RefreshTimeout, self)
    case c @ RoomAttending.RevokeOperatorCTS(granteeId) =>
      roomActorRef.foreach { x =>
        akka.pattern.pipe(
          x.ask(RoomActor.RevokeOperator(userId, granteeId))(10.seconds)
            .map { case RoomActor.RevokedOperator(b) => RoomAttending.RevokedOperatorSTC(b) }(Implicits.global)
        )(Implicits.global).to(sender())
      }
      userWorkerActorRef.tell(UserWorkerActor.RefreshTimeout, self)
  }

  override def receive: Receive =
    receiveEnterRoom
      .orElse(receiveInitTimeout)
}

object RoomAttendingActor {

  sealed trait RoomAttendingActorRequest

  sealed trait RoomAttendingActorResponse

  case object EnterRoom extends RoomAttendingActorRequest

  case class EnteredRoom(result: Boolean) extends RoomAttendingActorResponse

  case object InitTimeout extends RoomAttendingActorRequest

  case class BroadcastWebSocketActor(actorRef: ActorRef) extends RoomAttendingActorRequest

}
