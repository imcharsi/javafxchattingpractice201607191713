/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.auth

import akka.actor.{ Actor, ActorRef }

/**
 * Created by i on 7/22/16.
 */
class IdLookupActor extends Actor {
  // AuthenticateActor 와 IdLookupActor 를 분리하였기 때문에
  // 시간차로 인한 검색실패가 있을 수 있게 된다.
  // 예를 들면 AuthenticateActor 에서 인증도 성공하고 map 도 갱신했지만
  // IdLookupActor 에는 아직 map 이 갱신되지 않는 경우이다.
  // https://en.wikipedia.org/wiki/Bottleneck_(engineering)
  // 한편, 왜 AuthenticateActor 와 IdLookupActor 로 분리하는가.
  // 인증하는 데 필요한 자료를 구하는 데 상당한 비용이 들 수 있다고 가정하는 동시에
  // 반면, id 에 대응하는 user worker actor 를 찾는 작업은 비용이 적은 것이 사실이고
  // 매 http traffic 이 id 에 대응하는 user worker actor 찾기로 시작하기 때문에 빈도는 많을 것이라 가정한다. 높은 빈도는 http 의 특성상 피할 수 없다.
  // 따라서 인증기능과 user worker actor 찾기 기능을 하나로 묶으면
  // 인증기능이 user worker actor 찾기에 영향을 주는 병목처럼 될 수 있다.
  private[actor] var mapIdToActorRef: Map[String, ActorRef] = Map.empty

  override def receive: Receive = {
    case IdLookupActor.UpdateMap(map) => mapIdToActorRef = map
    case IdLookupActor.Lookup(id) => sender().tell(IdLookupActor.LookupResult(mapIdToActorRef.get(id)), self)
  }
}

object IdLookupActor {

  sealed trait IdLookupRequest

  sealed trait IdLookupResponse

  case class Lookup(id: String) extends IdLookupRequest

  case class UpdateMap(map: Map[String, ActorRef]) extends IdLookupRequest

  case class LookupResult(result: Option[ActorRef]) extends IdLookupResponse

}
