/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.auth

import akka.actor.{ Actor, ActorRef, Terminated }

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }
import scala.util.{ Success, Try }

/**
 * Created by i on 7/23/16.
 */
class AuthenticateActor[Cred](idLookupActorRef: ActorRef, retrieveF: (String, String) => Future[Option[Cred]]) extends Actor {
  private[this] var mapIdToActorRef: Map[String, ActorRef] = Map.empty
  private[this] var mapActorRefToId: Map[ActorRef, String] = Map.empty

  private[this] def tryAuthenticate(id: String, password: String, userWorkerActorRef: ActorRef, sendUpdateMapAlways: Boolean): Unit = {
    // sendUpdateMapAlways 는 용도를 이해하기 어렵다.
    // 앞단계에서 mapIdToActorRef 의 갱신이 있어서 이번 인증이 성공하든 실패하든 무조건 UpdateMap 을 보낼 것을 가리킨다.
    Try(Await.result(retrieveF(id, password).recover { case _ => None }(Implicits.global), 10.seconds)) match {
      case Success(Some(_)) =>
        mapIdToActorRef.get(id)
          .foreach { oldUserWorkerActorRef =>
            mapIdToActorRef = mapIdToActorRef - id
            mapActorRefToId = mapActorRefToId - oldUserWorkerActorRef
            context.unwatch(oldUserWorkerActorRef)
            // todo 중복접속에 의해 user worker actor 가 종료했음을 구분해서 알리면 좋은가.
            context.system.stop(oldUserWorkerActorRef)
          }
        mapIdToActorRef = mapIdToActorRef + ((id, userWorkerActorRef))
        mapActorRefToId = mapActorRefToId + ((userWorkerActorRef, id))
        context.watch(userWorkerActorRef)
        userWorkerActorRef.tell(AuthenticateActor.Authenticated(Some(id.hashCode)), self)
        // 인증을 성공했는데 sendUpdateMapAlways==false 이면, 여기서 UpdateMap 을 보내고 아래에서 UpdateMap 을 보내지 않는다.
        // 인증을 성공했든 실패했든 sendUpdateMapAlways==true 이면, 여기서 UpdateMap 을 보내지 않고 아래에서 UpdateMap 을 보낸다.
        if (!sendUpdateMapAlways)
          idLookupActorRef.tell(IdLookupActor.UpdateMap(mapIdToActorRef), self)
      case _ =>
        userWorkerActorRef.tell(AuthenticateActor.Authenticated(None), self)
    }
    if (sendUpdateMapAlways)
      idLookupActorRef.tell(IdLookupActor.UpdateMap(mapIdToActorRef), self)
  }

  override def receive: Receive = {
    // todo 명시적으로 인증을 해제하는 기능을 둘지는 생각해보기. 지금은 user worker actor 의 stop 을 인식하여 인증을 해제하도록 했다.
    case a @ AuthenticateActor.Authenticate(id, password, false) =>
      mapIdToActorRef.get(id) match {
        case Some(_) => sender().tell(AuthenticateActor.Authenticated(None), self)
        case None => tryAuthenticate(id, password, sender(), false)
      }
    case a @ AuthenticateActor.Authenticate(id, password, true) =>
      tryAuthenticate(id, password, sender(), true)
    case a @ Terminated(terminatedUserWorkerActorRef) =>
      mapActorRefToId.get(terminatedUserWorkerActorRef)
        .foreach { oldId =>
          // 갱신할 map 의 내용에 동일한 id 로 이미 다른 actor ref 가 쓰여져 있으면 바꿔서는 안 된다.
          // impo 그런데 과연 이런 일이 생길 수 있는가.
          // 예를 들어, 사용자a 가 종료했는데, 아직 watch 의 Terminated 가 처리되지는 않은 상황에서
          // 사용자a 가 다시 인증을 시도한다면 map 에는 사용중으로 표시되어 있을 것이므로 인증실패한다.
          // 중복접속을 한다면 구성에 따라 기존의 watch 는 풀리고 따라서 기존 user worker actor 의 stop 은 알림을 받지 않게 된다.
          // http://doc.akka.io/docs/akka/2.4/scala/actors.html#Lifecycle_Monitoring_aka_DeathWatch
          // 정의에 따라, 이미 Terminated 가 쌓여있었다 해도 unwatch 로 Terminated 를 받지 않게 된다.
          // http://doc.akka.io/docs/akka/2.4/general/actors.html#State
          // https://www.toptal.com/scala/concurrency-and-fault-tolerance-made-easy-an-intro-to-akka
          // Akka ensures that each instance of an actor runs in its own lightweight thread and that messages are processed one at a time.
          // 요약하면, 한 번에 한개의 message 만 처리되기 때문에, Authenticate message 를 처리가 끝나기 전에 Terminated 를 시작하거나 그 반대순서의 일은 있을 수 없다는 말이다.
          // 그래서 결론은,
          // 첫째, Terminated 가 처리된다면 mapActorRefToId 의 key 인 actor ref 와 mapIdToActorRef 의 value 인 actor ref 는 아래의 두번째 이유로 인해 반드시 같을 것이라는 말이고
          // 둘째, mapActorRefToId 의 key 인 actor ref 와 mapIdToActorRef 의 value 인 actor ref 가 다르게 된다면
          // 이미 중복접속으로 다른 user worker actor 가 map 을 새로운 정보로 덮어 써놨고 따라서
          // 이전 user worker actor 의 stop 에 대한 watch 를 더 이상 유지할 필요가 없기 때문에 Terminated 를 받지 않게 된다는 말이다.
          // watch 를 유지하는 이유는 user worker actor 가 stop 단계에 들어갈 때, 인증 map 을 자동으로 갱신하기 위함이다.
          // 사용자a1 이 접속 중 사용자a2 가 중복접속하여 map 을 사용자a2 에 맞게 덮어 썼는데,
          // 사용자a1 의 Terminated 를 처리하여 map 을 사용자a1 의 인증해제로 표시할 수는 없는 것이다. 사용자a2 의 인증이 유효한 것으로 map 에 남아있어야 하는 것이다.
          // 따라서, unwatch 를 하지 않았다면 아래의 구절이 필요했겠지만, unwatch 를 하고 있으므로 아래의 구절은 필요없게 된다.
          //          if (mapIdToActorRef.get(oldId).contains(terminatedUserWorkerActorRef)) {
          //            mapIdToActorRef = mapIdToActorRef - oldId
          //            idLookupActorRef.tell(IdLookupActor.UpdateMap(mapIdToActorRef), self)
          //          }
          mapIdToActorRef = mapIdToActorRef - oldId
          idLookupActorRef.tell(IdLookupActor.UpdateMap(mapIdToActorRef), self)
          // 성능에 큰 영향을 주는 것은 아니지만, 관련 자료를 map 에서 찾았든 못찾았든 삭제를 시도하는 것은 낭비인 듯 하다.
          // 자료가 있음을 알고 있다면 삭제시도를 하는 것은 당연한 것이지만, 자료가 없음을 이미 아는데 구태여 삭제시도를 할 필요가 없다.
          mapActorRefToId = mapActorRefToId - terminatedUserWorkerActorRef
        }
  }
}

object AuthenticateActor {

  sealed trait AuthenticateActorRequest

  sealed trait AuthenticateActorResponse

  case class Authenticate(id: String, password: String, force: Boolean) extends AuthenticateActorRequest

  case class Authenticated(result: Option[Int]) extends AuthenticateActorResponse

}
