/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.room

import akka.actor.{ Actor, Props }
import org.imcharsi.jfxcp.common.roommaster.RoomMaster

import scala.concurrent.duration._
import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 8/1/16.
 */
class RoomMasterActor(initTimeout: FiniteDuration) extends Actor {
  private[this] var roomNoCounter: Int = 0
  // 대화방 번호, 방장, 방제, 참가자 목록. 대화방 목록 구하기.
  private[this] var roomList: Map[Int, RoomMasterActor.RoomListEntry] = Map()
  // 자료가 바뀌는 빈도가 많고 자료를 구하는 빈도가 적다면 대화방 목록을 매번 계산하는 것이 이익이고
  // 자료가 바뀌는 빈도가 적고 자료를 구하는 빈도가 많다면 대화방 목록을 매번 계산하는 것은 낭비이다.
  // 상식적으로 생각해보면 자료를 구하는 빈도보다 자료가 바뀌는 빈도가 훨씬 많다고 할 수 있는데
  // 그렇다고 자료를 구할 때마다 매번 새로 계산하는 것도 낭비이다.
  private[this] var cachedRoomList: RoomMaster.ListedRoomSTC = RoomMaster.ListedRoomSTC(List())
  // impo 따라서, 자료를 다시 계산해야 할 필요를 나타내면 편하다.
  private[this] var cacheCounter: Int = 0

  override def receive: Receive = {
    case RoomMaster.CreateRoomCTS(creatorId, subject) =>
      val askActorRef = sender()
      val newRoomNo = roomNoCounter
      roomNoCounter = roomNoCounter + 1
      Try {
        context.actorOf(Props(new RoomActor(newRoomNo, initTimeout, Some(creatorId), subject)), newRoomNo.toString)
      } match {
        case Success(actorRef) =>
          askActorRef.tell(RoomMaster.CreatedRoomSTC(Some(newRoomNo)), self)
        case Failure(ex) =>
          askActorRef.tell(RoomMaster.CreatedRoomSTC(None), self)
      }
    case RoomMaster.ListRoomCTS =>
      // 대화방 목록 구하기.
      // 최악의 경우 자료가 한번 바뀌고 자료를 한번 구하고 다시 바뀌고 다시 구하고를 반복하는 경우 성능이 가장 나빠지고
      // 자료가 한번 바뀌고 여러번 자료를 구하고를 반복하는 경우 성능이 가장 좋아진다.
      // 아래와 같이 cache 기능을 쓴다면 동기식으로 해야 한다.
      // 비동기식으로 할 경우 잠금을 써야 하는데
      // actor 의 mail box 개념이 사실상 잠금역할을 하고 있으므로 따로 할 것이 없다.
      // 매번 계산을 하기로 한다면 비동기식으로 할 수 있지만,
      // 비동기식으로 하는 데 따른 이익보다 매번 계산을 해야 하는 비용이 더 클 듯 하다.
      // 계산 비용을 줄이는 방법으로서, 입력을 좀 더 세분하면 비용을 줄일 수 있다.
      // 대화방 목록에서 보일 내용이 방장,방제 뿐이라면
      // 방장,방제가 바뀌거나 대화방추가/삭제일때만 대화방 목록을 다시 계산하면 되는 것이다.
      // 일단은, 이와 같은 세분화는 하지 말기.
      if (cacheCounter > 0) {
        cachedRoomList = RoomMaster.ListedRoomSTC(roomList.toStream.map(x => (x._1, x._2.ownerId, x._2.subject)).toList)
        cacheCounter = 0
      }
      sender().tell(cachedRoomList, self)
    case RoomMasterActor.PutRoomList(roomNo, ownerId, subject, userList) =>
      // 대화방 목록 갱신
      roomList = roomList + (((roomNo, RoomMasterActor.RoomListEntry(ownerId, subject, userList))))
      cacheCounter = cacheCounter + 1
    case RoomMasterActor.DeleteRoomList(roomNo) =>
      // 대화방 목록 삭제
      roomList = roomList - roomNo
      cacheCounter = cacheCounter + 1
  }
}

object RoomMasterActor {

  sealed trait RoomMasterActorRequest

  sealed trait RoomMasterActorResponse

  // 대화방에서 개설자가 입장할 때 room master actor 로 보내고,
  // 이후부터 사용자의 퇴장/입장,방장변경/방제변경이 있을 때 보낸다.
  case class PutRoomList(roomNo: Int, ownerId: String, subject: String, userList: Map[String, Boolean]) extends RoomMasterActorRequest

  // 대화방이 없어질 때 room master actor 로 보낸다.
  case class DeleteRoomList(roomNo: Int) extends RoomMasterActorRequest

  // 역시 순서가 복잡하다.
  // client ->(1) RMA
  // RMA ->(2) RA
  // RA ->(3) RMA
  // RMA ->(4) client
  // client ->(5) RAMA
  // RAMA ->(6) RAA
  // RAA ->(7) RA
  // RA ->(8) RAA
  // RAA ->(9) RAMA
  // RAMA ->(10) client
  //  (1) RoomMasterActor.CreateRoomCTS
  //  (2) RoomActor.InitRoom
  //  (3) RoomActor.InitRoom
  //  (4) RoomMasterActor.CreatedRoomSTC
  //  (5) RoomAttendingMasterActor.EnterRoomCTS
  //  (6) RoomAttendingActor.EnterRoom
  //  (7) RoomActor.EnterRoom
  //  (8) RoomActor.EnteredRoom
  //  (9) RoomAttendingActor.EnteredRoom
  //  (10) RoomAttendingMasterActor.EnteredRoomSTC

  case class RoomListEntry(ownerId: String, subject: String, userList: Map[String, Boolean])
}