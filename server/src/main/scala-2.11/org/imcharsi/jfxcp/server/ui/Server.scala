/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.ui

import javafx.application.Application
import javafx.beans.binding.{ BooleanBinding, When }
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.value.ObservableObjectValue
import javafx.event.ActionEvent
import javafx.scene.Scene
import javafx.scene.control.{ TextField, ToggleButton }
import javafx.scene.layout.{ VBox, BorderPane }
import javafx.stage.Stage

import akka.actor.{ ActorRef, ActorSystem, Props, Terminated }
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.{ Http, HttpExt }
import akka.stream.ActorMaterializer
import com.softwaremill.session.{ SessionConfig, SessionManager, SessionUtil }
import org.imcharsi.jfxcp.common.util.ObservableUtil
import org.imcharsi.jfxcp.server.actor.auth.{ AuthenticateActor, IdLookupActor }
import org.imcharsi.jfxcp.server.actor.room.RoomMasterActor
import org.imcharsi.jfxcp.server.actor.user.UserMasterActor
import org.imcharsi.jfxcp.server.route.RouteTemplate
import rx.lang.scala.JavaConversions._
import rx.lang.scala.schedulers.IOScheduler
import rx.lang.scala.{ Worker, JavaConversions, Observable, Subscription }
import rx.observables.JavaFxObservable._
import rx.schedulers.JavaFxScheduler

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }
import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 7/28/16.
 */
object Server {

  def dummyRetrieveF(id: String, password: String): Future[Option[Unit]] = Future(Some(()))(Implicits.global)

  case class StateHolder(
    actorSystem:          ActorSystem,
    actorMaterializer:    ActorMaterializer,
    idLookupActorRef:     ActorRef,
    authenticateActorRef: ActorRef,
    userMasterActor:      ActorRef,
    roomMasterActor:      ActorRef,
    http:                 HttpExt,
    serverBinding:        ServerBinding
  )

  class ServerUI(
      javaFxScheduler: rx.lang.scala.Scheduler,
      javaFxWorker:    Worker,
      ioScheduler:     rx.lang.scala.Scheduler,
      ioWorker:        Worker
  ) {
    var stateHolder: Option[StateHolder] = None
    implicit val sessionManager = new SessionManager[Long](SessionConfig.default(SessionUtil.randomServerSecret()))

    private[this] def initActorSystem(serverNamePort: (String, Int)): Future[StateHolder] =
      Try {
        implicit val actorSystem = ActorSystem()
        implicit val actorMaterializer = ActorMaterializer()
        val idLookupActorRef = actorSystem.actorOf(Props[IdLookupActor])
        val authenticateActorRef = actorSystem.actorOf(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(_, _))))
        val roomMasterActorRef = actorSystem.actorOf(Props(new RoomMasterActor(10.seconds)))
        val userMasterActorRef = actorSystem.actorOf(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 10.seconds, 1.minute, 10.seconds)))
        val http = Http()
        actorSystem.registerOnTermination(Console.err.println("terminated"))
        http.bindAndHandle(
          Route.handlerFlow(RouteTemplate.finalRoute(userMasterActorRef, roomMasterActorRef)),
          serverNamePort._1, serverNamePort._2
        )
          .map { serverBinding =>
            StateHolder(
              actorSystem, actorMaterializer, idLookupActorRef,
              authenticateActorRef, userMasterActorRef, roomMasterActorRef, http,
              serverBinding
            )
          }(Implicits.global)
          .recoverWith {
            case ex =>
              // 실패하면, 만들었던 actor system 을 놓치게 된다. 이와 같이 정리하는 것이 지금으로서는 편리하다.
              Await.ready(actorSystem.terminate(), 10.seconds)
              Future.failed(ex)
          }(Implicits.global)
      } match {
        case Success(s) => s
        case Failure(ex) => Future.failed(ex)
      }

    private[this] def clearActorSystem(): Future[Terminated] =
      stateHolder match {
        case None => Future.failed(new Exception)
        case Some(s) =>
          s.serverBinding
            .unbind()
            .flatMap(_ => s.actorSystem.terminate())(Implicits.global)
      }

    val flipFlop = new SimpleBooleanProperty(false)
    val onGoing = new SimpleBooleanProperty(false)
    // rx observable 로부터 boolean binding 을 만드는 좋은 방법이 지금 당장으로서는 생각이 나지 않아서 이렇게 했다.
    val badPort = new SimpleBooleanProperty(false)

    object serverName extends TextField {
      textProperty().set("localhost")
      prefWidthProperty().set(120)
      disableProperty().bind(flipFlop)
    }

    object serverPort extends TextField {
      textProperty().set("8080")
      prefWidthProperty().set(80)
      disableProperty().bind(flipFlop)
    }

    object button extends ToggleButton {
      disableProperty().bind(flipFlop.or(badPort))
      minWidthProperty().set(100)
      textProperty().bind(new When(onGoing).`then`("stop server").otherwise("start server"))
    }

    private val observableServerNamePort =
      toScalaObservable(fromObservableValue(serverName.textProperty()))
        .combineLatest(toScalaObservable(fromObservableValue(serverPort.textProperty())))
        .map { x => Try((x._1, Integer.valueOf(x._2).toInt)).toOption }

    private val observableOnGoing =
      toScalaObservable(fromObservableValue(onGoing))

    private val observableButton =
      toScalaObservable(fromActionEvents(button))

    private val observableCombined =
      ObservableUtil.observableABOptionCThenBC(
        observableButton,
        observableOnGoing,
        observableServerNamePort
      )

    def utilOne[A](f: Unit => Future[A]): Observable[Try[A]] =
      Observable.from(f())(Implicits.global)
        .map(Success(_))
        // http://reactivex.io/documentation/operators/catch.html
        .onErrorResumeNext(error => Observable.just(Failure(error)))

    def subscribeClearActorSystem(t: Try[Terminated]): Unit =
      t match {
        case Success(_) =>
          stateHolder = None
          onGoing.set(false)
          flipFlop.set(false)
        case Failure(error) =>
          Console.err.println(error)
          flipFlop.set(false)
      }

    def subscribeInitActorSystem(t: Try[StateHolder]): Unit =
      t match {
        case Success(s) =>
          stateHolder = Some(s)
          onGoing.set(true)
          flipFlop.set(false)
        case Failure(error) =>
          Console.err.println(error)
          flipFlop.set(false)
          button.selectedProperty().set(false)
      }

    // https://github.com/ReactiveX/RxJava/wiki/Connectable-Observable-Operators
    // 이 연습에서는 connect 기능을 쓸 필요가 없다.
    observableCombined
      .observeOn(javaFxScheduler)
      .doOnNext(_ => flipFlop.set(true))
      .observeOn(ioScheduler)
      .filter(_._1)
      .flatMap(_ => utilOne(_ => clearActorSystem()))
      .observeOn(javaFxScheduler)
      .subscribe(subscribeClearActorSystem(_))

    observableCombined
      .observeOn(javaFxScheduler)
      .doOnNext(_ => flipFlop.set(true))
      .observeOn(ioScheduler)
      .filterNot(x => x._1)
      .flatMap(x => utilOne(_ => initActorSystem(x._2)))
      .observeOn(javaFxScheduler)
      .subscribe(subscribeInitActorSystem(_))

    // port 가 int 로 변환되지 않으면 server 시작 기능을 비활성화 시킨다.
    observableServerNamePort.map(_.isEmpty).subscribe(badPort.set(_))

    def stop(): Unit = {
      Await.ready(clearActorSystem(), 10.seconds)
    }
  }

}
