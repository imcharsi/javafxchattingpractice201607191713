/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.room

import akka.actor._
import akka.pattern.ask
import org.imcharsi.jfxcp.common.roomattendingmaster.RoomAttendingMaster
import org.imcharsi.jfxcp.server.actor.user.{ WebSocketActor, UserWorkerActor }

import scala.collection.immutable.Queue
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{ Failure, Success }

/**
 * Created by i on 7/23/16.
 */
class RoomAttendingMasterActor(
    roomMasterActorRef: ActorRef,
    userWorkerActorRef: ActorRef,
    initTimeout:        FiniteDuration,
    userId:             String
) extends Actor {
  private[room] var webSocketActorRef: Option[ActorRef] = None
  private[room] var raaIdCounter: Int = 0 // room no 대신 raa id 를 쓰고 있다는 확인을 편하게 하기 위해 일부러 이렇게 썼다.
  private[room] var queue: mutable.LinkedHashMap[(Int, Int), WebSocketActor.WebSocketActorQueueRequest] = mutable.LinkedHashMap()
  def receiveEnterRoom: Receive = {
    case RoomAttendingMaster.EnterRoomCTS(roomNo) =>
      val askActorRef = sender()
      val newRaaId = raaIdCounter
      raaIdCounter = raaIdCounter + 1
      Future(context.actorOf(Props(new RoomAttendingActor(roomMasterActorRef, userWorkerActorRef, self, initTimeout, roomNo, newRaaId, userId)), newRaaId.toString))(Implicits.global)
        .flatMap { roomAttendingActorRef =>
          webSocketActorRef
            .foreach { webSocketActorRef =>
              roomAttendingActorRef
                .tell(RoomAttendingActor.BroadcastWebSocketActor(webSocketActorRef), self)
            }
          roomAttendingActorRef.ask(RoomAttendingActor.EnterRoom)(10.seconds)
        }(Implicits.global)
        .onComplete {
          case Success(RoomAttendingActor.EnteredRoom(true)) =>
            askActorRef.tell(RoomAttendingMaster.EnteredRoomSTC(Some(newRaaId)), self)
          case _ =>
            askActorRef.tell(RoomAttendingMaster.EnteredRoomSTC(None), self)
        }(Implicits.global)
  }

  def receiveForwardedMessage: Receive = {
    case wsMsg @ WebSocketActor.QueueRoomAnnounceRTW(raaId, msgNo, _) =>
      queue += (((raaId, msgNo), wsMsg))
      webSocketActorRef.foreach(_.tell(wsMsg, self))
    case wsMsg @ WebSocketActor.QueueRoomChatRTW(raaId, msgNo, _, _) =>
      queue += (((raaId, msgNo), wsMsg))
      webSocketActorRef.foreach(_.tell(wsMsg, self))
    case wsMsg @ WebSocketActor.QueueRoomStatusChangedRTW(raaId, msgNo) =>
      queue += (((raaId, msgNo), wsMsg))
      webSocketActorRef.foreach(_.tell(wsMsg, self))
    case WebSocketActor.TransferredRoomMessageWTR(raaId, msgNo) =>
      queue -= ((raaId, msgNo))
  }

  def receiveBroadcastWebSocketActor: Receive = {
    case UserWorkerActor.BroadcastWebSocketActor(webSocketActorRef) =>
      this.webSocketActorRef.foreach(context.unwatch(_))
      this.webSocketActorRef = Some(webSocketActorRef)
      this.webSocketActorRef.foreach(context.watch(_))
      // 왜 이와 같이 하는가.
      // 받아야 할 응답을 전부 못 받고 중간에 web socket 이 끊겼다면
      // 앞서 보낸 것들이 제대로 전달되지 않았다고 보고 처음부터 다시 보낸다.
      // 어떤 것이 잘 전달되었고 어떤 것이 잘 전달되지 않았는지는 응답이 오지 않았기 때문에 알 수 없다.
      // queue 에 쌓아뒀던 것 전부 실패했다고 본다.
      // 받는 쪽에서 중복이 있을 수 있는데, 받는 쪽에서 해결할 문제다.
      webSocketActorRef.tell(
        WebSocketActor.BatchQueueToWebSocketActor(
          queue.foldLeft(Queue[WebSocketActor.WebSocketActorQueueRequest]())((q, v) => q.enqueue(v._2))
        ), self
      )
    case Terminated(terminated) if webSocketActorRef.contains(terminated) =>
      webSocketActorRef = None
  }

  override def receive: Actor.Receive =
    receiveEnterRoom
      .orElse(receiveForwardedMessage)
      .orElse(receiveBroadcastWebSocketActor)
}

object RoomAttendingMasterActor {

  sealed trait RoomAttendingMasterRequest

  sealed trait RoomAttendingMasterResponse

}
