/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.room

import akka.actor._
import akka.routing.{ BroadcastRoutingLogic, Router }
import org.imcharsi.jfxcp.common.room.Room
import org.imcharsi.jfxcp.server.actor.user.WebSocketActor
import org.imcharsi.jfxcp.server.actor.user.WebSocketActor.{ QueueRoomAnnounceRTW, QueueRoomChatRTW, QueueRoomStatusChangedRTW }

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._

/**
 * Created by i on 8/1/16.
 */
class RoomActor(
    roomNo:                    Int,
    initTimeout:               FiniteDuration,
    private[room] var ownerId: Option[String],
    private[room] var subject: String
) extends Actor {
  private[room] var cancellableTimeout: Option[Cancellable] = None
  private[room] var router: Router = Router(BroadcastRoutingLogic())
  private[room] var mapActorRefToId: Map[ActorRef, String] = Map()
  private[room] var mapIdToActorRef: Map[String, ActorRef] = Map()
  private[room] var messageCounter: Int = 0
  private[room] var version: Int = 0
  // 참가자 목록. 운영자 여부를 같이 표시한다. 그 외 필요한 자료가 있으면 추가한다.
  private[room] var userList: Map[String, Boolean] = Map()

  override def preStart(): Unit = {
    super.preStart()
    cancellableTimeout = Some(context.system.scheduler.scheduleOnce(initTimeout, self, RoomActor.InitTimeout)(Implicits.global))
  }

  override def postStop(): Unit = {
    super.postStop()
    cancellableTimeout.foreach(_.cancel())
    cancellableTimeout = None
    if (!router.routees.isEmpty) {
      // 일반적으로, 여기서 message 가 가서는 안 된다. 대화방에 아무도 없어서 대화방이 종료하는 단계를 거쳐야 한다.
      // 예외적인 종료임을 표시해야 한다.
      // 아무래도, postStop 에서 보내는 message 가 watch 의 Terminated 보다 더 빨리 도착하는 듯 하다.
      router.route(WebSocketActor.QueueRoomAnnounceRTW(roomNo, allocMessageNo(), "this room is closed accidentally."), self)
      // 각 client 가 대화방의 새로운 상태를 읽으려고 시도할 때 대화방이 종료했음을 알도록 하기.
      router.route(WebSocketActor.QueueRoomStatusChangedRTW(roomNo, allocMessageNo()), self)
    }
  }

  private[room] def allocMessageNo(): Int = {
    val newMessageNo = messageCounter
    messageCounter = messageCounter + 1
    newMessageNo
  }

  private[room] def addMap(id: String, roomAttendingActorRef: ActorRef): Unit = {
    router = router.addRoutee(roomAttendingActorRef)
    mapActorRefToId = mapActorRefToId + ((roomAttendingActorRef, id))
    mapIdToActorRef = mapIdToActorRef + ((id, roomAttendingActorRef))
    userList = userList + ((id, false))
    version = version + 1
  }

  private[room] def removeMap(roomAttendingActorRef: ActorRef): Unit = {
    router = router.removeRoutee(roomAttendingActorRef)
    mapActorRefToId.get(roomAttendingActorRef).foreach { id =>
      mapIdToActorRef = mapIdToActorRef - id
      userList = userList - id
    }
    mapActorRefToId = mapActorRefToId - roomAttendingActorRef
    version = version + 1
  }

  private[room] def enterRoom(userId: String, roomAttendingActorRef: ActorRef): Unit = {
    // client 가 EnterRoom 을 보낼 때, 새 대화창을 만들고 이어서 오는 EnteredRoom 을 비롯하여
    // 모든 대화방 관련 알림을 대화창에서 표시하도록 하면
    // EnteredRoom 보다 RoomAnnounce 가 먼저 도착해서 달아둬야 하는 문제를 피할 수 있다.
    // 관련 기능은, org.imcharsi.jfxcp.client.ui.WebSocketClientActorBase 이다.
    // 자료가 도착하면 일단 쌓아놓는다.
    roomAttendingActorRef.tell(RoomActor.EnteredRoom(true), self)
    context.watch(roomAttendingActorRef)
    addMap(userId, roomAttendingActorRef)
    // 대화방에 사용자가 입장했음을 알리고
    router.route(QueueRoomAnnounceRTW(roomNo, allocMessageNo(), s"user ${userId} is entered into this room."), self)
    // 대화방 상태를 갱신해야 함을 알린다.
    router.route(QueueRoomStatusChangedRTW(roomNo, allocMessageNo()), self)
    ownerId
      .map(RoomMasterActor.PutRoomList(roomNo, _, subject, userList))
      .foreach(context.parent.tell(_, self))
  }

  private[room] def leaveRoom(message: Any, roomAttendingActorRef: ActorRef, isLeaveRoom: Boolean): Unit = {
    // LeaveRoom 의 경우, LeavedRoom 을 제외한 다른 응답은 오지 않는다.
    // room attending actor 가 종료했음을 watch 로 알게 된 경우, 응답을 보내지 않기 위해서 isLeaveRoom 을 쓴다.
    if (mapActorRefToId.contains(roomAttendingActorRef)) {
      context.unwatch(roomAttendingActorRef)
      removeMap(roomAttendingActorRef)
      if (isLeaveRoom)
        roomAttendingActorRef.tell(RoomActor.LeavedRoom(true), self)
      // 퇴장을 한 사용자에게는 퇴장에 관한 message 를 보내지 않는다.
      if (!router.routees.isEmpty)
        router.route(message, self)
      // 방장이 퇴장했으면 자동으로 다른 방장을 정해야 한다.
      val currentOwner = ownerId.flatMap(mapIdToActorRef.get(_))
      if (currentOwner.isEmpty) {
        ownerId = mapIdToActorRef.headOption.map(_._1)
        // 아래의 경우, ownerId 가 None 이 된다면, router.routees.isEmpty = true 가 된다.
        // 다르게 말해서, 더 이상 남아있는 사람이 없으면, room announce 를 보내지 않는다.
        if (!router.routees.isEmpty) {
          // todo 방장이 바뀌었음을 알리ᅟ는 문장을 써야 한다.
          router.route(WebSocketActor.QueueRoomAnnounceRTW(roomNo, allocMessageNo(), s"room owner is changed to ${ownerId.get}"), self)
        }
      }
      // 마지막 참가자가 퇴장했으면 방을 지워야 한다.
      if (router.routees.isEmpty) {
        context.stop(self)
        context.parent.tell(RoomMasterActor.DeleteRoomList(roomNo), self)
      } else {
        // 여전히 운영되는 대화방이면 대화방 상태가 바뀌었음을 알린다.
        router.route(WebSocketActor.QueueRoomStatusChangedRTW(roomNo, allocMessageNo()), self)
        ownerId
          .map(RoomMasterActor.PutRoomList(roomNo, _, subject, userList))
          .foreach(context.parent.tell(_, self))
      }
    } else {
      if (isLeaveRoom)
        roomAttendingActorRef.tell(RoomActor.LeavedRoom(false), self)
    }
  }

  def receiveCreatorEnterRoom: Receive = {
    case RoomActor.EnterRoom(id) =>
      // sender 는 room attending actor 이어야 한다.
      val roomAttendingActorRef = sender()
      // 입장이 성공하든 실패하든 시간초과는 지운다. 더 이상 쓸 일이 없다.
      cancellableTimeout.foreach(_.cancel())
      cancellableTimeout = None
      if (ownerId.contains(id)) {
        enterRoom(id, roomAttendingActorRef)
        context.become(receiveMain.orElse(receiveWatchRoomAttendingActor))
      } else {
        // 개설자의 최초 입장이 아니면 대화방을 종료시킨다.
        roomAttendingActorRef.tell(RoomActor.EnteredRoom(false), self)
        context.stop(self)
      }
  }

  def receiveTimeout: Receive = {
    case RoomActor.InitTimeout =>
      context.stop(self)
  }

  def receiveMain: Receive = {
    case RoomActor.EnterRoom(id) =>
      val roomAttendingActorRef = sender()
      // id 가 중복되어서도 안 되고, room attending actor 가 중복되어서도 안 된다.
      // 재접속을 하는 경우는 어떤가. 재접속을 시도하면 기존 접속에 관한 actor 는 모두 종료된다.
      // 따라서, 기존 접속에서 만들어진 room attending actor 에 대한 watch 로 퇴장하게 된다.
      // 중복으로 인한 재접속 문제는 없다.
      if (!mapActorRefToId.contains(roomAttendingActorRef) && !mapIdToActorRef.contains(id)) {
        enterRoom(id, roomAttendingActorRef)
      } else {
        roomAttendingActorRef.tell(RoomActor.EnteredRoom(false), self)
      }
    case RoomActor.Chat(userId, s) =>
      router.route(QueueRoomChatRTW(roomNo, allocMessageNo(), userId, s), self)
      sender().tell(RoomActor.Chatted, self)
    case RoomActor.LeaveRoom =>
      val roomAttendingActorRef = sender()
      mapActorRefToId.get(roomAttendingActorRef)
        .map(id => WebSocketActor.QueueRoomAnnounceRTW(roomNo, allocMessageNo(), s"user ${id} leaved this room."))
        .orElse(Some(WebSocketActor.QueueRoomAnnounceRTW(roomNo, allocMessageNo(), s"???")))
        .foreach(leaveRoom(_, roomAttendingActorRef, true))
    case Room.GetRoomStatusCTS =>
      // 참가자 목록 구하기. 다른 명령들은 room attending actor 에서 받지만 이 명령은 route 에서 직접 받는다.
      // 참가자 목록은 비참가자도 구할 수 있도록 정했다. 정하기 나름이다.
      ownerId
        .map(Room.GotRoomStatusSTC(version, _, subject, userList))
        .foreach(sender().tell(_, self))
    case RoomActor.GrantOperator(granterId, granteeId) =>
      (userList.get(granteeId), ownerId) match {
        case (Some(_), Some(ownerId)) if ownerId == granterId =>
          userList = userList + (((granteeId, true)))
          version = version + 1
          router.route(WebSocketActor.QueueRoomAnnounceRTW(roomNo, allocMessageNo(), s"user ${granteeId} is granted by ${granterId}."), self)
          router.route(WebSocketActor.QueueRoomStatusChangedRTW(roomNo, allocMessageNo()), self)
          sender().tell(RoomActor.GrantedOperator(true), self)
        case (Some(_), Some(ownerId)) if ownerId != granterId =>
          sender().tell(RoomActor.GrantedOperator(false), self)
        case (None, Some(_)) =>
          sender().tell(RoomActor.GrantedOperator(false), self)
        case _ =>
          sender().tell(RoomActor.GrantedOperator(false), self)
      }
    case RoomActor.RevokeOperator(granterId, granteeId) =>
      (userList.get(granteeId), ownerId) match {
        case (Some(_), Some(ownerId)) if ownerId == granterId =>
          userList = userList + (((granteeId, false)))
          version = version + 1
          router.route(WebSocketActor.QueueRoomAnnounceRTW(roomNo, allocMessageNo(), s"user ${granteeId} is revoked by ${granterId}."), self)
          router.route(WebSocketActor.QueueRoomStatusChangedRTW(roomNo, allocMessageNo()), self)
          sender().tell(RoomActor.RevokedOperator(true), self)
        case (Some(_), Some(ownerId)) if ownerId != granterId =>
          sender().tell(RoomActor.RevokedOperator(false), self)
        case (None, Some(_)) =>
          sender().tell(RoomActor.RevokedOperator(false), self)
        case _ =>
          sender().tell(RoomActor.RevokedOperator(false), self)
      }
  }

  def receiveWatchRoomAttendingActor: Receive = {
    case Terminated(terminated) if mapActorRefToId.contains(terminated) =>
      val roomAttendingActorRef = sender()
      mapActorRefToId.get(roomAttendingActorRef)
        .map(id => WebSocketActor.QueueRoomAnnounceRTW(roomNo, allocMessageNo(), s"user ${id} leaved this room accidentally."))
        .orElse(Some(WebSocketActor.QueueRoomAnnounceRTW(roomNo, allocMessageNo(), s"???")))
        .foreach(leaveRoom(_, terminated, false))
  }

  override def receive: Receive = receiveCreatorEnterRoom.orElse(receiveTimeout)
}

object RoomActor {

  sealed trait RoomActorRequest

  sealed trait RoomActorResponse

  case class EnterRoom(id: String) extends RoomActorRequest

  case class EnteredRoom(result: Boolean) extends RoomActorResponse

  // LeaveRoom message 를 받고 퇴장하는 것이 원칙이지만 watch 로도 퇴장하도록 하기.
  case object LeaveRoom extends RoomActorRequest

  case class LeavedRoom(result: Boolean) extends RoomActorResponse

  case class Chat(id: String, s: String) extends RoomActorRequest

  case class GrantOperator(granterId: String, granteeId: String) extends RoomActorRequest

  case class GrantedOperator(result: Boolean) extends RoomActorResponse

  case class RevokeOperator(granterId: String, granteeId: String) extends RoomActorRequest

  case class RevokedOperator(result: Boolean) extends RoomActorResponse

  case object Chatted extends RoomActorResponse

  case object InitTimeout extends RoomActorRequest

}
