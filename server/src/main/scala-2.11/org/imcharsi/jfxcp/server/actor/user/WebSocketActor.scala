/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.user

import akka.actor.{ Actor, ActorRef, Cancellable }
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.stream.actor.ActorPublisherMessage.{ Cancel, Request }
import akka.stream.actor.ActorSubscriberMessage.{ OnComplete, OnError, OnNext }
import akka.stream.actor.{ ActorPublisher, ActorSubscriber, RequestStrategy, WatermarkRequestStrategy }
import org.imcharsi.jfxcp.common.websocket.WebSocket
import org.imcharsi.jfxcp.common.websocket.WebSocket._
import org.imcharsi.jfxcp.server.actor.user.WebSocketActor._
import spray.json._

import scala.collection.immutable.Queue
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration.FiniteDuration
import scala.util.{ Success, Try }

/**
 * Created by i on 7/23/16.
 */
class WebSocketActor(
  oneTimeMessageActorRef:      ActorRef,
  roomAttendingMasterActorRef: ActorRef,
  firstTimeout:                FiniteDuration
)
    extends Actor with ActorPublisher[Message] with ActorSubscriber {
  var queue: Queue[WebSocketActor.WebSocketActorQueueRequest] = Queue()
  var cancellableTimeout: Option[Cancellable] = None

  override def preStart(): Unit = {
    super.preStart()
    cancellableTimeout =
      Some(
        context.system.scheduler
          .scheduleOnce(firstTimeout, self, WebSocketActor.Timeout)(Implicits.global)
      )
  }

  override def postStop(): Unit = {
    super.postStop()
    cancellableTimeout.forall(_.cancel())
    cancellableTimeout = None
  }

  def processQueue(): Unit = {
    while (totalDemand > 0 && !queue.isEmpty) {
      val (dequeue, newQueue) = queue.dequeue
      queue = newQueue
      dequeue match {
        case WebSocketActor.QueueOneTimeMessageOTW(id, chat, userNo, msgNo) =>
          // http://www.mkyong.com/java/jackson-streaming-api-to-read-and-write-json/
          // 에서 보는 것과는 다르게,
          // https://github.com/spray/spray-json/blob/master/src/main/scala/spray/json/JsonParser.scala#L217
          // 에서 보는 것처럼 spray-json 은 streaming 방식이 아니다.
          // 이유는, 최소한 length 기능 때문이라 할 수 있는데, stream 을 전부 읽어내기 전까지는 전체 길이를 알 수 없기 때문이다.
          // stream 을 전부 읽은 후에야 필요한 작업을 할 수 있다면 stream 을 쓰는 의미가 없는 것이다.
          // 따라서, 장문의 json 이 있다 하더라도 spray-json 을 쓰는 이상, TextMessage.Streamed 로 이익을 볼 수는 없고
          // impo 이번 연습에서는 TextMessage.Streamed 의 각각이 모두 TextMessage.Strict 로 오는 자료와 동일한 성격의 자료라고 가정한다.
          // stream 출력에 관한 한가지 요령은,
          // Source(List(1, 2, 3).toStream.map(i => s"${i} hi")).runForeach(Console.err.println(_))
          // 인데, 주어진 자료가 아주 많을 때 유용하다.
          onNext(
            TextMessage.Strict(
              OneTimeMessageSTC(id, chat, userNo, msgNo)
              .asInstanceOf[WebSocketMessageSTC]
              .toJson.prettyPrint
            )
          )
        case WebSocketActor.QueueOneTimeMessageAckOTW(userNo, msgNo) =>
          onNext(
            TextMessage.Strict(
              OneTimeMessageAckSTC(userNo, msgNo)
              .asInstanceOf[WebSocketMessageSTC]
              .toJson.prettyPrint
            )
          )
        case WebSocketActor.QueueRoomAnnounceRTW(raaId, msgNo, s) =>
          onNext(
            TextMessage.Strict(
              RoomAnnounceSTC(raaId, msgNo, s)
              .asInstanceOf[WebSocketMessageSTC]
              .toJson.prettyPrint
            )
          )
        case WebSocketActor.QueueRoomChatRTW(raaId, msgNo, userId, s) =>
          onNext(
            TextMessage.Strict(
              RoomChatSTC(raaId, msgNo, userId, s)
              .asInstanceOf[WebSocketMessageSTC]
              .toJson.prettyPrint
            )
          )
        case WebSocketActor.QueueRoomStatusChangedRTW(raaId, msgNo) =>
          onNext(
            TextMessage.Strict(
              RoomStatusChangedSTC(raaId, msgNo)
              .asInstanceOf[WebSocketMessageSTC]
              .toJson.prettyPrint
            )
          )
        case WebSocketActor.GreetingSecondSTC =>
          onNext(
            TextMessage.Strict(
              WebSocket.GreetingSecondSTC
              .asInstanceOf[WebSocketMessageSTC]
              .toJson.prettyPrint
            )
          )
        // 그 외, room attending worker actor 로부터 온 입력도 다뤄야 한다.
        case _ =>
      }
    }
  }

  def processIncomingFromWebSocket(s: String): Unit =
    Try(s.parseJson.convertTo[WebSocketMessageCTS]) match {
      case Success(TransferredOneTimeMessageCTS(userNo, msgNo)) =>
        oneTimeMessageActorRef.tell(TransferredOneTimeMessageWTO(userNo, msgNo), self)
      case Success(TransferredOneTimeMessageAckCTS(userNo, msgNo)) =>
        oneTimeMessageActorRef.tell(TransferredOneTimeMessageAckWTO(userNo, msgNo), self)
      case Success(a @ TransferredRoomMessageCTS(raaId, msgNo)) =>
        roomAttendingMasterActorRef.tell(TransferredRoomMessageWTR(raaId, msgNo), self)
      // 원래의 구상은, 최초의 자료교환이 있기 전에는 어떠한 자료교환도 가능하지 않도록 하는 것이었는데
      // 지금은 web socket actor 가 만들어진 이후 주어진 시간 안에 GreetingFirstCTS 가 오기만 하면 되는 것으로 바꿨다.
      // 이와 같이 해도 주어진 시간안에 최초의 자료전달이 있어야 한다는 것을 강제하기에 충분하고
      // 만약 더 빨리 다른 자료교환이 이미 있었다 해도, 한번의 자료교환을 추가로 더 하는 것이 큰 낭비는 아니다.
      // 더 복잡하게 구성해야 할 필요가 없다.
      case Success(GreetingFirstCTS) =>
        cancellableTimeout.foreach(_.cancel())
        cancellableTimeout = None
        queue = queue.enqueue(WebSocketActor.GreetingSecondSTC)
        processQueue()
      case _ =>
    }

  def receiveAsPublisher: Receive = {
    case Request(n) =>
      processQueue()
    case Cancel =>
      // subscription 이 취소되었다. onComplete() 를 해도 받을 상대가 없다. 그냥 멈추면 된다.
      context.stop(self)
  }

  def receiveAsSubscriber: Receive = {
    case OnNext(s: String) =>
      processIncomingFromWebSocket(s)
    case OnError(ex) =>
      onErrorThenStop(ex)
    case OnComplete =>
      onCompleteThenStop()
  }

  def receiveAsWebSocketActor: Receive = {
    case WebSocketActor.BatchQueueToWebSocketActor(queue) =>
      this.queue = this.queue.enqueue(queue)
      processQueue()
    case a: WebSocketActor.WebSocketActorQueueRequest =>
      queue = queue.enqueue(a)
      processQueue()
    case WebSocketActor.Timeout =>
      cancellableTimeout = None
      onErrorThenStop(new TimeoutException)
  }

  def receiveAnother: Receive = {
    // 미리 정하지 않은 입력을 받으면 web socket 을 종료한다.
    case _ => onErrorThenStop(new UnknownException)
  }

  override def receive: Receive =
    receiveAsPublisher
      .orElse(receiveAsSubscriber)
      .orElse(receiveAsWebSocketActor)
      .orElse(receiveAnother)

  override protected def requestStrategy: RequestStrategy = WatermarkRequestStrategy(16)
}

object WebSocketActor {

  sealed trait WebSocketActorRequest

  sealed trait WebSocketActorQueueRequest

  sealed trait WebSocketActorResponse

  case class QueueOneTimeMessageOTW(senderId: String, chat: String, userNo: Int, msgNo: Int) extends WebSocketActorRequest with WebSocketActorQueueRequest

  case class BatchQueueToWebSocketActor(queue: Queue[WebSocketActor.WebSocketActorQueueRequest]) extends WebSocketActorRequest

  case class TransferredOneTimeMessageWTO(userNo: Int, msgNo: Int) extends WebSocketActorResponse

  case class QueueOneTimeMessageAckOTW(userNo: Int, msgNo: Int) extends WebSocketActorRequest with WebSocketActorQueueRequest

  case class TransferredOneTimeMessageAckWTO(userNo: Int, msgNo: Int) extends WebSocketActorResponse

  case object Timeout extends WebSocketActorRequest

  // 아래의 case 는 WebSocketMessage.GreetingSecondSTC 와 의도가 같다.
  // 단지, queue 에 WebSocketMessage.GreetingSecondSTC 를 넣을 수 없어서 똑같이 만들었다.
  // 한편, GreetingFirstCTS 는 server 단계에서는 쓸 일이 없다. 그래서 만들지 않았다.
  case object GreetingSecondSTC extends WebSocketActorRequest with WebSocketActorQueueRequest

  // RoomActor/RoomAttendingActor To WebSocketActor
  case class QueueRoomAnnounceRTW(roomNo: Int, msgNo: Int, s: String) extends WebSocketActorRequest with WebSocketActorQueueRequest

  case class QueueRoomChatRTW(roomNo: Int, msgNo: Int, userId: String, s: String) extends WebSocketActorRequest with WebSocketActorQueueRequest

  case class QueueRoomStatusChangedRTW(roomNo: Int, msgNo: Int) extends WebSocketActorRequest with WebSocketActorQueueRequest

  case class TransferredRoomMessageWTR(roomNo: Int, msgNo: Int) extends WebSocketActorResponse

  class UnknownException extends RuntimeException

  class TimeoutException extends RuntimeException

  // web socket actor 에서는 따로 자료를 보관해 둘 필요는 없다.
  // OneTimeMessageActor 로 부터 들어오는 OneTimeMessage 는 전부 쌓아놓고 client 로 보낸 후 버리면 된다.
  // 쌓아두는 이유는, OneTimeMessage 가 들어오는 속도가 web socket 으로 나가는 속도보다 더 빠를수 있기 때문에 쌓아두는 것이다.
  // 그 이외에 쌓아두는 이유는 없다.
  // 들어오는 응답 역시 parser 로 해석하여 관련된 actor 로 보내주면 된다.
  // 문제는, 들어오는 응답이 OneTimeMessage 에 대한 수신응답이면, 생성자로 주어진 oneTimeMessageActorRef 로 수신응답을 보낼 수 있지만
  // 들어오는 응답이 RoomAttendingMessage 에 대한 수신응답이면, 정확히 어느 RoomAttendingWorkerActor 로 수신응답을 보낼지 알 수 없다.
  // 따라서, 생성자로 주어진 roomAttendingMasterActor 와, 응답으로 받은 자료의 room id 를 섞어서 room attending worker actor 위치를 계산하여 응답을 보내는 수 밖에 없다.

}
