/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.auth

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import org.scalatest.FunSuiteLike

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.Future
import scala.concurrent.duration._

/**
 * Created by i on 7/23/16.
 */
class TestAuthenticateActor extends TestKit(ActorSystem()) with FunSuiteLike {
  def dummyRetrieveF(correctId: String)(id: String, password: String): Future[Option[Unit]] = {
    Future {
      if (id == correctId)
        Some(())
      else
        None
    }(Implicits.global)
  }

  test("authenticate actor success") {
    val idLookUpActorRef = TestProbe("idLookupActor")
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookUpActorRef.ref, dummyRetrieveF("hi")(_, _))))
    authenticateActorRef.tell(AuthenticateActor.Authenticate("hi", "hi", false), testActor)
    expectMsg(AuthenticateActor.Authenticated(Some("hi".hashCode)))
    idLookUpActorRef.expectMsg(IdLookupActor.UpdateMap(Map("hi" -> testActor)))
  }
  test("authenticate actor failure") {
    val idLookUpActorRef = TestProbe("idLookupActor")
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookUpActorRef.ref, dummyRetrieveF("hi")(_, _))))
    authenticateActorRef.tell(AuthenticateActor.Authenticate("hii", "hi", false), testActor)
    expectMsg(AuthenticateActor.Authenticated(None))
    idLookUpActorRef.expectNoMsg(0.5.seconds)
  }
  // 중복없이 인증을 시도해서 실패하기.
  test("authenticate actor duplicated try without force") {
    val idLookUpActorRef = TestProbe("idLookupActor")
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookUpActorRef.ref, dummyRetrieveF("hi")(_, _))))
    val userWorkerActorRef1 = TestProbe("userWorkerActorRef1")
    val userWorkerActorRef2 = TestProbe("userWorkerActorRef2")
    authenticateActorRef.tell(AuthenticateActor.Authenticate("hi", "hi", false), userWorkerActorRef1.ref)
    userWorkerActorRef1.expectMsg(AuthenticateActor.Authenticated(Some("hi".hashCode)))
    idLookUpActorRef.expectMsg(IdLookupActor.UpdateMap(Map("hi" -> userWorkerActorRef1.ref)))
    authenticateActorRef.tell(AuthenticateActor.Authenticate("hi", "hi", false), userWorkerActorRef2.ref)
    userWorkerActorRef2.expectMsg(AuthenticateActor.Authenticated(None))
    idLookUpActorRef.expectNoMsg(0.5.seconds)
  }
  // 중복하여 인증을 시도해서 앞의 user worker 가 알림을 받기.
  test("authenticate actor duplicated try with force") {
    val idLookUpActorRef = TestProbe("idLookupActor")
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookUpActorRef.ref, dummyRetrieveF("hi")(_, _))))
    val userWorkerActorRef1 = TestProbe("userWorkerActorRef1")
    val userWorkerActorRef2 = TestProbe("userWorkerActorRef2")
    authenticateActorRef.tell(AuthenticateActor.Authenticate("hi", "hi", false), userWorkerActorRef1.ref)
    userWorkerActorRef1.expectMsg(AuthenticateActor.Authenticated(Some("hi".hashCode)))
    idLookUpActorRef.expectMsg(IdLookupActor.UpdateMap(Map("hi" -> userWorkerActorRef1.ref)))
    watch(userWorkerActorRef1.ref)
    authenticateActorRef.tell(AuthenticateActor.Authenticate("hi", "hi", true), userWorkerActorRef2.ref)
    userWorkerActorRef2.expectMsg(AuthenticateActor.Authenticated(Some("hi".hashCode)))
    idLookUpActorRef.expectMsg(IdLookupActor.UpdateMap(Map("hi" -> userWorkerActorRef2.ref)))
    expectTerminated(userWorkerActorRef1.ref)
  }
  // 앞의 user worker 가 종료하면, authenticate actor 가 알림을 받고 인증목록에서 지우기.
  test("authenticate actor notified") {
    val idLookUpActorRef = TestProbe("idLookupActor")
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookUpActorRef.ref, dummyRetrieveF("hi")(_, _))))
    val userWorkerActorRef1 = TestProbe("userWorkerActorRef1")
    authenticateActorRef.tell(AuthenticateActor.Authenticate("hi", "hi", false), userWorkerActorRef1.ref)
    userWorkerActorRef1.expectMsg(AuthenticateActor.Authenticated(Some("hi".hashCode)))
    idLookUpActorRef.expectMsg(IdLookupActor.UpdateMap(Map("hi" -> userWorkerActorRef1.ref)))
    system.stop(userWorkerActorRef1.ref)
    idLookUpActorRef.expectMsg(IdLookupActor.UpdateMap(Map()))
    val userWorkerActorRef2 = TestProbe("userWorkerActorRef2")
    authenticateActorRef.tell(AuthenticateActor.Authenticate("hi", "hi", false), userWorkerActorRef2.ref)
    userWorkerActorRef2.expectMsg(AuthenticateActor.Authenticated(Some("hi".hashCode)))
    idLookUpActorRef.expectMsg(IdLookupActor.UpdateMap(Map("hi" -> userWorkerActorRef2.ref)))
  }
}
