/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.room

import akka.actor.{ ActorRef, PoisonPill, Props, ActorSystem }
import akka.routing.ActorRefRoutee
import akka.testkit.{ TestProbe, TestActorRef, TestKit }
import org.imcharsi.jfxcp.common.room.Room
import org.imcharsi.jfxcp.server.actor.user.WebSocketActor
import org.scalatest.FunSuiteLike

import scala.concurrent.duration._

/**
 * Created by i on 8/1/16.
 */
class TestRoomActor extends TestKit(ActorSystem()) with FunSuiteLike {
  def testExistence(roomActor: RoomActor, id: String, roomAttendingActorRef: ActorRef): (Boolean, Boolean, Boolean, Boolean) = {
    (
      roomActor.router.routees.contains(ActorRefRoutee(roomAttendingActorRef)),
      roomActor.mapIdToActorRef.contains(id),
      roomActor.mapActorRefToId.contains(roomAttendingActorRef),
      roomActor.mapIdToActorRef.get(id)
      .filter(actorRef => actorRef == roomAttendingActorRef && roomActor.mapActorRefToId.get(roomAttendingActorRef).contains(id))
      .isDefined
    )
  }

  test("success create room actor") {
    val roomAttendingActorRef = TestProbe("roomAttendingActor")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    assert(roomActorRef.underlyingActor.cancellableTimeout.isDefined)
    val result1 = testExistence(roomActorRef.underlyingActor, "hi", roomAttendingActorRef.ref)
    assertResult(false)(result1._1 || result1._2 || result1._3)
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef.ref)
    roomAttendingActorRef.expectMsg(RoomActor.EnteredRoom(true))
    roomAttendingActorRef.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    assert(roomActorRef.underlyingActor.cancellableTimeout.isEmpty)
    val result2 = testExistence(roomActorRef.underlyingActor, "hi", roomAttendingActorRef.ref)
    assertResult(true)(result2._1 && result2._2 && result2._3)
  }
  test("timeout") {
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    watch(roomActorRef)
    expectTerminated(roomActorRef)
  }
  test("enter room with wrong creator id") {
    val roomAttendingActorRef = TestProbe("roomAttendingActor")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    assert(roomActorRef.underlyingActor.cancellableTimeout.isDefined)
    watch(roomActorRef)
    roomActorRef.tell(RoomActor.EnterRoom("hi1"), roomAttendingActorRef.ref)
    roomAttendingActorRef.expectMsg(RoomActor.EnteredRoom(false))
    expectTerminated(roomActorRef)
  }
  test("duplicated enter room with same id but different room attending actor") {
    val roomAttendingActorRef1 = TestProbe("roomAttendingActor1")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(true))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    val roomAttendingActorRef2 = TestProbe("roomAttendingActor2")
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef2.ref)
    roomAttendingActorRef2.expectMsg(RoomActor.EnteredRoom(false))
    val result1 = testExistence(roomActorRef.underlyingActor, "hi", roomAttendingActorRef1.ref)
    assertResult(true)(result1._1 && result1._2 && result1._3 && result1._4)
    val result2 = testExistence(roomActorRef.underlyingActor, "hi", roomAttendingActorRef2.ref)
    assertResult(false)(result2._1 || result2._3 || result2._4)
  }
  test("duplicated enter room with different id but same room attending actor") {
    val roomAttendingActorRef1 = TestProbe("roomAttendingActor1")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(true))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomActorRef.tell(RoomActor.EnterRoom("hi1"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(false))
    val result1 = testExistence(roomActorRef.underlyingActor, "hi", roomAttendingActorRef1.ref)
    assertResult(true)(result1._1 && result1._2 && result1._3 && result1._4)
    val result2 = testExistence(roomActorRef.underlyingActor, "hi1", roomAttendingActorRef1.ref)
    assertResult(false)(result2._2 || result2._4)
  }
  test("duplicated enter room with same id and same room attending actor") {
    val roomAttendingActorRef1 = TestProbe("roomAttendingActor1")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(true))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(false))
    val result1 = testExistence(roomActorRef.underlyingActor, "hi", roomAttendingActorRef1.ref)
    assertResult(true)(result1._1 && result1._2 && result1._3 && result1._4)
  }
  test("watch second room attending actor") {
    val roomAttendingActorRef1 = TestProbe("roomAttendingActor1")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(true))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    val roomAttendingActorRef2 = TestProbe("roomAttendingActor2")
    roomActorRef.tell(RoomActor.EnterRoom("hi2"), roomAttendingActorRef2.ref)
    roomAttendingActorRef2.expectMsg(RoomActor.EnteredRoom(true))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    assert(roomActorRef.underlyingActor.router.routees.contains(ActorRefRoutee(roomAttendingActorRef2.ref)))
    // hi2 가 stop 으로 퇴장했으므로, 남아있는 hi 는 알림을 받아야 한다.
    roomAttendingActorRef2.ref.tell(PoisonPill, testActor)
    assert(!roomActorRef.underlyingActor.router.routees.contains(ActorRefRoutee(roomAttendingActorRef2.ref)))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
  }
  test("success leave room as second room attending actor") {
    val roomAttendingActorRef1 = TestProbe("roomAttendingActor1")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(true))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    val roomAttendingActorRef2 = TestProbe("roomAttendingActor2")
    roomActorRef.tell(RoomActor.EnterRoom("hi2"), roomAttendingActorRef2.ref)
    roomAttendingActorRef2.expectMsg(RoomActor.EnteredRoom(true))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    // stop 으로 퇴장하지 않고 LeaveRoom 으로 퇴장한다.
    roomActorRef.tell(RoomActor.LeaveRoom, roomAttendingActorRef2.ref)
    roomAttendingActorRef2.expectMsg(RoomActor.LeavedRoom(true))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    val result = testExistence(roomActorRef.underlyingActor, "hi2", roomAttendingActorRef2.ref)
    assertResult(false)(result._1 || result._2 || result._3 || result._4)
    assert(!roomActorRef.underlyingActor.router.routees.contains(ActorRefRoutee(roomAttendingActorRef2.ref)))
  }
  test("success leave room as only one room attending actor") {
    val roomAttendingActorRef1 = TestProbe("roomAttendingActor1")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(true))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    watch(roomActorRef)
    roomActorRef.tell(RoomActor.LeaveRoom, roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.LeavedRoom(true))
    expectTerminated(roomActorRef)
  }
  test("success leave room as owner") {
    val roomAttendingActorRef1 = TestProbe("roomAttendingActor1")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(true))
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    val roomAttendingActorRef2 = TestProbe("roomAttendingActor2")
    roomActorRef.tell(RoomActor.EnterRoom("hi2"), roomAttendingActorRef2.ref)
    roomAttendingActorRef2.expectMsg(RoomActor.EnteredRoom(true))
    // hi 가 있는 방에, hi2 가 들어갔으므로, hi 와 hi2 모두, 새 사용자의 대화방 입장과 대화방 상태변경을 알림받게 된다.
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    assertResult(Some("hi"))(roomActorRef.underlyingActor.ownerId)
    roomActorRef.tell(RoomActor.LeaveRoom, roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.LeavedRoom(true))
    // 사용자2는, 방장이었던 사용자1 의 기존 사용자로서의 퇴장과, 방장의 변경, 대화방 상태 변경을 알림 받게 된다.
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    val result = testExistence(roomActorRef.underlyingActor, "hi", roomAttendingActorRef1.ref)
    assertResult(false)(result._1 || result._2 || result._3 || result._4)
    assert(!roomActorRef.underlyingActor.router.routees.contains(ActorRefRoutee(roomAttendingActorRef1.ref)))
    assertResult(Some("hi2"))(roomActorRef.underlyingActor.ownerId)
  }
  test("fail leave room") {
    val roomAttendingActorRef1 = TestProbe("roomAttendingActor1")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")))
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(true))
    val roomAttendingActorRef2 = TestProbe("roomAttendingActor2")
    roomActorRef.tell(RoomActor.LeaveRoom, roomAttendingActorRef2.ref)
    roomAttendingActorRef2.expectMsg(RoomActor.LeavedRoom(false))
  }
  // a in,b in,a out 순서로 입퇴장을 반복하여 각 단계마다 GetRoomStatusCTS 를 하기.
  // 방장 외에 운영자기능을 넣는 중에, 운영자를 표시할 수 있는 장소만 준비하고 운영자를 지정할 수 있는 기능은 준비하지 않았다.
  // 그래서, 대화방 정보를 구하면 운영자가 없는 대화방 정보를 받게 된다.
  // todo 운영자여부를 확인하는 검사는 운영자를 지정하는 기능을 한 뒤에 검사해야 한다.
  test("GetRoomStatusCTS/GotRoomStatusSTC with a in, b in, a out") {
    val roomAttendingActorRef1 = TestProbe("roomAttendingActor1")
    val roomMasterActorRef = TestProbe("roomMasterActorRef")
    val roomActorRef = TestActorRef[RoomActor](Props(new RoomActor(1, 1.seconds, Some("hi"), "subject")), roomMasterActorRef.ref)
    roomActorRef.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.EnteredRoom(true))
    val roomVersion1 = roomActorRef.underlyingActor.version
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomMasterActorRef.expectMsg(RoomMasterActor.PutRoomList(1, "hi", "subject", Map("hi" -> false)))
    roomActorRef.tell(Room.GetRoomStatusCTS, testActor)
    expectMsg(Room.GotRoomStatusSTC(roomVersion1, "hi", "subject", Map("hi" -> false)))
    val roomAttendingActorRef2 = TestProbe("roomAttendingActor2")
    roomActorRef.tell(RoomActor.EnterRoom("hi2"), roomAttendingActorRef2.ref)
    roomAttendingActorRef2.expectMsg(RoomActor.EnteredRoom(true))
    val roomVersion2 = roomActorRef.underlyingActor.version
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomMasterActorRef.expectMsg(RoomMasterActor.PutRoomList(1, "hi", "subject", Map("hi" -> false, "hi2" -> false)))
    roomActorRef.tell(Room.GetRoomStatusCTS, testActor)
    expectMsg(Room.GotRoomStatusSTC(roomVersion2, "hi", "subject", Map("hi" -> false, "hi2" -> false)))
    roomActorRef.tell(RoomActor.LeaveRoom, roomAttendingActorRef1.ref)
    roomAttendingActorRef1.expectMsg(RoomActor.LeavedRoom(true))
    val roomVersion3 = roomActorRef.underlyingActor.version
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    roomAttendingActorRef2.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomMasterActorRef.expectMsg(RoomMasterActor.PutRoomList(1, "hi2", "subject", Map("hi2" -> false)))
    roomActorRef.tell(Room.GetRoomStatusCTS, testActor)
    expectMsg(Room.GotRoomStatusSTC(roomVersion3, "hi2", "subject", Map("hi2" -> false)))
  }

}
