/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.route

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.http.scaladsl.model.{ HttpResponse, StatusCodes }
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Flow, Sink }
import akka.stream.testkit.{ TestSubscriber, TestPublisher }
import akka.stream.testkit.scaladsl.{ TestSink, TestSource }
import akka.testkit.{ TestProbe, TestActorRef, TestKit }
import org.imcharsi.jfxcp.common.onetimemessage.OneTimeMessage
import org.imcharsi.jfxcp.common.room.Room
import org.imcharsi.jfxcp.common.roomattending.RoomAttending
import org.imcharsi.jfxcp.common.roomattendingmaster.RoomAttendingMaster
import org.imcharsi.jfxcp.common.roommaster.RoomMaster
import org.imcharsi.jfxcp.common.websocket.WebSocket
import org.imcharsi.jfxcp.common.websocket.WebSocket._
import org.imcharsi.jfxcp.server.actor.auth.{ IdLookupActor, AuthenticateActor }
import org.imcharsi.jfxcp.server.actor.room.RoomMasterActor
import org.imcharsi.jfxcp.server.actor.user.UserMasterActor
import org.scalatest.FunSuiteLike
import spray.json._

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }
import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 7/25/16.
 */
class TestRouteTemplateWithoutRoute extends TestKit(ActorSystem()) with FunSuiteLike {
  implicit val actorMaterializer = ActorMaterializer()

  def dummyRetrieveF(correctId: List[String])(id: String, password: String): Future[Option[Unit]] = {
    Future {
      if (correctId.contains(id))
        Some(())
      else
        None
    }(Implicits.global)
  }

  test("verifyUserWorkerActor") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef.ref, 1.second, 1.second, 1.second)))
    // 편의를 위해서 실패를 뜻하는 HttpResponse 가 아닌, 예외를 던지도록 했다.
    // route 에서 이 예외를 잡아서 인증을 새로 시도한다.
    Try(Await.result(RouteTemplate.verifyUserWorkerActor(userMasterActorRef, 1), 10.seconds)) match {
      case Success(_) => fail()
      case Failure(_) =>
    }
    assertResult(StatusCodes.OK)(Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds).status)
    Try(Await.result(RouteTemplate.verifyUserWorkerActor(userMasterActorRef, 1), 10.seconds)) match {
      case Success(_) =>
      case Failure(_) => fail()
    }
  }

  // 중복접속이 잘 되는지 검사한다.
  test("integration test for duplicated login/logout") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef.ref, 1.second, 1.second, 1.second)))
    assertResult(StatusCodes.OK)(Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds).status)
    Await.result(RouteTemplate.verifyUserWorkerActor(userMasterActorRef, 1), 10.seconds)
    val webSocketFlow = Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 1), 10.seconds)
    val (testSource, testSink) =
      webSocketFlow
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        assertResult(GreetingSecondSTC)(string.parseJson.convertTo[WebSocketMessageSTC])
      case _ => fail()
    }
    val userWorkerActorRef1 = Await.result(system.actorSelection(userMasterActorRef.path / (1.toString)).resolveOne(10.seconds), 10.seconds)
    watch(userWorkerActorRef1)
    assertResult(StatusCodes.InternalServerError)(Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds).status)
    assertResult(StatusCodes.Unauthorized)(Await.result(RouteTemplate.tryLogin(userMasterActorRef, 2, "hi1", "", false), 10.seconds).status)
    // 바로 위에서 만든 actor 가 인증실패 후 완전히 종료하기 전에 아래에서 actor 만들기를 시도하게 될 수 있다.
    // 아래에서 0.5초를 기다렸는데, 기계의 성능에 따라 0.5초가 부족할 수도 있다.
    Thread.sleep(500)
    assertResult(StatusCodes.OK)(Await.result(RouteTemplate.tryLogin(userMasterActorRef, 2, "hi1", "", true), 10.seconds).status)
    expectTerminated(userWorkerActorRef1)
    testSink.expectComplete()
    Await.result(RouteTemplate.verifyUserWorkerActor(userMasterActorRef, 2), 10.seconds)
    // 앞서 중복접속으로 강제종료된 user worker actor 를 기준으로 web socket 접속을 시도해서 실패해야 한다.
    Try(Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 1), 10.seconds)) match {
      case Failure(_) =>
      case Success(_) => fail()
    }
    val (testSource2, testSink2) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 2), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    testSource2.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        assertResult(GreetingSecondSTC)(string.parseJson.convertTo[WebSocketMessageSTC])
      case _ => fail()
    }
    // 중복접속으로 종료된 user worker actor 를 logout 하려 시도하면 실패해야 한다.
    assertResult(StatusCodes.InternalServerError)(Await.result(RouteTemplate.tryLogout(userMasterActorRef, 1), 10.seconds).status)
    assertResult(StatusCodes.OK)(Await.result(RouteTemplate.tryLogout(userMasterActorRef, 2), 10.seconds).status)
    Thread.sleep(500)
    // 이미 종료한 user worker actor 를 logout 하려 시도하면 실패해야 한다.
    assertResult(StatusCodes.InternalServerError)(Await.result(RouteTemplate.tryLogout(userMasterActorRef, 2), 10.seconds).status)
  }

  test("integration test for onetime message") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef.ref, 1.second, 1.second, 1.second)))
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds)
    val (testSource, testSink) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 1), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink.requestNext()
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 2, "hi2", "", false), 10.seconds)
    val (testSource2, testSink2) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 2), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    testSource2.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink2.requestNext()

    // 사용자1 이 사용자2 에게 일회용 대화를 전달.
    val response =
      Await.result(
        RouteTemplate.tryOneTimeMessage(
          userMasterActorRef, 1,
          OneTimeMessage.OneTimeMessageCTO("hi2", "hi2 greeting")
        ), 10.seconds
      )
    // 전달되었음을 확인.
    assertResult(StatusCodes.OK)(response.status)
    val scheduledOneTimeMessageOTC =
      Await.result(
        response.entity.dataBytes
          .map(_.utf8String)
          .map(_.parseJson
            .convertTo[OneTimeMessage.OneTimeMessageResponseOTC]
            .asInstanceOf[OneTimeMessage.ScheduledOneTimeMessageOTC])
          .runWith(Sink.head), 10.seconds
      )

    // 사용자2 가 실제로 대화를 전달받기 시작한다.
    val oneTimeMessageSTC =
      testSink2.requestNext() match {
        case TextMessage.Strict(string) =>
          string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.OneTimeMessageSTC]
        case _ => fail()
      }
    assertResult("hi1")(oneTimeMessageSTC.id)
    assertResult("hi2 greeting")(oneTimeMessageSTC.chat)
    assertResult(scheduledOneTimeMessageOTC.userNo)(oneTimeMessageSTC.userNo)
    assertResult(scheduledOneTimeMessageOTC.msgNo)(oneTimeMessageSTC.msgNo)
    // 대화를 전달받았음을 응답해야 한다.
    testSource2.sendNext(
      WebSocket.TransferredOneTimeMessageCTS(oneTimeMessageSTC.userNo, oneTimeMessageSTC.msgNo)
      .asInstanceOf[WebSocketMessageCTS]
      .toJson.prettyPrint
    )
    // 사용자1 은, 사용자2 의 대화를 전달받았음에 대한 응답을 받는다.
    val oneTimeMessageAckSTC =
      testSink.requestNext() match {
        case TextMessage.Strict(string) =>
          val x = string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.OneTimeMessageAckSTC]
          assertResult(scheduledOneTimeMessageOTC.userNo)(x.userNo)
          assertResult(scheduledOneTimeMessageOTC.msgNo)(x.msgNo)
          x
        case _ => fail()
      }
    // 앞서 사용자1 이 받은 수신확인에 대해서도 응답을 해야 한다.
    testSource.sendNext(
      WebSocket.TransferredOneTimeMessageAckCTS(oneTimeMessageAckSTC.userNo, oneTimeMessageAckSTC.msgNo)
      .asInstanceOf[WebSocketMessageCTS]
      .toJson.prettyPrint
    )
    // 이 단계에 이르러, onetime message actor 의 queue 에서
    // TransferredOneTimeMessageAckCTS 가 지워졌음을 여기서 확인할 수 있는 방법은 없다.
    // 관련된 검사는 TestOneTimeMessageActor.about ScheduleOneTimeMessageAckOTO 이다.
  }

  test("room create/enter/leave") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(2.second)))
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 2.second, 2.second)))
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds)
    val (testSource, testSink) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 1), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink.requestNext()
    val result1 =
      Await.result(
        RouteTemplate.tryCreateRoom(
          userMasterActorRef, 1,
          RoomMaster.CreateRoomCTS("hi1", "hi1 room")
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    val enterResult =
      Await.result(
        RouteTemplate.tryEnterRoom(
          userMasterActorRef, 1,
          RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get)
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomStatusChangedSTC]
      case _ => fail()
    }
    Await.result(
      RouteTemplate.tryLeaveRoom(userMasterActorRef, 1, enterResult.raaId.get, RoomAttending.LeaveRoomCTS), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Await.result(
            entity.dataBytes
              .map(_.utf8String.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC].asInstanceOf[RoomAttending.LeavedRoomSTC])
              .runWith(Sink.head), 10.seconds
          )
        case _ => fail()
      }
  }

  // 이 검사는 아래의 중복 퇴장과 내용이 같았다. 이 검사를 하려면 대화방이 스스로 종료되도록 구성을 해야 하는데, 지금은 이러한 기능이 없다.
  // 이미 종료된 대화방에서 각종 명령을 입력했을 때 실패하는 검사를 해야 한다.
  //  test("already stopped room")

  test("duplicated enter room") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(2.second)))
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 2.second, 2.second)))
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds)
    val (testSource, testSink) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 1), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink.requestNext()
    val result1 =
      Await.result(
        RouteTemplate.tryCreateRoom(
          userMasterActorRef, 1,
          RoomMaster.CreateRoomCTS("hi1", "hi1 room")
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    val enterRoom =
      Await.result(
        RouteTemplate.tryEnterRoom(
          userMasterActorRef, 1,
          RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get)
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    Await.result(
      RouteTemplate.tryEnterRoom(
        userMasterActorRef, 1,
        RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get)
      ), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.BadRequest, _, _, _) =>
        case _ => fail()
      }
  }

  test("duplicated leave room") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(2.second)))
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 2.second, 2.second)))
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds)
    val (testSource, testSink) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 1), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink.requestNext()
    val result1 =
      Await.result(
        RouteTemplate.tryCreateRoom(
          userMasterActorRef, 1,
          RoomMaster.CreateRoomCTS("hi1", "hi1 room")
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    val enterResult =
      Await.result(
        RouteTemplate.tryEnterRoom(
          userMasterActorRef, 1,
          RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get)
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    Await.result(
      RouteTemplate.tryLeaveRoom(userMasterActorRef, 1, enterResult.raaId.get, RoomAttending.LeaveRoomCTS), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Await.result(
            entity.dataBytes
              .map(_.utf8String.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC].asInstanceOf[RoomAttending.LeavedRoomSTC])
              .runWith(Sink.head), 10.seconds
          )
        case _ => fail()
      }
    Await.result(
      RouteTemplate.tryLeaveRoom(userMasterActorRef, 1, enterResult.raaId.get, RoomAttending.LeaveRoomCTS), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.InternalServerError, _, _, _) =>
        case _ => fail()
      }
  }

  test("wrong enter room") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(2.second)))
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 2.second, 2.second)))
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds)
    val (testSource, testSink) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 1), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink.requestNext()
    Await.result(
      RouteTemplate.tryEnterRoom(
        userMasterActorRef, 1,
        RoomAttendingMaster.EnterRoomCTS(1)
      ), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.BadRequest, _, _, _) =>
        case _ => fail()
      }
  }

  test("create room time out") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(0.5.seconds)))
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 2.second, 2.second)))
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds)
    val (testSource, testSink) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 1), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink.requestNext()

    val result1 =
      Await.result(
        RouteTemplate.tryCreateRoom(
          userMasterActorRef, 1,
          RoomMaster.CreateRoomCTS("hi1", "hi1 room")
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }

    Thread.sleep(750)
    Await.result(
      RouteTemplate.tryEnterRoom(
        userMasterActorRef, 1,
        RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get)
      ), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.BadRequest, _, _, _) =>
        case _ => fail()
      }
  }

  test("conversation") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(2.second)))
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 2.second, 2.second)))
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds)
    val (testSource, testSink) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 1), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink.requestNext()
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 2, "hi2", "", false), 10.seconds)
    val (testSource2, testSink2) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 2), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS2: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource2.sendNext(greetingFirstCTS2.toJson.prettyPrint)
    testSink2.requestNext()
    val result1 =
      Await.result(
        RouteTemplate.tryCreateRoom(
          userMasterActorRef, 1,
          RoomMaster.CreateRoomCTS("hi1", "hi1 room")
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    val enterResult1 =
      Await.result(
        RouteTemplate.tryEnterRoom(
          userMasterActorRef, 1,
          RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get)
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomStatusChangedSTC]
      case _ => fail()
    }
    val enterResult2 =
      Await.result(
        RouteTemplate.tryEnterRoom(
          userMasterActorRef, 2,
          RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get)
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomStatusChangedSTC]
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomStatusChangedSTC]
      case _ => fail()
    }
    Await.result(
      RouteTemplate.tryVariousRequest(
        userMasterActorRef, 2,
        RoomAttending.ChatCTS("hi1"),
        enterResult2.raaId.get
      ), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
        case _ => fail()
      }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomChatSTC]
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomChatSTC]
      case _ => fail()
    }
    Await.result(
      RouteTemplate.tryLeaveRoom(userMasterActorRef, 1, enterResult1.raaId.get, RoomAttending.LeaveRoomCTS), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Await.result(
            entity.dataBytes
              .map(_.utf8String.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC].asInstanceOf[RoomAttending.LeavedRoomSTC])
              .runWith(Sink.head), 10.seconds
          )
        case _ => fail()
      }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomStatusChangedSTC]
      case _ => fail()
    }
    Await.result(
      RouteTemplate.tryLeaveRoom(userMasterActorRef, 2, enterResult2.raaId.get, RoomAttending.LeaveRoomCTS), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Await.result(
            entity.dataBytes
              .map(_.utf8String.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC].asInstanceOf[RoomAttending.LeavedRoomSTC])
              .runWith(Sink.head), 10.seconds
          )
        case _ => fail()
      }
  }

  test("logout and leave room") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(2.second)))
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 2.second, 2.second)))
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds)
    val (testSource, testSink) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 1), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink.requestNext()
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 2, "hi2", "", false), 10.seconds)
    val (testSource2, testSink2) =
      Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, 2), 10.seconds)
        .runWith(
          TestSource.probe[String]
            .via(Flow[String].map(x => TextMessage.Strict(x))),
          TestSink.probe[Message]
        )
    val greetingFirstCTS2: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource2.sendNext(greetingFirstCTS2.toJson.prettyPrint)
    testSink2.requestNext()
    val result1 =
      Await.result(
        RouteTemplate.tryCreateRoom(
          userMasterActorRef, 1,
          RoomMaster.CreateRoomCTS("hi1", "hi1 room")
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    val enterResult1 =
      Await.result(
        RouteTemplate.tryEnterRoom(
          userMasterActorRef, 1,
          RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get)
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomStatusChangedSTC]
      case _ => fail()
    }
    val enterResult2 =
      Await.result(
        RouteTemplate.tryEnterRoom(
          userMasterActorRef, 2,
          RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get)
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomStatusChangedSTC]
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomStatusChangedSTC]
      case _ => fail()
    }
    Await.result(
      RouteTemplate.tryVariousRequest(
        userMasterActorRef, 2,
        RoomAttending.ChatCTS("hi1"),
        enterResult2.raaId.get
      ), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
        case _ => fail()
      }
    testSink.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomChatSTC]
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomChatSTC]
      case _ => fail()
    }
    Await.result(RouteTemplate.tryLogout(userMasterActorRef, 1), 10.seconds) match {
      case HttpResponse(StatusCodes.OK, _, _, _) =>
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomAnnounceSTC]
      case _ => fail()
    }
    testSink2.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.RoomStatusChangedSTC]
      case _ => fail()
    }
    Await.result(
      RouteTemplate.tryLeaveRoom(userMasterActorRef, 2, enterResult2.raaId.get, RoomAttending.LeaveRoomCTS), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Await.result(
            entity.dataBytes
              .map(_.utf8String.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC].asInstanceOf[RoomAttending.LeavedRoomSTC])
              .runWith(Sink.head), 10.seconds
          )
        case _ => fail()
      }
  }

  test("list room/room status") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(2.second)))
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 2.second, 2.second)))
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds)
    assertResult(RoomMaster.ListedRoomSTC(List())) {
      Await.result(
        RouteTemplate.tryListRoom(roomMasterActorRef)
          .flatMap {
            case HttpResponse(StatusCodes.OK, _, entity, _) =>
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[(Int, String, String)])
                .fold(List[(Int, String, String)]())((l, x) => x :: l)
                .map(RoomMaster.ListedRoomSTC(_))
                .runWith(Sink.head)
          }(Implicits.global),
        10.seconds
      )
    }
    val result1 =
      Await.result(
        RouteTemplate.tryCreateRoom(
          userMasterActorRef, 1,
          RoomMaster.CreateRoomCTS("hi1", "hi1 room")
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    assertResult(RoomMaster.ListedRoomSTC(List())) {
      Await.result(
        RouteTemplate.tryListRoom(roomMasterActorRef)
          .flatMap {
            case HttpResponse(StatusCodes.OK, _, entity, _) =>
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[(Int, String, String)])
                .fold(List[(Int, String, String)]())((l, x) => x :: l)
                .map(RoomMaster.ListedRoomSTC(_))
                .runWith(Sink.head)
          }(Implicits.global),
        10.seconds
      )
    }
    val enterResult1 =
      Await.result(
        RouteTemplate.tryEnterRoom(
          userMasterActorRef, 1,
          RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get)
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }
    assertResult(RoomMaster.ListedRoomSTC(List((0, "hi1", "hi1 room")))) {
      Await.result(
        RouteTemplate.tryListRoom(roomMasterActorRef)
          .flatMap {
            case HttpResponse(StatusCodes.OK, _, entity, _) =>
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[(Int, String, String)])
                .fold(List[(Int, String, String)]())((l, x) => x :: l)
                .map(RoomMaster.ListedRoomSTC(_))
                .runWith(Sink.head)
          }(Implicits.global),
        10.seconds
      )
    }
    assertResult(Room.GotRoomStatusSTC(0, "hi1", "hi1 room", Map("hi1" -> false))) {
      Await.result(
        RouteTemplate.tryGetRoomStatus(roomMasterActorRef, 0)
          .flatMap {
            case HttpResponse(StatusCodes.OK, _, entity, _) =>
              Room.fromJson(entity.dataBytes).runWith(Sink.head)
          }(Implicits.global), 10.seconds
      ).copy(version = 0) // 부득이하게 이와 같이 version 을 바꾼다. version 까지 예상할 수는 없기 때문이다.
    }
    Await.result(
      RouteTemplate.tryLeaveRoom(userMasterActorRef, 1, enterResult1.raaId.get, RoomAttending.LeaveRoomCTS), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Await.result(
            entity.dataBytes
              .map(_.utf8String.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC].asInstanceOf[RoomAttending.LeavedRoomSTC])
              .runWith(Sink.head), 10.seconds
          )
        case _ => fail()
      }
    assertResult(RoomMaster.ListedRoomSTC(List())) {
      Await.result(
        RouteTemplate.tryListRoom(roomMasterActorRef)
          .flatMap {
            case HttpResponse(StatusCodes.OK, _, entity, _) =>
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[(Int, String, String)])
                .fold(List[(Int, String, String)]())((l, x) => x :: l)
                .map(RoomMaster.ListedRoomSTC(_))
                .runWith(Sink.head)
          }(Implicits.global),
        10.seconds
      )
    }
  }

  def webSocket(userMasterActorRef: ActorRef, sessionId: Int): (TestPublisher.Probe[String], TestSubscriber.Probe[Message]) = {
    Await.result(RouteTemplate.tryWebSocket(userMasterActorRef, sessionId), 10.seconds)
      .runWith(
        TestSource.probe[String]
          .via(Flow[String].map(x => TextMessage.Strict(x))),
        TestSink.probe[Message]
      )
  }
  def enterRoom(userMasterActorRef: ActorRef, sessionId: Int, roomNo: Int): RoomAttendingMaster.EnteredRoomSTC = {
    Await.result(
      RouteTemplate.tryEnterRoom(
        userMasterActorRef, sessionId,
        RoomAttendingMaster.EnterRoomCTS(roomNo)
      ), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Await.result(
            entity.dataBytes
              .map(_.utf8String.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC])
              .runWith(Sink.head), 10.seconds
          )
        case _ => fail()
      }
  }
  def responseFromAnnounce(stc: WebSocketMessageSTC): WebSocketMessageCTS = {
    stc match {
      case RoomAnnounceSTC(raaId, msgNo, _) =>
        TransferredRoomMessageCTS(raaId, msgNo)
      case _ => fail()
    }
  }
  def responseFromChat(stc: WebSocketMessageSTC): WebSocketMessageCTS = {
    stc match {
      case RoomChatSTC(raaId, msgNo, _, _) =>
        TransferredRoomMessageCTS(raaId, msgNo)
      case _ => fail()
    }
  }
  def responseFromRoomStatusChanged(stc: WebSocketMessageSTC): WebSocketMessageCTS = {
    stc match {
      case RoomStatusChangedSTC(raaId, msgNo) =>
        TransferredRoomMessageCTS(raaId, msgNo)
      case _ => fail()
    }
  }
  def expectWebSocketNext(sink: TestSubscriber.Probe[Message], source: TestPublisher.Probe[String], responseGenerator: WebSocketMessageSTC => WebSocketMessageCTS): WebSocketMessageSTC = {
    sink.requestNext() match {
      case TextMessage.Strict(string) =>
        val result = string.parseJson.convertTo[WebSocketMessageSTC]
        source.sendNext(responseGenerator(result).toJson.prettyPrint)
        result
      case _ => fail()
    }
  }

  // 이 검사가, room attending master actor 와 room attending actor 에 대해 바뀐 구상에 따른 새로운 검사다.
  test("leave room/lazy web socket") {
    val idLookupActorRef = TestActorRef(Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2", "hi3"))(_, _))))
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(2.second)))
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 2.second, 2.second)))

    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 1, "hi1", "", false), 10.seconds)
    val (testSource, testSink) = webSocket(userMasterActorRef, 1)
    val greetingFirstCTS: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource.sendNext(greetingFirstCTS.toJson.prettyPrint)
    testSink.requestNext()
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 2, "hi2", "", false), 10.seconds)
    val (testSource2, testSink2) = webSocket(userMasterActorRef, 2)
    val greetingFirstCTS2: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource2.sendNext(greetingFirstCTS2.toJson.prettyPrint)
    testSink2.requestNext()
    Await.result(RouteTemplate.tryLogin(userMasterActorRef, 3, "hi3", "", false), 10.seconds)
    val (testSource3, testSink3) = webSocket(userMasterActorRef, 3)
    val greetingFirstCTS3: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSource3.sendNext(greetingFirstCTS3.toJson.prettyPrint)
    testSink3.requestNext()

    val result1 =
      Await.result(
        RouteTemplate.tryCreateRoom(
          userMasterActorRef, 1,
          RoomMaster.CreateRoomCTS("hi1", "hi1 room")
        ), 10.seconds
      ) match {
          case HttpResponse(StatusCodes.OK, _, entity, _) =>
            Await.result(
              entity.dataBytes
                .map(_.utf8String.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC])
                .runWith(Sink.head), 10.seconds
            )
          case _ => fail()
        }

    val enterResult1 = enterRoom(userMasterActorRef, 1, result1.roomNo.get)
    expectWebSocketNext(testSink, testSource, responseFromAnnounce)
    expectWebSocketNext(testSink, testSource, responseFromRoomStatusChanged)
    val enterResult2 = enterRoom(userMasterActorRef, 2, result1.roomNo.get)
    expectWebSocketNext(testSink, testSource, responseFromAnnounce)
    expectWebSocketNext(testSink, testSource, responseFromRoomStatusChanged)
    expectWebSocketNext(testSink2, testSource2, responseFromAnnounce)
    expectWebSocketNext(testSink2, testSource2, responseFromRoomStatusChanged)
    val enterResult3 = enterRoom(userMasterActorRef, 3, result1.roomNo.get)
    expectWebSocketNext(testSink, testSource, responseFromAnnounce)
    expectWebSocketNext(testSink, testSource, responseFromRoomStatusChanged)
    expectWebSocketNext(testSink2, testSource2, responseFromAnnounce)
    expectWebSocketNext(testSink2, testSource2, responseFromRoomStatusChanged)
    expectWebSocketNext(testSink3, testSource3, responseFromAnnounce)
    expectWebSocketNext(testSink3, testSource3, responseFromRoomStatusChanged)

    testSource.sendComplete()
    testSink.expectComplete()

    Await.result(
      RouteTemplate.tryVariousRequest(
        userMasterActorRef, 2,
        RoomAttending.ChatCTS("hi1"),
        enterResult2.raaId.get
      ), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
        case _ => fail()
      }
    expectWebSocketNext(testSink2, testSource2, responseFromChat)
    expectWebSocketNext(testSink3, testSource3, responseFromChat)

    Await.result(
      RouteTemplate.tryLeaveRoom(userMasterActorRef, 2, enterResult2.raaId.get, RoomAttending.LeaveRoomCTS), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Await.result(
            entity.dataBytes
              .map(_.utf8String.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC].asInstanceOf[RoomAttending.LeavedRoomSTC])
              .runWith(Sink.head), 10.seconds
          )
        case _ => fail()
      }
    expectWebSocketNext(testSink3, testSource3, responseFromAnnounce)
    expectWebSocketNext(testSink3, testSource3, responseFromRoomStatusChanged)

    Await.result(
      RouteTemplate.tryVariousRequest(
        userMasterActorRef, 3,
        RoomAttending.ChatCTS("hi1"),
        enterResult3.raaId.get
      ), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
        case _ => fail()
      }
    expectWebSocketNext(testSink3, testSource3, responseFromChat)

    Await.result(
      RouteTemplate.tryLeaveRoom(userMasterActorRef, 1, enterResult1.raaId.get, RoomAttending.LeaveRoomCTS), 10.seconds
    ) match {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Await.result(
            entity.dataBytes
              .map(_.utf8String.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC].asInstanceOf[RoomAttending.LeavedRoomSTC])
              .runWith(Sink.head), 10.seconds
          )
        case _ => fail()
      }
    expectWebSocketNext(testSink3, testSource3, responseFromAnnounce)
    expectWebSocketNext(testSink3, testSource3, responseFromAnnounce)
    expectWebSocketNext(testSink3, testSource3, responseFromRoomStatusChanged)

    val (testSource1_1, testSink1_1) = webSocket(userMasterActorRef, 1)
    val greetingFirstCTS1_1: WebSocketMessageCTS = WebSocket.GreetingFirstCTS

    expectWebSocketNext(testSink1_1, testSource1_1, responseFromChat)
    expectWebSocketNext(testSink1_1, testSource1_1, responseFromAnnounce)
    expectWebSocketNext(testSink1_1, testSource1_1, responseFromRoomStatusChanged)
    expectWebSocketNext(testSink1_1, testSource1_1, responseFromChat)

    testSource1_1.sendNext(greetingFirstCTS1_1.toJson.prettyPrint)
    testSink1_1.requestNext()

  }

}
