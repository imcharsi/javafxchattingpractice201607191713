/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.auth

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestActorRef, TestKit }
import org.scalatest.FunSuiteLike

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.Future

/**
 * Created by i on 7/23/16.
 */
class TestIdLookupActor extends TestKit(ActorSystem()) with FunSuiteLike {
  def dummyRetrieveF(correctId: String)(id: String, password: String): Future[Option[Unit]] = {
    Future {
      if (id == correctId)
        Some(())
      else
        None
    }(Implicits.global)
  }

  test("update map") {
    val idLookupActor = TestActorRef[IdLookupActor](Props[IdLookupActor])
    idLookupActor.tell(IdLookupActor.UpdateMap(Map("hi" -> testActor)), testActor)
    assertResult(Map("hi" -> testActor))(idLookupActor.underlyingActor.mapIdToActorRef)
  }
  test("lookup") {
    val idLookupActor = TestActorRef[IdLookupActor](Props[IdLookupActor])
    idLookupActor.tell(IdLookupActor.UpdateMap(Map("hi" -> testActor)), testActor)
    idLookupActor.tell(IdLookupActor.Lookup("hi"), testActor)
    expectMsg(IdLookupActor.LookupResult(Some(testActor)))
    idLookupActor.tell(IdLookupActor.Lookup("hii"), testActor)
    expectMsg(IdLookupActor.LookupResult(None))
  }
  test("integration") {
    val idLookupActor = TestActorRef[IdLookupActor](Props[IdLookupActor])
    val authenticateActorRef = TestActorRef(Props(new AuthenticateActor[Unit](idLookupActor, dummyRetrieveF("hi")(_, _))))
    idLookupActor.tell(IdLookupActor.Lookup("hi"), testActor)
    expectMsg(IdLookupActor.LookupResult(None))
    authenticateActorRef.tell(AuthenticateActor.Authenticate("hi", "hi", false), testActor)
    expectMsg(AuthenticateActor.Authenticated(Some("hi".hashCode)))
    idLookupActor.tell(IdLookupActor.Lookup("hi"), testActor)
    expectMsg(IdLookupActor.LookupResult(Some(testActor)))
  }
}
