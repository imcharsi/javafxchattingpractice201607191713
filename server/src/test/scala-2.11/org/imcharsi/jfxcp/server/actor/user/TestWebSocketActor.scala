/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.user

import akka.actor.{ ActorSystem, Props }
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.stream.ActorMaterializer
import akka.stream.actor.{ ActorPublisher, ActorSubscriber }
import akka.stream.scaladsl.{ Flow, Sink, Source }
import akka.stream.testkit.scaladsl.{ TestSink, TestSource }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import org.imcharsi.jfxcp.common.websocket.WebSocket
import org.imcharsi.jfxcp.common.websocket.WebSocket._
import org.imcharsi.jfxcp.server.actor.user.WebSocketActor.TimeoutException
import org.scalatest.FunSuiteLike
import spray.json._

import scala.collection.immutable.Queue
import scala.concurrent.duration._

/**
 * Created by i on 7/25/16.
 */
class TestWebSocketActor extends TestKit(ActorSystem()) with FunSuiteLike {
  implicit val actorMaterializer = ActorMaterializer()

  test("success GreetingFirstCTS/GreetingSecondSTC") {
    val oneTimeMessageActor = TestProbe("oneTimeMessageActor")
    val webSocketActorRef = TestActorRef[WebSocketActor](Props(new WebSocketActor(oneTimeMessageActor.ref, testActor, 2.seconds)))
    val testFlow = Flow.fromSinkAndSource(
      Sink.fromSubscriber(ActorSubscriber[String](webSocketActorRef)),
      Source.fromPublisher(ActorPublisher[Message](webSocketActorRef))
    )
    val (testSourceProbe, testSinkProbe) = testFlow.runWith(TestSource.probe[String], TestSink.probe[Message])
    val data1: WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    testSourceProbe.sendNext(data1.toJson.prettyPrint)
    testSinkProbe.requestNext() match {
      case TextMessage.Strict(string) =>
        assertResult(WebSocket.GreetingSecondSTC)(string.parseJson.convertTo[WebSocketMessageSTC])
      case _ => fail()
    }
  }

  test("fail GreetingFirstCTS/GreetingSecondSTC") {
    val oneTimeMessageActor = TestProbe("oneTimeMessageActor")
    val webSocketActorRef = TestActorRef[WebSocketActor](Props(new WebSocketActor(oneTimeMessageActor.ref, testActor, 2.seconds)))
    val testFlow = Flow.fromSinkAndSource(
      Sink.fromSubscriber(ActorSubscriber[String](webSocketActorRef)),
      Source.fromPublisher(ActorPublisher[Message](webSocketActorRef))
    )
    val (testSourceProbe, testSinkProbe) = testFlow.runWith(TestSource.probe[String], TestSink.probe[Message])
    watch(webSocketActorRef)
    testSinkProbe.expectSubscription()
    assertResult(classOf[TimeoutException])(testSinkProbe.expectError().getClass)
    expectTerminated(webSocketActorRef)
  }

  // 아래의 검사에서는 시간초과를 전부 생략한다.
  // 주어진 message 쌍이 구상한대로 교환된다는 것을 확인하는 것이 검사의 목표이다.
  // 검사하는 데 소요되는 시간이 워낙 짧아서 시간초과범위 안에 검사가 끝난다.
  test("about OneTimeMessage") {
    val oneTimeMessageActor = TestProbe("oneTimeMessageActor")
    val webSocketActorRef = TestActorRef[WebSocketActor](Props(new WebSocketActor(oneTimeMessageActor.ref, testActor, 3.seconds)))
    val testFlow = Flow.fromSinkAndSource(
      Sink.fromSubscriber(ActorSubscriber[String](webSocketActorRef)),
      Source.fromPublisher(ActorPublisher[Message](webSocketActorRef))
    )
    val (testSourceProbe, testSinkProbe) = testFlow.runWith(TestSource.probe[String], TestSink.probe[Message])
    val data1 = WebSocketActor.QueueOneTimeMessageOTW("hi1", "hi2 greeting", 1, 1)
    webSocketActorRef.tell(data1, oneTimeMessageActor.ref)
    testSinkProbe.requestNext() match {
      case TextMessage.Strict(string) =>
        assertResult(WebSocket.OneTimeMessageSTC("hi1", "hi2 greeting", 1, 1))(string.parseJson.convertTo[WebSocketMessageSTC])
      case _ => fail()
    }
    val data2: WebSocketMessageCTS = WebSocket.TransferredOneTimeMessageCTS(1, 1)
    testSourceProbe.sendNext(data2.toJson.prettyPrint)
    oneTimeMessageActor.expectMsg(WebSocketActor.TransferredOneTimeMessageWTO(1, 1))
  }

  test("about OneTimeMessageAck") {
    val oneTimeMessageActor = TestProbe("oneTimeMessageActor")
    val webSocketActorRef = TestActorRef[WebSocketActor](Props(new WebSocketActor(oneTimeMessageActor.ref, testActor, 2.seconds)))
    val testFlow = Flow.fromSinkAndSource(
      Sink.fromSubscriber(ActorSubscriber[String](webSocketActorRef)),
      Source.fromPublisher(ActorPublisher[Message](webSocketActorRef))
    )
    val (testSourceProbe, testSinkProbe) = testFlow.runWith(TestSource.probe[String], TestSink.probe[Message])
    val data1 = WebSocketActor.QueueOneTimeMessageAckOTW(1, 1)
    webSocketActorRef.tell(data1, oneTimeMessageActor.ref)
    testSinkProbe.requestNext() match {
      case TextMessage.Strict(string) =>
        assertResult(WebSocket.OneTimeMessageAckSTC(1, 1))(string.parseJson.convertTo[WebSocketMessageSTC])
      case _ => fail()
    }
    val data2: WebSocketMessageCTS = WebSocket.TransferredOneTimeMessageAckCTS(1, 1)
    testSourceProbe.sendNext(data2.toJson.prettyPrint)
    oneTimeMessageActor.expectMsg(WebSocketActor.TransferredOneTimeMessageAckWTO(1, 1))
  }

  test("about BatchQueueOneTimeMessageOTW") {
    val oneTimeMessageActor = TestProbe("oneTimeMessageActor")
    val webSocketActorRef = TestActorRef[WebSocketActor](Props(new WebSocketActor(oneTimeMessageActor.ref, testActor, 3.seconds)))
    val testFlow = Flow.fromSinkAndSource(
      Sink.fromSubscriber(ActorSubscriber[String](webSocketActorRef)),
      Source.fromPublisher(ActorPublisher[Message](webSocketActorRef))
    )
    val (testSourceProbe, testSinkProbe) = testFlow.runWith(TestSource.probe[String], TestSink.probe[Message])
    val data1 =
      WebSocketActor.BatchQueueToWebSocketActor(
        Queue(
          WebSocketActor.QueueOneTimeMessageOTW("hi1", "hi2 greeting", 1, 1),
          WebSocketActor.QueueOneTimeMessageAckOTW(1, 1)
        )
      )
    webSocketActorRef.tell(data1, oneTimeMessageActor.ref)
    testSinkProbe.requestNext() match {
      case TextMessage.Strict(string) =>
        assertResult(WebSocket.OneTimeMessageSTC("hi1", "hi2 greeting", 1, 1))(string.parseJson.convertTo[WebSocketMessageSTC])
      case _ => fail()
    }
    testSinkProbe.requestNext() match {
      case TextMessage.Strict(string) =>
        assertResult(WebSocket.OneTimeMessageAckSTC(1, 1))(string.parseJson.convertTo[WebSocketMessageSTC])
      case _ => fail()
    }
  }

}
