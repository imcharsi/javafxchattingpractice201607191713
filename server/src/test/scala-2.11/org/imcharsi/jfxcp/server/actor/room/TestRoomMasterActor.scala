/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.room

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestProbe, TestActorRef, TestKit }
import org.imcharsi.jfxcp.common.roommaster.RoomMaster
import org.scalatest.FunSuiteLike

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._

/**
 * Created by i on 8/1/16.
 */
class TestRoomMasterActor extends TestKit(ActorSystem()) with FunSuiteLike {
  test("success CreateRoomCTS") {
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.seconds)))
    roomMasterActorRef.tell(RoomMaster.CreateRoomCTS("hi", "hihi"), testActor)
    val result = expectMsgType[RoomMaster.CreatedRoomSTC]
    assert(result.roomNo.isDefined)
    val roomAttendingActorRef = TestProbe("roomAttendingActorRef")
    result.roomNo.foreach { roomNo =>
      system
        .actorSelection(roomMasterActorRef.path / roomNo.toString)
        .resolveOne(10.seconds)
        .foreach(_.tell(RoomActor.EnterRoom("hi"), roomAttendingActorRef.ref))(Implicits.global)
    }
    val result2 = roomAttendingActorRef.expectMsgType[RoomActor.EnteredRoom]
    assert(result2.result)
  }
  // 4개의 기능이 서로 관련이 있어서 묶어서 검사하는 것이 실용적이다.
  test("ListRoomCTS/ListedRoomCTS/PutRoomList/DeleteRoomList") {
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.seconds)))
    roomMasterActorRef.tell(RoomMaster.ListRoomCTS, testActor)
    expectMsg(RoomMaster.ListedRoomSTC(List()))
    roomMasterActorRef.tell(RoomMasterActor.PutRoomList(1, "1", "2", Map("3" -> true, "4" -> false)), testActor)
    roomMasterActorRef.tell(RoomMaster.ListRoomCTS, testActor)
    expectMsg(RoomMaster.ListedRoomSTC(List((1, "1", "2"))))
    roomMasterActorRef.tell(RoomMasterActor.PutRoomList(2, "2", "3", Map("5" -> true, "6" -> false)), testActor)
    roomMasterActorRef.tell(RoomMaster.ListRoomCTS, testActor)
    expectMsg(RoomMaster.ListedRoomSTC(List((1, "1", "2"), (2, "2", "3"))))
    roomMasterActorRef.tell(RoomMasterActor.PutRoomList(1, "3", "4", Map("4" -> false)), testActor)
    roomMasterActorRef.tell(RoomMaster.ListRoomCTS, testActor)
    expectMsg(RoomMaster.ListedRoomSTC(List((1, "3", "4"), (2, "2", "3"))))
    roomMasterActorRef.tell(RoomMasterActor.DeleteRoomList(1), testActor)
    roomMasterActorRef.tell(RoomMaster.ListRoomCTS, testActor)
    expectMsg(RoomMaster.ListedRoomSTC(List((2, "2", "3"))))
  }
}
