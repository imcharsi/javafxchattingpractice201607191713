/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.user

import akka.actor.Actor.Receive
import akka.actor._
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.stream.ActorMaterializer
import akka.stream.actor.{ ActorPublisher, ActorSubscriber }
import akka.stream.scaladsl.{ Flow, Sink, Source }
import akka.stream.testkit.scaladsl.{ TestSink, TestSource }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import org.imcharsi.jfxcp.common.websocket.WebSocket
import org.imcharsi.jfxcp.common.websocket.WebSocket.{ TransferredOneTimeMessageCTS, _ }
import org.imcharsi.jfxcp.server.actor.auth.{ AuthenticateActor, IdLookupActor }
import org.scalatest.FunSuiteLike
import spray.json._

import scala.collection.immutable.Queue
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }

/**
 * Created by i on 7/24/16.
 */
class TestOneTimeMessageActor extends TestKit(ActorSystem()) with FunSuiteLike {
  implicit val actorMaterializer = ActorMaterializer()

  def dummyRetrieveF(correctId: List[String])(id: String, password: String): Future[Option[Unit]] = {
    Future {
      if (correctId.contains(id))
        Some(())
      else
        None
    }(Implicits.global)
  }

  test("about OneTimeMessageCTO") {
    val idLookupActorRef = TestProbe("idLookupActorRef")
    val userWorkerActorRef1 = TestProbe("userWorkerActorRef1")
    val oneTimeMessageActorRef = TestActorRef[OneTimeMessageActor](Props(new OneTimeMessageActor(idLookupActorRef.ref, userWorkerActorRef1.ref, 1, "hi1")))
    val userWorkerActorRef2 = TestProbe("userWorkerActorRef2")
    val oneTimeMessageActorRef2 = TestProbe("oneTimeMessageActorRef2")
    val xxx = TestActorRef[Xxx](Props(new Xxx(oneTimeMessageActorRef2.ref)), userWorkerActorRef2.ref, UserWorkerActor.nameOneTimeMessageActor)
    val webSocketActorRef = TestProbe("webSocketActorRef")
    oneTimeMessageActorRef.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorRef.ref), testActor)
    webSocketActorRef.expectMsg(WebSocketActor.BatchQueueToWebSocketActor(Queue()))
    assertResult(Some(webSocketActorRef.ref))(oneTimeMessageActorRef.underlyingActor.webSocketActorRef)
    oneTimeMessageActorRef.tell(OneTimeMessageActor.OneTimeMessageCTO("hi2", "hi2 greeting"), testActor)
    idLookupActorRef.expectMsg(IdLookupActor.Lookup("hi2"))
    // 아래와 같이 lastSender 에 대해 답을 해야 한다. 왜냐하면 ask 로 물어봤기 때문이다.
    // ask pattern 은 context 의 self 를 사용하지 않고 다른 actor ref 를 사용한다.
    idLookupActorRef.lastSender.tell(IdLookupActor.LookupResult(Some(userWorkerActorRef2.ref)), idLookupActorRef.ref)
    // test probe 가 특정 위치에 오도록 하는 기능은 없고, 대안은 있다.
    // impo http://stackoverflow.com/questions/21285260/giving-an-actorpath-for-a-testprobe
    // 왜 이와 같은 문제가 생기는가. 다른 사용자에게 onetime message 를 보낼 때 먼저 다른 사용자의 onetime message actor 를 찾는데
    // 이 때 찾는 방법으로, 찾는 용도로 쓸 message 를 명시적으로 만들어서 쓰는 방법과 actorSelection 을 활용하는 방법이 있다.
    // actorSelection 만으로도 충분하지만 검사를 하기가 쉽지 않은데, actorSelection 기능을 사용해서
    // userWorkerActorRef2.path / UserWorkerActor.nameOneTimeMessageActor 를 찾는 시도가 있었다는 것을
    // 어떻게 확인할 것이며, 이에 대한 응답을 어떻게 조작할 것인가.
    // 그래서 test probe 가 특정위치에 오도록 해야 검사를 할 수 있는 것이다.
    // 그런데 actorSelection 을 할 때 사용되는 message 인 Identity 는 내부에서 처리되기 때문에 사용자 단계에서는 볼 수 없다.
    // 결국, 이와 같이 해도 onetime message actor 1 이 onetime message actor 2 를 찾는 시도를 했다는 것을 확인하는 방법은 없다.
    // 그래서, 먼저 onetime message actor 1 가 시도하는 것과 같은 방법으로 여기서 찾기를 시도해서 잘 찾았음을 확인한다.
    // 이 시도는 onetime message actor 1 이 찾기 시도를 했다는 것을 검사하는 것이 아니고
    // 만약 onetime message actor 1 이 이와 같은 방법으로 시도를 한다면 성공할 것이라는 검사이기 때문에 정확한 검사는 아니다.
    // 이어서, 잘 찾았음을 전제로 하는 다른 검사를 이어서 해서 문제없으니 앞단계에서 잘 찾았을 것이라고 추정하는 방법밖에 없다.
    // 다른 방법을 모르겠다.
    assertResult(xxx)(
      Await.result(
        system.actorSelection(userWorkerActorRef2.ref.path / UserWorkerActor.nameOneTimeMessageActor)
          .resolveOne(10.seconds), 10.seconds
      )
    )
    // OneTimeMessageCTO 를 보내면 user worker actor 의 시간초과는 갱신되어야 한다.
    userWorkerActorRef1.expectMsg(UserWorkerActor.RefreshTimeout)
    // 햇갈리는 구절이다.
    // onetime message actor 1 은 onetime message actor2 로 ask 로 물어봤기 때문에 lastSender 로 답을 해야 한다.
    // 한편, onetime message actor 2 가 web socket 전송결과를 전달할 때 쓸 actor ref 는
    // ask lastSender 가 아니라, onetime message actor 1 이 되어야 한다.
    val (actorRef1, message1) = oneTimeMessageActorRef2.expectMsgType[(ActorRef, OneTimeMessageActor.ScheduleOneTimeMessageOTO)]
    assertResult(oneTimeMessageActorRef)(actorRef1)
    assertResult(message1.userNo)(1)
    assertResult("hi1")(message1.senderId)
    // OneTimeMessageCTO 에서는 받는 사용자 id 가 입력되었지만
    // ScheduleOneTimeMessageOTO 부터는 보내는 사용자 id 가 입력됨을 검사한다.
    // 이후 단계부터는 보내는 사용자 id 에 관한 검사를 할 필요가 없다. 그대로 복사해서 넘겨주면 되기 때문이다.
    assertResult("hi1")(message1.senderId)
    oneTimeMessageActorRef2.lastSender.tell(OneTimeMessageActor.ScheduledOneTimeMessageOTO(message1.userNo, message1.msgNo), oneTimeMessageActorRef2.ref)
    expectMsg(OneTimeMessageActor.ScheduledOneTimeMessageOTC(message1.userNo, message1.msgNo))
  }
  test("about ScheduledOneTimeMessageOTC") {
    val userWorkerActorRef2 = TestProbe("userWorkerActorRef2")
    val oneTimeMessageActorRef2 = TestActorRef[OneTimeMessageActor](Props(new OneTimeMessageActor(testActor, userWorkerActorRef2.ref, 2, "hi2")))
    val webSocketActorRef2 = TestProbe("webSocketActorRef2")
    val oneTimeMessageActorRef1 = TestProbe("oneTimeMessageActorRef1")
    oneTimeMessageActorRef2.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorRef2.ref), testActor)
    webSocketActorRef2.expectMsg(WebSocketActor.BatchQueueToWebSocketActor(Queue()))
    // ask pattern 으로 물은 것과 같이 해야 한다.
    // ask pattern 에 따라 답을 받게 될 위치를 tell 의 sender 로 두고
    // web socket 전송결과를 전달받을 위치를 tuple 안에 써야 구상에 맞다.
    oneTimeMessageActorRef2.tell((oneTimeMessageActorRef1.ref, OneTimeMessageActor.ScheduleOneTimeMessageOTO(1, 1, "hi1", "hi2 greeting")), testActor)
    expectMsg(OneTimeMessageActor.ScheduledOneTimeMessageOTO(1, 1))
    webSocketActorRef2.expectMsg(WebSocketActor.QueueOneTimeMessageOTW("hi1", "hi2 greeting", 1, 1))
    oneTimeMessageActorRef2.tell(WebSocketActor.TransferredOneTimeMessageWTO(1, 1), webSocketActorRef2.ref)
    oneTimeMessageActorRef1.expectMsg(OneTimeMessageActor.ScheduleOneTimeMessageAckOTO(1, 1))
  }
  test("about ScheduleOneTimeMessageAckOTO") {
    val userWorkerActorRef1 = TestProbe("userWorkerActorRef1")
    val oneTimeMessageActorRef1 = TestActorRef[OneTimeMessageActor](Props(new OneTimeMessageActor(testActor, userWorkerActorRef1.ref, 1, "hi1")))
    val webSocketActorRef1 = TestProbe("webSocketActorRef1")
    oneTimeMessageActorRef1.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorRef1.ref), testActor)
    webSocketActorRef1.expectMsg(WebSocketActor.BatchQueueToWebSocketActor(Queue()))
    val oneTimeMessageActorRef2 = TestProbe("oneTimeMessageActorRef2")
    oneTimeMessageActorRef1.tell(OneTimeMessageActor.ScheduleOneTimeMessageAckOTO(1, 1), oneTimeMessageActorRef2.ref)
    webSocketActorRef1.expectMsg(WebSocketActor.QueueOneTimeMessageAckOTW(1, 1))
    assertResult(false)(oneTimeMessageActorRef1.underlyingActor.queue.isEmpty)
    oneTimeMessageActorRef1.tell(WebSocketActor.TransferredOneTimeMessageAckWTO(1, 1), webSocketActorRef1.ref)
    assertResult(true)(oneTimeMessageActorRef1.underlyingActor.queue.isEmpty)
  }

  test("integration test") {
    val idLookupActorRef = TestActorRef[IdLookupActor](Props[IdLookupActor])
    val authenticateActorRef = system.actorOf(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2")))))
    val roomMasterActorRef = TestProbe("roomMasterActor")
    // hi1 사용자 접속
    val userWorkerActorRef1 = TestActorRef[UserWorkerActor](Props(new UserWorkerActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef.ref, 10.seconds, 10.seconds, 10.seconds)))
    userWorkerActorRef1.tell(UserWorkerActor.Authenticate("hi1", "", false), testActor)
    userWorkerActorRef1.tell(AuthenticateActor.Authenticated(Some("hi1".hashCode)), authenticateActorRef)
    expectMsg(UserWorkerActor.Authenticated(true))
    userWorkerActorRef1.tell(UserWorkerActor.Init, testActor)
    expectMsg(UserWorkerActor.Inited(true))
    userWorkerActorRef1.tell(UserWorkerActor.CreateWebSocketActor, testActor)
    val webSocketActorRef1 = expectMsgType[UserWorkerActor.CreatedWebSocketActor].webSocketActorRef
    // hi2 사용자 접속
    val userWorkerActorRef2 = TestActorRef[UserWorkerActor](Props(new UserWorkerActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef.ref, 10.seconds, 10.seconds, 10.seconds)))
    userWorkerActorRef2.tell(UserWorkerActor.Authenticate("hi2", "", false), testActor)
    userWorkerActorRef2.tell(AuthenticateActor.Authenticated(Some("hi2".hashCode)), authenticateActorRef)
    expectMsg(UserWorkerActor.Authenticated(true))
    userWorkerActorRef2.tell(UserWorkerActor.Init, testActor)
    expectMsg(UserWorkerActor.Inited(true))
    userWorkerActorRef2.tell(UserWorkerActor.CreateWebSocketActor, testActor)
    expectMsgType[UserWorkerActor.CreatedWebSocketActor]
    // hi1 이 onetime message actor 찾기
    val oneTimeMessageActorRef1 = Await.result(system.actorSelection(userWorkerActorRef1.path / UserWorkerActor.nameOneTimeMessageActor).resolveOne(10.seconds), 10.seconds)
    val clientActorRef = TestProbe("clientAskActorRef")

    // hi1 사용자가 web socket 접속
    val (webSocketActorPublisher1, webSocketActorSubscriber1) =
      Flow
        .fromSinkAndSource(
          Sink.fromSubscriber(ActorSubscriber[String](userWorkerActorRef1.underlyingActor.webSocketActorRef.get)),
          Source.fromPublisher(ActorPublisher[Message](userWorkerActorRef1.underlyingActor.webSocketActorRef.get))
        )
        .runWith(TestSource.probe[String], TestSink.probe[Message])
    webSocketActorPublisher1.sendNext(WebSocket.GreetingFirstCTS.asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint)
    webSocketActorSubscriber1.requestNext() match {
      case TextMessage.Strict(string) =>
        assertResult(WebSocket.GreetingSecondSTC)(string.parseJson.convertTo[WebSocketMessageSTC])
      case _ => fail()
    }
    // hi2 사용자가 web socket 접속
    val (webSocketActorPublisher2, webSocketActorSubscriber2) =
      Flow
        .fromSinkAndSource(
          Sink.fromSubscriber(ActorSubscriber[String](userWorkerActorRef2.underlyingActor.webSocketActorRef.get)),
          Source.fromPublisher(ActorPublisher[Message](userWorkerActorRef2.underlyingActor.webSocketActorRef.get))
        )
        .runWith(TestSource.probe[String], TestSink.probe[Message])
    webSocketActorPublisher2.sendNext(WebSocket.GreetingFirstCTS.asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint)
    webSocketActorSubscriber2.requestNext() match {
      case TextMessage.Strict(string) =>
        assertResult(WebSocket.GreetingSecondSTC)(string.parseJson.convertTo[WebSocketMessageSTC])
      case _ => fail()
    }
    // 아래의 message 전달을 web socket 연결보다 다음에 해야 한다.
    // 아니면 검사가 복잡해지는데, greeting 보다 onetime message 가 먼저 오고가는 문제가 생긴다.
    // 이는 기능의 문제는 아니고 단순히 검사의 편의에 관한 문제이다.
    // hi1 이 hi2 로 onetime message 보내기.
    val oldCancellableTimeout = userWorkerActorRef1.underlyingActor.cancellableTimeout
    assert(oldCancellableTimeout.map(_.isCancelled).contains(false))
    oneTimeMessageActorRef1.tell(OneTimeMessageActor.OneTimeMessageCTO("hi2", "hi2 greeting"), clientActorRef.ref)
    // hi2 의 onetime message actor 로 onetime message 가 전달되었음을 확인받기
    val result1 = clientActorRef.expectMsgType[OneTimeMessageActor.ScheduledOneTimeMessageOTC]
    assertResult("hi1".hashCode)(result1.userNo)
    // user worker actor 의 시간초과가 갱신되었음을 확인.
    assert(oldCancellableTimeout.map(_.isCancelled).contains(true))
    assert(userWorkerActorRef1.underlyingActor.cancellableTimeout.map(_.isCancelled).contains(false))
    // 없는 사용자에 대한 onetime message 전송은 실패
    oneTimeMessageActorRef1.tell(OneTimeMessageActor.OneTimeMessageCTO("hi3", "hi3 greeting"), clientActorRef.ref)
    clientActorRef.expectMsg(OneTimeMessageActor.FailedOneTimeMessageOTC)
    // hi2 사용자가 web socket 으로 자료전송을 요청하여, 자료를 전달.
    val result2 =
      webSocketActorSubscriber2.requestNext() match {
        case TextMessage.Strict(string) =>
          string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[OneTimeMessageSTC]
        case _ => fail()
      }
    // hi2 사용자가 자료를 전송받았음을 확인함
    webSocketActorPublisher2.sendNext(TransferredOneTimeMessageCTS(result2.userNo, result2.msgNo).asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint)
    // hi2 사용자가 보낸 확인 자료가 hi2 web socket actor 를 거쳐서 hi2 onetime message actor 를 거쳐서
    // hi1 onetime message actor 를 거쳐서 hi1 web socket actor 까지 왔음을 확인
    // hi1 사용자가 web socket 으로 자료전송을 요청하여, 받아둔 자료를 전달
    val result3 = webSocketActorSubscriber1.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[OneTimeMessageAckSTC]
      case _ => fail()
    }
    // hi1 사용자가 자료를 전송받았음을 확인함
    webSocketActorPublisher1.sendNext(TransferredOneTimeMessageAckCTS(result3.userNo, result3.msgNo).asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint)
  }

  // 자기 자신에게 보낸 onetime message 가 잘 도착하는지 확인한다.
  test("integration test about self-onetime message") {
    val idLookupActorRef = TestActorRef[IdLookupActor](Props[IdLookupActor])
    val authenticateActorRef = system.actorOf(Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2")))))
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userWorkerActorRef1 = TestActorRef[UserWorkerActor](Props(new UserWorkerActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef.ref, 10.seconds, 10.seconds, 10.seconds)))
    userWorkerActorRef1.tell(UserWorkerActor.Authenticate("hi1", "", false), testActor)
    userWorkerActorRef1.tell(AuthenticateActor.Authenticated(Some("hi1".hashCode)), authenticateActorRef)
    expectMsg(UserWorkerActor.Authenticated(true))
    userWorkerActorRef1.tell(UserWorkerActor.Init, testActor)
    expectMsg(UserWorkerActor.Inited(true))
    userWorkerActorRef1.tell(UserWorkerActor.CreateWebSocketActor, testActor)
    expectMsgType[UserWorkerActor.CreatedWebSocketActor].webSocketActorRef
    val oneTimeMessageActorRef1 = Await.result(system.actorSelection(userWorkerActorRef1.path / UserWorkerActor.nameOneTimeMessageActor).resolveOne(10.seconds), 10.seconds)
    val clientActorRef = TestProbe("clientAskActorRef")
    val (webSocketActorPublisher1, webSocketActorSubscriber1) =
      Flow
        .fromSinkAndSource(
          Sink.fromSubscriber(ActorSubscriber[String](userWorkerActorRef1.underlyingActor.webSocketActorRef.get)),
          Source.fromPublisher(ActorPublisher[Message](userWorkerActorRef1.underlyingActor.webSocketActorRef.get))
        )
        .runWith(TestSource.probe[String], TestSink.probe[Message])
    webSocketActorPublisher1.sendNext(WebSocket.GreetingFirstCTS.asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint)
    webSocketActorSubscriber1.requestNext() match {
      case TextMessage.Strict(string) =>
        assertResult(WebSocket.GreetingSecondSTC)(string.parseJson.convertTo[WebSocketMessageSTC])
      case _ => fail()
    }
    oneTimeMessageActorRef1.tell(OneTimeMessageActor.OneTimeMessageCTO("hi1", "hi1 greeting"), clientActorRef.ref)
    val result1 = clientActorRef.expectMsgType[OneTimeMessageActor.ScheduledOneTimeMessageOTC]
    assertResult("hi1".hashCode)(result1.userNo)
    // 발신자이면서 수신자이기도 한 자신의 web socket 으로 자료전송을 요청하여, 자료를 전달.
    val result2 =
      webSocketActorSubscriber1.requestNext() match {
        case TextMessage.Strict(string) =>
          string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[OneTimeMessageSTC]
        case _ => fail()
      }
    // hi1 사용자는, 자신이 보낸 자료를 받았음을 확인응답함.
    webSocketActorPublisher1.sendNext(TransferredOneTimeMessageCTS(result2.userNo, result2.msgNo).asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint)
    // 자신이 보낸 자료를 수신자인 자신이 받았음에 대한 확인응답을 수신자인 자신의 onetime message actor 가 받으면
    // 다시 발신자인 자신의 onetime message actor 로 이 응답을 중개해주고 여기서부터는 위의 검사와 동일하다.
    val result3 = webSocketActorSubscriber1.requestNext() match {
      case TextMessage.Strict(string) =>
        string.parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[OneTimeMessageAckSTC]
      case _ => fail()
    }
    webSocketActorPublisher1.sendNext(TransferredOneTimeMessageAckCTS(result3.userNo, result3.msgNo).asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint)
  }
  test("watch web socket actor") {
    val idLookupActorRef = TestProbe("idLookupActorRef")
    val userWorkerActorRef1 = TestProbe("userWorkerActorRef1")
    val oneTimeMessageActorRef = TestActorRef[OneTimeMessageActor](Props(new OneTimeMessageActor(idLookupActorRef.ref, userWorkerActorRef1.ref, 1, "hi1")))
    val webSocketActorRef = TestProbe("webSocketActorRef")
    oneTimeMessageActorRef.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorRef.ref), testActor)
    webSocketActorRef.expectMsg(WebSocketActor.BatchQueueToWebSocketActor(Queue()))
    assertResult(Some(webSocketActorRef.ref))(oneTimeMessageActorRef.underlyingActor.webSocketActorRef)
    webSocketActorRef.ref.tell(PoisonPill, testActor)
    assert(oneTimeMessageActorRef.underlyingActor.webSocketActorRef.isEmpty)
    // 여기서 unwatch 가 제대로 실행됐는지 확인할 방법이 없다.
  }
}

class Xxx(actorRef: ActorRef) extends Actor {

  override def receive: Receive = {
    case x =>
      actorRef.forward(x)
  }
}
