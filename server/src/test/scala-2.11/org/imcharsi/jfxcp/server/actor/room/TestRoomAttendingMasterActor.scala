/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.room

import akka.actor.{ Props, ActorSystem }
import akka.pattern.ask
import akka.testkit.{ TestProbe, TestActorRef, TestKit }
import org.imcharsi.jfxcp.common.roomattendingmaster.RoomAttendingMaster
import org.imcharsi.jfxcp.common.roommaster.RoomMaster
import org.imcharsi.jfxcp.server.actor.user.WebSocketActor.{ QueueRoomStatusChangedRTW, QueueRoomAnnounceRTW }
import org.imcharsi.jfxcp.server.actor.user.{ WebSocketActor, UserWorkerActor }
import org.scalatest.FunSuiteLike
import scala.collection.immutable.Queue
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{ Success, Try }

/**
 * Created by i on 8/3/16.
 */
class TestRoomAttendingMasterActor extends TestKit(ActorSystem()) with FunSuiteLike {
  test("success enter room") {
    val userWorkerActorRef = TestProbe("userWorkerActorRef")
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.second)))
    val roomAttendingMasterActorRef = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef, userWorkerActorRef.ref, 1.second, "hi")))
    roomMasterActorRef.tell(RoomMaster.CreateRoomCTS("hi", "there"), testActor)
    val result1 = expectMsgType[RoomMaster.CreatedRoomSTC]
    Try {
      Await.result(
        roomAttendingMasterActorRef.ask(RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get))(1.second),
        10.seconds
      )
    } match {
      case Success(RoomAttendingMaster.EnteredRoomSTC(Some(_))) =>
      case _ => fail()
    }
  }
  test("fail enter room") {
    val userWorkerActorRef = TestProbe("userWorkerActorRef")
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.second)))
    val roomAttendingMasterActorRef = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef, userWorkerActorRef.ref, 1.second, "hi")))
    roomMasterActorRef.tell(RoomMaster.CreateRoomCTS("hi", "there"), testActor)
    val result1 = expectMsgType[RoomMaster.CreatedRoomSTC]
    Try {
      Await.result(
        roomAttendingMasterActorRef.ask(RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get + 1))(1.second),
        10.seconds
      )
    } match {
      case Success(RoomAttendingMaster.EnteredRoomSTC(None)) =>
      case _ => fail()
    }
  }
  test("broadcast web socket 1") {
    val userWorkerActorRef = TestProbe("userWorkerActorRef")
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.second)))
    val webSocketActorRef = TestProbe("webSocketActor")
    val roomAttendingMasterActorRef = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef, userWorkerActorRef.ref, 1.second, "hi")))
    // room attending actor 를 만들기 전에 web socket 을 정해 놓는다.
    roomAttendingMasterActorRef.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorRef.ref), testActor)
    roomMasterActorRef.tell(RoomMaster.CreateRoomCTS("hi", "there"), testActor)
    val result1 = expectMsgType[RoomMaster.CreatedRoomSTC]
    Try {
      Await.result(
        roomAttendingMasterActorRef.ask(RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get))(1.second),
        10.seconds
      )
    } match {
      case Success(RoomAttendingMaster.EnteredRoomSTC(Some(_))) =>
      case _ => fail()
    }
    assertResult(0)(webSocketActorRef.expectMsgType[WebSocketActor.BatchQueueToWebSocketActor].queue.size)
    webSocketActorRef.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActorRef.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
  }
  test("broadcast web socket 2") {
    val userWorkerActorRef = TestProbe("userWorkerActorRef")
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.second)))
    val webSocketActorRef = TestProbe("webSocketActor")
    val roomAttendingMasterActorRef = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef, userWorkerActorRef.ref, 1.second, "hi")))
    roomMasterActorRef.tell(RoomMaster.CreateRoomCTS("hi", "there"), testActor)
    val result1 = expectMsgType[RoomMaster.CreatedRoomSTC]
    Try {
      Await.result(
        roomAttendingMasterActorRef.ask(RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get))(1.second),
        10.seconds
      )
    } match {
      case Success(RoomAttendingMaster.EnteredRoomSTC(Some(_))) =>
      case _ => fail()
    }
    // room attending actor 를 만들고 나서 web socket 을 정해 놓는다.
    webSocketActorRef.expectNoMsg(0.5.seconds)
    roomAttendingMasterActorRef.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorRef.ref), testActor)
    assertResult(2)(webSocketActorRef.expectMsgType[WebSocketActor.BatchQueueToWebSocketActor].queue.size)
  }

  test("late web socket") {
    // room actor 가 보내는 자료는 room attending actor 가 받아서 room attending master actor 로 전달한 다음
    // web socket actor 로 전달된다. 누락대비/전송보장역할은 room attending master actor 가 하기로 구상을 바꿨다.
    // 따라서, 모든 room attending actor 가 room actor 에게서 받는 자료는 모두 room attending master actor 로 모이게 된다.
    val webSocketActorRef = TestProbe("webSocketActor")
    val userWorkerActorRef = TestProbe("userWorkerActor")
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val roomAttendingMasterActorRef = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef.ref, userWorkerActorRef.ref, 1.second, "hi")))
    // web socket 으로 아무것도 오지 않아야 한다.
    roomAttendingMasterActorRef.tell(WebSocketActor.QueueRoomAnnounceRTW(1, 2, "3"), testActor)
    roomAttendingMasterActorRef.tell(WebSocketActor.QueueRoomStatusChangedRTW(1, 3), testActor)
    webSocketActorRef.expectNoMsg(1.second)
    // web socket 갱신을 전달받으면, 그 동안 쌓아놨던 것을 전부 받는다.
    roomAttendingMasterActorRef.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorRef.ref), testActor)
    val result2 = webSocketActorRef.expectMsgType[WebSocketActor.BatchQueueToWebSocketActor]
    assertResult(Queue(classOf[QueueRoomAnnounceRTW], classOf[QueueRoomStatusChangedRTW]))(result2.queue.map(_.getClass))
  }
}
