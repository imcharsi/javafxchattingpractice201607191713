/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.room

import akka.actor.{ PoisonPill, ActorRef, Props, ActorSystem }
import akka.testkit.{ TestProbe, TestActorRef, TestKit }
import org.imcharsi.jfxcp.common.roomattending.RoomAttending
import org.imcharsi.jfxcp.common.roomattendingmaster.RoomAttendingMaster
import org.imcharsi.jfxcp.common.roommaster.RoomMaster
import org.imcharsi.jfxcp.server.actor.user.{ UserWorkerActor, WebSocketActor }
import org.imcharsi.jfxcp.server.actor.user.WebSocketActor.{ QueueRoomStatusChangedRTW, QueueRoomAnnounceRTW }
import org.scalatest.{ BeforeAndAfterEach, FunSuiteLike }
import scala.collection.immutable.Queue
import scala.concurrent.duration._

/**
 * Created by i on 8/2/16.
 */
class TestRoomAttendingActor extends TestKit(ActorSystem()) with FunSuiteLike {
  test("enter/leave room with web socket/refresh timeout") {
    val webSocketActorRef = TestProbe("webSocketActor")
    val userWorkerActorRef = TestProbe("userWorkerActor")
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.second)))
    val roomAttendingMasterActorRef = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef, userWorkerActorRef.ref, 1.second, "hi")))
    // 대화방 만들기
    roomMasterActorRef.tell(RoomMaster.CreateRoomCTS("hi", "there"), testActor)
    val result1 = expectMsgType[RoomMaster.CreatedRoomSTC]

    // web socket actor 알리기
    roomAttendingMasterActorRef.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorRef.ref), testActor)
    webSocketActorRef.expectMsgType[WebSocketActor.BatchQueueToWebSocketActor]
    // 대화방 참가하기
    roomAttendingMasterActorRef.tell(RoomAttendingMaster.EnterRoomCTS(result1.roomNo.get), testActor)
    val enterResult = expectMsgType[RoomAttendingMaster.EnteredRoomSTC]
    val roomAttendingActorRef = roomAttendingMasterActorRef.getSingleChild(enterResult.raaId.get.toString)
    // 대화방에 참가하면 시간초과를 갱신해야 한다.
    userWorkerActorRef.expectMsg(UserWorkerActor.RefreshTimeout)
    // 입장할 때는 입장하는 본인도 입장에 관한 알림을 받는다.
    val message1 = webSocketActorRef.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    val message2 = webSocketActorRef.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    assertResult(true)(roomAttendingMasterActorRef.underlyingActor.queue.contains((message1.roomNo, message1.msgNo)))
    assertResult(true)(roomAttendingMasterActorRef.underlyingActor.queue.contains((message2.roomNo, message2.msgNo)))
    // client 가 message 를 받았음을 응답하면
    roomAttendingMasterActorRef.tell(WebSocketActor.TransferredRoomMessageWTR(message1.roomNo, message1.msgNo), webSocketActorRef.ref)
    // room attending actor 의 queue 에 해당 내용이 지워져야 한다.
    assertResult(false)(roomAttendingMasterActorRef.underlyingActor.queue.contains((message1.roomNo, message1.msgNo)))
    roomAttendingMasterActorRef.tell(WebSocketActor.TransferredRoomMessageWTR(message2.roomNo, message2.msgNo), webSocketActorRef.ref)
    assertResult(false)(roomAttendingMasterActorRef.underlyingActor.queue.contains((message2.roomNo, message2.msgNo)))
    watch(roomAttendingActorRef)
    // 대화방에서 퇴장하기
    roomAttendingActorRef.tell(RoomAttending.LeaveRoomCTS, testActor)
    expectMsg(RoomAttending.LeavedRoomSTC(true))
    expectTerminated(roomAttendingActorRef)
    // 대화방에서 퇴장하면 시간초과를 갱신해야 한다.
    userWorkerActorRef.expectMsg(UserWorkerActor.RefreshTimeout)
    // 퇴장하는 본인은 퇴장에 관한 어떠한 알림도 web socket 을 거쳐서 받지 않는다.
    webSocketActorRef.expectNoMsg(0.5.seconds)
  }
  test("timeout") {
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.second)))
    val roomAttendingActorRef =
      TestActorRef[RoomAttendingActor](
        Props(new RoomAttendingActor(roomMasterActorRef, ActorRef.noSender, ActorRef.noSender, 1.second, 1, 2, "hi")), "wrong"
      )
    watch(roomAttendingActorRef)
    expectTerminated(roomAttendingActorRef)
  }
  test("fail to enter room with not existence room") {
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.second)))
    roomMasterActorRef.tell(RoomMaster.CreateRoomCTS("hi", "there"), testActor)
    val result1 = expectMsgType[RoomMaster.CreatedRoomSTC]
    val roomAttendingActorRef =
      TestActorRef[RoomAttendingActor](
        Props(new RoomAttendingActor(roomMasterActorRef, ActorRef.noSender, ActorRef.noSender, 1.second, 1, 2, "hi")),
        (result1.roomNo.get + 1).toString
      )
    watch(roomAttendingActorRef)
    result1.roomNo.foreach(roomNo => roomAttendingActorRef.tell(RoomAttendingActor.EnterRoom, testActor))
    expectMsg(RoomAttendingActor.EnteredRoom(false))
    expectTerminated(roomAttendingActorRef)
  }
  test("fail to first enter room with wrong creator id") {
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.second)))
    roomMasterActorRef.tell(RoomMaster.CreateRoomCTS("hi", "there"), testActor)
    val result1 = expectMsgType[RoomMaster.CreatedRoomSTC]
    val roomAttendingActorRef =
      TestActorRef[RoomAttendingActor](
        Props(new RoomAttendingActor(roomMasterActorRef, ActorRef.noSender, ActorRef.noSender, 1.second, 1, 2, "hi1")),
        result1.roomNo.get.toString
      )
    watch(roomAttendingActorRef)
    result1.roomNo.foreach(roomNo => roomAttendingActorRef.tell(RoomAttendingActor.EnterRoom, testActor))
    expectMsg(RoomAttendingActor.EnteredRoom(false))
    expectTerminated(roomAttendingActorRef)
  }
}
