/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.room

import akka.actor.{ ActorRef, Props, ActorSystem }
import akka.pattern.ask
import akka.testkit.{ TestProbe, TestActorRef, TestKit }
import org.imcharsi.jfxcp.common.room.Room
import org.imcharsi.jfxcp.common.roomattending.RoomAttending
import org.imcharsi.jfxcp.common.roomattendingmaster.RoomAttendingMaster
import org.imcharsi.jfxcp.common.roommaster.RoomMaster
import org.imcharsi.jfxcp.server.actor.user.{ WebSocketActor, UserWorkerActor }
import org.scalatest.FunSuiteLike
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.util.{ Failure, Try }

/**
 * Created by i on 8/3/16.
 */
class TestIntegration extends TestKit(ActorSystem()) with FunSuiteLike {
  test("two room attending actors' conversation/grant,revoke operator") {
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.second)))
    val userWorkerActorRef1 = TestProbe("userWorkerActorRef1")
    val userWorkerActorRef2 = TestProbe("userWorkerActorRef2")
    val roomAttendingMasterActorRefHi1 = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef, userWorkerActorRef1.ref, 1.second, "hi1")))
    val roomAttendingMasterActorRefHi2 = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef, userWorkerActorRef2.ref, 1.second, "hi2")))
    val webSocketActor1 = TestProbe("webSocketActor1")
    roomAttendingMasterActorRefHi1.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActor1.ref), testActor)
    val webSocketActor2 = TestProbe("webSocketActor2")
    roomAttendingMasterActorRefHi2.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActor2.ref), testActor)
    roomMasterActorRef.tell(RoomMaster.CreateRoomCTS("hi1", "hi1's room"), testActor)
    val createdRoomResult = expectMsgType[RoomMaster.CreatedRoomSTC]
    roomAttendingMasterActorRefHi1.tell(RoomAttendingMaster.EnterRoomCTS(createdRoomResult.roomNo.get), testActor)
    val enterResult1 = expectMsgType[RoomAttendingMaster.EnteredRoomSTC]
    webSocketActor1.expectMsgType[WebSocketActor.BatchQueueToWebSocketActor]
    webSocketActor1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActor1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    userWorkerActorRef1.expectMsg(UserWorkerActor.RefreshTimeout)
    roomAttendingMasterActorRefHi2.tell(RoomAttendingMaster.EnterRoomCTS(createdRoomResult.roomNo.get), testActor)
    val enterResult2 = expectMsgType[RoomAttendingMaster.EnteredRoomSTC]
    webSocketActor2.expectMsgType[WebSocketActor.BatchQueueToWebSocketActor]
    webSocketActor2.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActor2.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    webSocketActor1.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActor1.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    userWorkerActorRef2.expectMsg(UserWorkerActor.RefreshTimeout)
    system.actorSelection(roomAttendingMasterActorRefHi1.path / enterResult1.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.ChatCTS("hi there"), testActor))(Implicits.global)
    expectMsg(RoomActor.Chatted)
    val chatResult11 = webSocketActor1.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    val chatResult12 = webSocketActor2.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    assertResult("hi1")(chatResult11.userId)
    assertResult("hi there")(chatResult11.s)
    assertResult("hi1")(chatResult12.userId)
    assertResult("hi there")(chatResult12.s)
    userWorkerActorRef1.expectMsg(UserWorkerActor.RefreshTimeout)
    system.actorSelection(roomAttendingMasterActorRefHi2.path / enterResult2.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.ChatCTS("hi there2"), testActor))(Implicits.global)
    expectMsg(RoomActor.Chatted)
    val chatResult21 = webSocketActor1.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    val chatResult22 = webSocketActor2.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    assertResult("hi2")(chatResult21.userId)
    assertResult("hi there2")(chatResult21.s)
    assertResult("hi2")(chatResult22.userId)
    assertResult("hi there2")(chatResult22.s)
    userWorkerActorRef2.expectMsg(UserWorkerActor.RefreshTimeout)
    system.actorSelection(roomAttendingMasterActorRefHi2.path / enterResult2.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.GrantOperatorCTS("hi1"), testActor))(Implicits.global)
    expectMsg(RoomAttending.GrantedOperatorSTC(false))
    system.actorSelection(roomMasterActorRef.path / createdRoomResult.roomNo.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(Room.GetRoomStatusCTS, testActor))(Implicits.global)
    assertResult(Room.GotRoomStatusSTC(0, "hi1", "hi1's room", Map("hi1" -> false, "hi2" -> false)))(expectMsgType[Room.GotRoomStatusSTC].copy(version = 0))
    system.actorSelection(roomAttendingMasterActorRefHi1.path / enterResult1.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.GrantOperatorCTS("hi2"), testActor))(Implicits.global)
    expectMsg(RoomAttending.GrantedOperatorSTC(true))
    system.actorSelection(roomMasterActorRef.path / createdRoomResult.roomNo.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(Room.GetRoomStatusCTS, testActor))(Implicits.global)
    assertResult(Room.GotRoomStatusSTC(0, "hi1", "hi1's room", Map("hi1" -> false, "hi2" -> true)))(expectMsgType[Room.GotRoomStatusSTC].copy(version = 0))
    system.actorSelection(roomAttendingMasterActorRefHi2.path / enterResult2.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.RevokeOperatorCTS("hi2"), testActor))(Implicits.global)
    expectMsg(RoomAttending.RevokedOperatorSTC(false))
    system.actorSelection(roomMasterActorRef.path / createdRoomResult.roomNo.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(Room.GetRoomStatusCTS, testActor))(Implicits.global)
    assertResult(Room.GotRoomStatusSTC(0, "hi1", "hi1's room", Map("hi1" -> false, "hi2" -> true)))(expectMsgType[Room.GotRoomStatusSTC].copy(version = 0))
    system.actorSelection(roomAttendingMasterActorRefHi1.path / enterResult1.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.RevokeOperatorCTS("hi2"), testActor))(Implicits.global)
    expectMsg(RoomAttending.RevokedOperatorSTC(true))
    system.actorSelection(roomMasterActorRef.path / createdRoomResult.roomNo.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(Room.GetRoomStatusCTS, testActor))(Implicits.global)
    assertResult(Room.GotRoomStatusSTC(0, "hi1", "hi1's room", Map("hi1" -> false, "hi2" -> false)))(expectMsgType[Room.GotRoomStatusSTC].copy(version = 0))
  }
  test("A enter, B enter, C enter, A leave") {
    val roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(1.second)))
    val userWorkerActorRef = TestProbe("userWorkerActorRef")
    val roomAttendingMasterActorRefHiA = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef, userWorkerActorRef.ref, 1.second, "A")))
    val roomAttendingMasterActorRefHiB = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef, userWorkerActorRef.ref, 1.second, "B")))
    val roomAttendingMasterActorRefHiC = TestActorRef[RoomAttendingMasterActor](Props(new RoomAttendingMasterActor(roomMasterActorRef, userWorkerActorRef.ref, 1.second, "C")))
    val webSocketActorA = TestProbe("webSocketActorA")
    roomAttendingMasterActorRefHiA.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorA.ref), testActor)
    val webSocketActorB = TestProbe("webSocketActorB")
    roomAttendingMasterActorRefHiB.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorB.ref), testActor)
    val webSocketActorC = TestProbe("webSocketActorC")
    roomAttendingMasterActorRefHiC.tell(UserWorkerActor.BroadcastWebSocketActor(webSocketActorC.ref), testActor)
    roomMasterActorRef.tell(RoomMaster.CreateRoomCTS("A", "A's room"), testActor)
    val createdRoomResult = expectMsgType[RoomMaster.CreatedRoomSTC]
    roomAttendingMasterActorRefHiA.tell(RoomAttendingMaster.EnterRoomCTS(createdRoomResult.roomNo.get), testActor)
    val enterResult1 = expectMsgType[RoomAttendingMaster.EnteredRoomSTC]
    webSocketActorA.expectMsgType[WebSocketActor.BatchQueueToWebSocketActor]
    webSocketActorA.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActorA.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomAttendingMasterActorRefHiB.tell(RoomAttendingMaster.EnterRoomCTS(createdRoomResult.roomNo.get), testActor)
    val enterResult2 = expectMsgType[RoomAttendingMaster.EnteredRoomSTC]
    webSocketActorB.expectMsgType[WebSocketActor.BatchQueueToWebSocketActor]
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    webSocketActorA.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActorA.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    roomAttendingMasterActorRefHiC.tell(RoomAttendingMaster.EnterRoomCTS(createdRoomResult.roomNo.get), testActor)
    val enterResult3 = expectMsgType[RoomAttendingMaster.EnteredRoomSTC]
    webSocketActorC.expectMsgType[WebSocketActor.BatchQueueToWebSocketActor]
    webSocketActorC.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActorC.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    webSocketActorA.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActorA.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    system.actorSelection(roomAttendingMasterActorRefHiA.path / enterResult1.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.ChatCTS("hi there"), testActor))(Implicits.global)
    expectMsg(RoomActor.Chatted)
    webSocketActorA.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    webSocketActorC.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    system.actorSelection(roomAttendingMasterActorRefHiB.path / enterResult2.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.ChatCTS("hi there2"), testActor))(Implicits.global)
    expectMsg(RoomActor.Chatted)
    webSocketActorA.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    webSocketActorC.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    system.actorSelection(roomAttendingMasterActorRefHiC.path / enterResult3.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.ChatCTS("hi there3"), testActor))(Implicits.global)
    expectMsg(RoomActor.Chatted)
    webSocketActorA.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    webSocketActorC.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    system.actorSelection(roomAttendingMasterActorRefHiA.path / enterResult1.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.LeaveRoomCTS, testActor))(Implicits.global)
    expectMsg(RoomAttending.LeavedRoomSTC(true))
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW] // 방장의 퇴장을 알린다
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    webSocketActorC.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW]
    webSocketActorC.expectMsgType[WebSocketActor.QueueRoomAnnounceRTW] // 방장의 퇴장을 알린다
    webSocketActorC.expectMsgType[WebSocketActor.QueueRoomStatusChangedRTW]
    system.actorSelection(roomAttendingMasterActorRefHiC.path / enterResult3.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.ChatCTS("hi there4"), testActor))(Implicits.global)
    expectMsg(RoomActor.Chatted)
    webSocketActorA.expectNoMsg(0.5.seconds)
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    webSocketActorC.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    system.actorSelection(roomAttendingMasterActorRefHiB.path / enterResult2.raaId.get.toString)
      .resolveOne(1.second)
      .foreach(_.tell(RoomAttending.ChatCTS("hi there5"), testActor))(Implicits.global)
    expectMsg(RoomActor.Chatted)
    webSocketActorA.expectNoMsg(0.5.seconds)
    webSocketActorB.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    webSocketActorC.expectMsgType[WebSocketActor.QueueRoomChatRTW]
    Try {
      Await.result(
        system.actorSelection(roomAttendingMasterActorRefHiA.path / enterResult1.raaId.get.toString)
          .resolveOne(1.second), 3.seconds
      )
    } match {
      case Failure(_) =>
      case _ => fail()
    }
  }

}
