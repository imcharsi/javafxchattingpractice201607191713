/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.route

import akka.actor.{ ActorRef, PoisonPill, Props }
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{ BasicHttpCredentials, Cookie, `Set-Cookie` }
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.actor.ActorPublisher
import akka.stream.actor.ActorPublisherMessage.{ Cancel, Request }
import akka.stream.scaladsl.{ Flow, Sink, Source }
import akka.testkit.{ TestActorRef, TestProbe }
import akka.util.ByteString
import com.softwaremill.session.{ SessionConfig, SessionManager, SessionUtil }
import org.imcharsi.jfxcp.common.onetimemessage.OneTimeMessage
import org.imcharsi.jfxcp.common.room.Room
import org.imcharsi.jfxcp.common.roomattending.RoomAttending
import org.imcharsi.jfxcp.common.roomattendingmaster.RoomAttendingMaster
import org.imcharsi.jfxcp.common.roommaster.RoomMaster
import org.imcharsi.jfxcp.common.websocket.WebSocket
import org.imcharsi.jfxcp.common.websocket.WebSocket._
import org.imcharsi.jfxcp.server.actor.auth.{ IdLookupActor, AuthenticateActor }
import org.imcharsi.jfxcp.server.actor.room.RoomMasterActor
import org.imcharsi.jfxcp.server.actor.user.UserMasterActor
import org.scalatest.{ BeforeAndAfterEach, FunSuite }
import spray.json._

import scala.collection.immutable.Queue
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }

/**
 * Created by i on 7/26/16.
 */
class TestRouteTemplateWithRoute extends FunSuite with ScalatestRouteTest with BeforeAndAfterEach {
  def dummyRetrieveF(correctId: List[String])(id: String, password: String): Future[Option[Unit]] = {
    Future {
      if (correctId.contains(id))
        Some(())
      else
        None
    }(Implicits.global)
  }

  def fold(request: HttpRequest)(list: List[RequestTransformer]): HttpRequest =
    list.foldLeft(request) { (z, x) => (z ~> x).asInstanceOf[HttpRequest] }

  var idLookupActorRef: ActorRef = ActorRef.noSender
  var authenticateActorRef: ActorRef = ActorRef.noSender
  var userMasterActorRef: ActorRef = ActorRef.noSender
  var roomMasterActorRef: ActorRef = ActorRef.noSender
  implicit val sessionManager = new SessionManager[Long](SessionConfig.default(SessionUtil.randomServerSecret()))

  test("login/logout") {
    val route = Route.seal(
      RouteTemplate.routeLogin(userMasterActorRef) ~
        RouteTemplate.routeLogout(userMasterActorRef)
    )
    val cookiePair = fold(Post("/login"))(List(addCredentials(BasicHttpCredentials("hi1", "")))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
      header[`Set-Cookie`].map(_.cookie.pair()).get
    }
    fold(Put("/logout"))(List(addHeader(Cookie(cookiePair)))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
    }
  }

  test("login retry") {
    val route = Route.seal(
      RouteTemplate.routeLogin(userMasterActorRef) ~
        RouteTemplate.routeLogout(userMasterActorRef)
    )
    val cookiePair = fold(Post("/login"))(List(addCredentials(BasicHttpCredentials("hi3", "")))) ~> route ~> check {
      assertResult(StatusCodes.Unauthorized)(status)
      header[`Set-Cookie`].map(_.cookie.pair()).get
    }
    // 인증을 틀릴 당시 썼던 cookie 로 이후 맞는 인증을 써서 성공하기.
    fold(Post("/login"))(List(addCredentials(BasicHttpCredentials("hi1", "")), addHeader(Cookie(cookiePair)))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
    }
    fold(Put("/logout"))(List(addHeader(Cookie(cookiePair)))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
    }
  }

  // 시험용 publisher 이다. 이와 같이 하지 않고 검사를 하는 방법을 모르겠다.
  // TestSource, Source.actorRef 모두 run 이 필요한데, probe 를 구할 방법을 모르겠다.
  class TestPublisher extends ActorPublisher[String] {
    var queue: Queue[String] = Queue()

    def processQueue(): Unit = {
      while (totalDemand > 0 && !queue.isEmpty) {
        val (a, b) = queue.dequeue
        queue = b
        onNext(a)
      }
    }

    override def receive: Receive = {
      case s: String =>
        queue = queue.enqueue(s)
        processQueue()
      case Request(_) => processQueue()
      case Cancel => onComplete()
    }
  }

  test("web socket") {
    val route = Route.seal(
      RouteTemplate.routeLogin(userMasterActorRef) ~
        RouteTemplate.routeWebSocket(userMasterActorRef) ~
        RouteTemplate.routeLogout(userMasterActorRef)
    )
    val watch = TestProbe("watch")
    val subscriberActorRef = TestProbe("subscriber")
    val testPublisher = TestActorRef[TestPublisher](Props(new TestPublisher))
    val webSocketClient =
      Flow.fromSinkAndSource(
        Flow[Message]
          .flatMapConcat {
            case TextMessage.Strict(s) => Source.single(s)
            case TextMessage.Streamed(s) => s
            case _ => Source.empty
          }.to(Sink.actorRef[String](subscriberActorRef.ref, PoisonPill)),
        Source.fromPublisher(ActorPublisher[String](testPublisher))
          .via(Flow[String].map { s => TextMessage.Strict(s) })
      )
    val cookiePair = fold(Post("/login"))(List(addCredentials(BasicHttpCredentials("hi1", "")))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
      header[`Set-Cookie`].map(_.cookie.pair()).get
    }
    fold(WS("/webSocket", webSocketClient))(List(addHeader(Cookie(cookiePair)))) ~> route ~> check {
      assertResult(StatusCodes.SwitchingProtocols)(status)
      testPublisher.tell(GreetingFirstCTS.asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint, ActorRef.noSender)
      assertResult(GreetingSecondSTC)(subscriberActorRef.expectMsgType[String].parseJson.convertTo[WebSocketMessageSTC])
    }
    watch.watch(subscriberActorRef.ref)
    fold(Put("/logout"))(List(addHeader(Cookie(cookiePair)))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
    }
    // logout 을 하면 web socket 도 자동으로 끊겨야 한다.
    watch.expectTerminated(subscriberActorRef.ref)
  }

  test("onetime message") {
    val route = Route.seal(
      RouteTemplate.routeLogin(userMasterActorRef) ~
        RouteTemplate.routeWebSocket(userMasterActorRef) ~
        RouteTemplate.routeOneTimeMessage(userMasterActorRef) ~
        RouteTemplate.routeLogout(userMasterActorRef)
    )
    val watch = TestProbe("watch")
    // 사용자1 준비
    val subscriberActorRef1 = TestProbe("subscriber")
    val testPublisher1 = TestActorRef[TestPublisher](Props(new TestPublisher))
    val webSocketClient1 =
      Flow.fromSinkAndSource(
        Flow[Message]
          .flatMapConcat {
            case TextMessage.Strict(s) => Source.single(s)
            case TextMessage.Streamed(s) => s
            case _ => Source.empty
          }.to(Sink.actorRef[String](subscriberActorRef1.ref, PoisonPill)),
        Source.fromPublisher(ActorPublisher[String](testPublisher1))
          .via(Flow[String].map { s => TextMessage.Strict(s) })
      )
    val cookiePair1 = fold(Post("/login"))(List(addCredentials(BasicHttpCredentials("hi1", "")))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
      header[`Set-Cookie`].map(_.cookie.pair()).get
    }
    // 사용자2 준비
    val subscriberActorRef2 = TestProbe("subscriber")
    val testPublisher2 = TestActorRef[TestPublisher](Props(new TestPublisher))
    val webSocketClient2 =
      Flow.fromSinkAndSource(
        Flow[Message]
          .flatMapConcat {
            case TextMessage.Strict(s) => Source.single(s)
            case TextMessage.Streamed(s) => s
            case _ => Source.empty
          }.to(Sink.actorRef[String](subscriberActorRef2.ref, PoisonPill)),
        Source.fromPublisher(ActorPublisher[String](testPublisher2))
          .via(Flow[String].map { s => TextMessage.Strict(s) })
      )
    val cookiePair2 = fold(Post("/login"))(List(addCredentials(BasicHttpCredentials("hi2", "")))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
      header[`Set-Cookie`].map(_.cookie.pair()).get
    }
    // 사용자1 web socket 연결.
    fold(WS("/webSocket", webSocketClient1))(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
      assertResult(StatusCodes.SwitchingProtocols)(status)
      testPublisher1.tell(GreetingFirstCTS.asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint, ActorRef.noSender)
      assertResult(GreetingSecondSTC)(subscriberActorRef1.expectMsgType[String].parseJson.convertTo[WebSocketMessageSTC])
    }
    // 사용자2 web socket 연결.
    fold(WS("/webSocket", webSocketClient2))(List(addHeader(Cookie(cookiePair2)))) ~> route ~> check {
      assertResult(StatusCodes.SwitchingProtocols)(status)
      testPublisher2.tell(GreetingFirstCTS.asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint, ActorRef.noSender)
      assertResult(GreetingSecondSTC)(subscriberActorRef2.expectMsgType[String].parseJson.convertTo[WebSocketMessageSTC])
    }
    // 여기서 일회용 대화를 전달한다.
    val scheduledOneTimeMessageOTC =
      fold(
        Post(
          "/oneTimeMessage",
          HttpEntity(
            ContentTypes.`application/json`,
            Source.single(OneTimeMessage.OneTimeMessageCTO("hi2", "hi2 greeting")
              .asInstanceOf[OneTimeMessage.OneTimeMessageRequestCTO].toJson.prettyPrint)
              .map(ByteString(_))
          )
        )
      )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
          assertResult(StatusCodes.OK)(status)
          Await.result(responseEntity.dataBytes.map(_.utf8String).runWith(Sink.head), 10.seconds)
            .parseJson
            .convertTo[OneTimeMessage.OneTimeMessageResponseOTC]
            .asInstanceOf[OneTimeMessage.ScheduledOneTimeMessageOTC]
        }
    // 사용자2 가 web socket 을 통해서 일회용 대화를 전달받았음을 검사한다.
    val oneTimeMessageSTC = subscriberActorRef2.expectMsgType[String].parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.OneTimeMessageSTC]
    assertResult("hi1")(oneTimeMessageSTC.id)
    assertResult("hi2 greeting")(oneTimeMessageSTC.chat)
    assertResult(scheduledOneTimeMessageOTC.userNo)(oneTimeMessageSTC.userNo)
    assertResult(scheduledOneTimeMessageOTC.msgNo)(oneTimeMessageSTC.msgNo)
    // 전달받았음을 응답한다.
    testPublisher2.tell(
      WebSocket.TransferredOneTimeMessageCTS(oneTimeMessageSTC.userNo, oneTimeMessageSTC.msgNo)
      .asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint,
      ActorRef.noSender
    )
    // 사용자1 은 상대방이 대화를 전달받았음을 확인받는다.
    val oneTimeMessageAckSTC = subscriberActorRef1.expectMsgType[String].parseJson.convertTo[WebSocketMessageSTC].asInstanceOf[WebSocket.OneTimeMessageAckSTC]
    // 햇갈린다. 우선, 사용자2 는 대화를 받으면 받았다는 응답을 보낸다.
    // 이 응답을 사용자1 에게 보내면 사용자1 은 사용자2 가 자신이 보낸 대화를 받았음을 알게 된다.
    // 사용자2 의 응답을 사용자1 이 받았음을 응답해야 사용자1 의 onetime message actor 가 사용자2 의 응답을 재전송을 하지 않게 된다.
    testPublisher1.tell(
      WebSocket.TransferredOneTimeMessageAckCTS(oneTimeMessageAckSTC.userNo, oneTimeMessageAckSTC.msgNo)
      .asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint,
      ActorRef.noSender
    )
    // logout 을 하면 web socket 도 자동으로 끊겨야 한다.
    watch.watch(subscriberActorRef1.ref)
    fold(Put("/logout"))(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
    }
    watch.expectTerminated(subscriberActorRef1.ref)
    watch.watch(subscriberActorRef2.ref)
    fold(Put("/logout"))(List(addHeader(Cookie(cookiePair2)))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
    }
    watch.expectTerminated(subscriberActorRef2.ref)
  }

  test("room conversation/pingpong") {
    val route = Route.seal(
      RouteTemplate.routeLogin(userMasterActorRef) ~
        RouteTemplate.routeWebSocket(userMasterActorRef) ~
        RouteTemplate.routeLogout(userMasterActorRef) ~
        RouteTemplate.routeCreateRoom(userMasterActorRef) ~
        RouteTemplate.routeRoomAttendingPingPong(userMasterActorRef) ~
        RouteTemplate.routeLeaveRoom(userMasterActorRef) ~
        RouteTemplate.routeVariousRequest(userMasterActorRef) ~
        RouteTemplate.routeEnterRoom(userMasterActorRef)
    )
    val watch = TestProbe("watch")
    // 사용자1 준비
    val subscriberActorRef1 = TestProbe("subscriber")
    val testPublisher1 = TestActorRef[TestPublisher](Props(new TestPublisher))
    val webSocketClient1 =
      Flow.fromSinkAndSource(
        Flow[Message]
          .flatMapConcat {
            case TextMessage.Strict(s) => Source.single(s)
            case TextMessage.Streamed(s) => s
            case _ => Source.empty
          }.to(Sink.actorRef[String](subscriberActorRef1.ref, PoisonPill)),
        Source.fromPublisher(ActorPublisher[String](testPublisher1))
          .via(Flow[String].map { s => TextMessage.Strict(s) })
      )
    val cookiePair1 = fold(Post("/login"))(List(addCredentials(BasicHttpCredentials("hi1", "")))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
      header[`Set-Cookie`].map(_.cookie.pair()).get
    }
    // 사용자2 준비
    val subscriberActorRef2 = TestProbe("subscriber")
    val testPublisher2 = TestActorRef[TestPublisher](Props(new TestPublisher))
    val webSocketClient2 =
      Flow.fromSinkAndSource(
        Flow[Message]
          .flatMapConcat {
            case TextMessage.Strict(s) => Source.single(s)
            case TextMessage.Streamed(s) => s
            case _ => Source.empty
          }.to(Sink.actorRef[String](subscriberActorRef2.ref, PoisonPill)),
        Source.fromPublisher(ActorPublisher[String](testPublisher2))
          .via(Flow[String].map { s => TextMessage.Strict(s) })
      )
    val cookiePair2 = fold(Post("/login"))(List(addCredentials(BasicHttpCredentials("hi2", "")))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
      header[`Set-Cookie`].map(_.cookie.pair()).get
    }
    // 사용자1 web socket 연결.
    fold(WS("/webSocket", webSocketClient1))(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
      assertResult(StatusCodes.SwitchingProtocols)(status)
      testPublisher1.tell(GreetingFirstCTS.asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint, ActorRef.noSender)
      assertResult(GreetingSecondSTC)(subscriberActorRef1.expectMsgType[String].parseJson.convertTo[WebSocketMessageSTC])
    }
    // 사용자2 web socket 연결.
    fold(WS("/webSocket", webSocketClient2))(List(addHeader(Cookie(cookiePair2)))) ~> route ~> check {
      assertResult(StatusCodes.SwitchingProtocols)(status)
      testPublisher2.tell(GreetingFirstCTS.asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint, ActorRef.noSender)
      assertResult(GreetingSecondSTC)(subscriberActorRef2.expectMsgType[String].parseJson.convertTo[WebSocketMessageSTC])
    }

    // 대화방 개설
    val createdRoomResult =
      fold(
        Post(
          "/room",
          HttpEntity(
            ContentTypes.`application/json`,
            Source.single(
              RoomMaster.CreateRoomCTS("", "room")
              .asInstanceOf[RoomMaster.RoomMasterRequestCTS]
              .toJson.prettyPrint
            ).map(ByteString(_))
          )
        )
      )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
          assertResult(StatusCodes.OK)(status)
          entityAs[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC]
        }

    val enterResult1 =
      // 사용자1 대화방 입장
      fold(
        Post(
          "/roomAttending",
          HttpEntity(
            ContentTypes.`application/json`,
            Source.single(
              RoomAttendingMaster.EnterRoomCTS(createdRoomResult.roomNo.get)
              .asInstanceOf[RoomAttendingMaster.RoomAttendingMasterRequestCTS]
              .toJson.prettyPrint
            ).map(ByteString(_))
          )
        )
      )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
          assertResult(StatusCodes.OK)(status)
          entityAs[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC]
        }
    // 입장 후 받게 된 message 에 대해 응답한다.
    val received11 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomAnnounceSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received11.raaId, received11.msgNo), ActorRef.noSender)
    val received12 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomStatusChangedSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received12.raaId, received12.msgNo), ActorRef.noSender)

    val enterResult2 =
      // 사용자2 대화방 입장
      fold(
        Post(
          "/roomAttending",
          HttpEntity(
            ContentTypes.`application/json`,
            Source.single(
              RoomAttendingMaster.EnterRoomCTS(createdRoomResult.roomNo.get)
              .asInstanceOf[RoomAttendingMaster.RoomAttendingMasterRequestCTS]
              .toJson.prettyPrint
            ).map(ByteString(_))
          )
        )
      )(List(addHeader(Cookie(cookiePair2)))) ~> route ~> check {
          assertResult(StatusCodes.OK)(status)
          entityAs[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC]
        }
    val received21 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomAnnounceSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received21.raaId, received21.msgNo), ActorRef.noSender)
    val received22 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomStatusChangedSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received22.raaId, received22.msgNo), ActorRef.noSender)
    // 사용자1은 사용자2가 입장했음을 알림받는다.
    val received13 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomAnnounceSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received13.raaId, received13.msgNo), ActorRef.noSender)
    val received14 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomStatusChangedSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received14.raaId, received14.msgNo), ActorRef.noSender)

    // 사용자1의 대화
    fold(
      Post(
        s"/roomAttending/${enterResult1.raaId.get}",
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.ChatCTS("hi")
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      )
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
      }
    val received23 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomChatSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received23.raaId, received23.msgNo), ActorRef.noSender)
    val received15 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomChatSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received15.raaId, received15.msgNo), ActorRef.noSender)

    // 사용자2의 대화
    fold(
      Post(
        s"/roomAttending/${enterResult2.raaId.get}",
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.ChatCTS("hi")
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      )
    )(List(addHeader(Cookie(cookiePair2)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
      }
    val received24 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomChatSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received24.raaId, received24.msgNo), ActorRef.noSender)
    val received16 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomChatSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received16.raaId, received16.msgNo), ActorRef.noSender)

    // 사용자2의 사용자1에 대한 운영자 권한 수여
    fold(
      Post(
        s"/roomAttending/${enterResult2.raaId.get}",
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.GrantOperatorCTS("hi1")
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      )
    )(List(addHeader(Cookie(cookiePair2)))) ~> route ~> check {
        assertResult(StatusCodes.BadRequest)(status)
      }

    // 사용자1의 사용자2에 대한 운영자 권한 수여
    fold(
      Post(
        s"/roomAttending/${enterResult1.raaId.get}",
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.GrantOperatorCTS("hi2")
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      )
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
      }

    val received16_1 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomAnnounceSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received16_1.raaId, received16_1.msgNo), ActorRef.noSender)
    val received16_2 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomStatusChangedSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received16_2.raaId, received16_2.msgNo), ActorRef.noSender)

    val received24_1 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomAnnounceSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received24_1.raaId, received24_1.msgNo), ActorRef.noSender)
    val received24_2 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomStatusChangedSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received24_2.raaId, received24_2.msgNo), ActorRef.noSender)

    // 사용자2의 사용자1에 대한 운영자 권한 회수
    fold(
      Post(
        s"/roomAttending/${enterResult2.raaId.get}",
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.RevokeOperatorCTS("hi1")
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      )
    )(List(addHeader(Cookie(cookiePair2)))) ~> route ~> check {
        assertResult(StatusCodes.BadRequest)(status)
      }

    // 사용자1의 사용자2에 대한 운영자 권한 회수
    fold(
      Post(
        s"/roomAttending/${enterResult1.raaId.get}",
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.GrantOperatorCTS("hi2")
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      )
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
      }
    val received16_3 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomAnnounceSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received16_3.raaId, received16_3.msgNo), ActorRef.noSender)
    val received16_4 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomStatusChangedSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received16_4.raaId, received16_4.msgNo), ActorRef.noSender)

    val received24_3 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomAnnounceSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received24_3.raaId, received24_3.msgNo), ActorRef.noSender)
    val received24_4 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomStatusChangedSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received24_4.raaId, received24_4.msgNo), ActorRef.noSender)

    // 사용자1은 현재 참가 중이므로 ping 에 성공한다.
    fold(
      Get(s"/roomAttending/${enterResult1.raaId.get}/ping")
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
      }

    // 사용자1의 퇴장
    fold(
      Delete(
        s"/roomAttending/${enterResult1.raaId.get}",
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.LeaveRoomCTS
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      )
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
      }
    val received25 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomAnnounceSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received25.raaId, received25.msgNo), ActorRef.noSender)
    val received26 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomAnnounceSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received26.raaId, received26.msgNo), ActorRef.noSender)
    val received27 =
      subscriberActorRef2.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomStatusChangedSTC]
    testPublisher2.tell(WebSocket.TransferredRoomMessageCTS(received27.raaId, received27.msgNo), ActorRef.noSender)

    // 사용자1은 현재 참가 중이지 않으므로 ping 에 실패한다.
    fold(
      Get(s"/roomAttending/${enterResult1.raaId.get}/ping")
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.BadRequest)(status)
      }

    // 사용자2의 퇴장
    fold(
      Delete(
        s"/roomAttending/${enterResult2.raaId.get}",
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.LeaveRoomCTS
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      )
    )(List(addHeader(Cookie(cookiePair2)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
      }

    watch.watch(subscriberActorRef1.ref)
    fold(Put("/logout"))(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
    }
    // logout 을 하면 web socket 도 자동으로 끊겨야 한다.
    watch.expectTerminated(subscriberActorRef1.ref)
    watch.watch(subscriberActorRef2.ref)
    fold(Put("/logout"))(List(addHeader(Cookie(cookiePair2)))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
    }
    watch.expectTerminated(subscriberActorRef2.ref)
  }

  test("late web socket") {
    val route = Route.seal(
      RouteTemplate.routeLogin(userMasterActorRef) ~
        RouteTemplate.routeWebSocket(userMasterActorRef) ~
        RouteTemplate.routeLogout(userMasterActorRef) ~
        RouteTemplate.routeCreateRoom(userMasterActorRef) ~
        RouteTemplate.routeLeaveRoom(userMasterActorRef) ~
        RouteTemplate.routeVariousRequest(userMasterActorRef) ~
        RouteTemplate.routeEnterRoom(userMasterActorRef)
    )
    val watch = TestProbe("watch")
    // 사용자1 준비
    val subscriberActorRef1 = TestProbe("subscriber")
    val testPublisher1 = TestActorRef[TestPublisher](Props(new TestPublisher))
    val webSocketClient1 =
      Flow.fromSinkAndSource(
        Flow[Message]
          .flatMapConcat {
            case TextMessage.Strict(s) => Source.single(s)
            case TextMessage.Streamed(s) => s
            case _ => Source.empty
          }.to(Sink.actorRef[String](subscriberActorRef1.ref, PoisonPill)),
        Source.fromPublisher(ActorPublisher[String](testPublisher1))
          .via(Flow[String].map { s => TextMessage.Strict(s) })
      )
    val cookiePair1 = fold(Post("/login"))(List(addCredentials(BasicHttpCredentials("hi1", "")))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
      header[`Set-Cookie`].map(_.cookie.pair()).get
    }

    // 대화방 개설
    val createdRoomResult =
      fold(
        Post(
          "/room",
          HttpEntity(
            ContentTypes.`application/json`,
            Source.single(
              RoomMaster.CreateRoomCTS("", "room")
              .asInstanceOf[RoomMaster.RoomMasterRequestCTS]
              .toJson.prettyPrint
            ).map(ByteString(_))
          )
        )
      )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
          assertResult(StatusCodes.OK)(status)
          entityAs[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC]
        }

    // 사용자1 대화방 입장
    fold(
      Post(
        "/roomAttending",
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttendingMaster.EnterRoomCTS(createdRoomResult.roomNo.get)
            .asInstanceOf[RoomAttendingMaster.RoomAttendingMasterRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      )
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
        entityAs[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC]
      }

    // 대화방에 입장했으므로 web socket 으로 받아야 할 자료가 있지만, 아직 web socket 접속을 하지 않아 아무것도 오지 않는다.
    subscriberActorRef1.expectNoMsg(1.second)

    // 사용자1 web socket 연결.
    fold(WS("/webSocket", webSocketClient1))(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
      assertResult(StatusCodes.SwitchingProtocols)(status)
    }

    // 접속했으므로, 그 동안 쌓아놨던 자료를 받는다.
    val received11 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomAnnounceSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received11.raaId, received11.msgNo), ActorRef.noSender)
    val received12 =
      subscriberActorRef1.expectMsgType[String].parseJson
        .convertTo[WebSocketMessageSTC]
        .asInstanceOf[WebSocket.RoomStatusChangedSTC]
    testPublisher1.tell(WebSocket.TransferredRoomMessageCTS(received12.raaId, received12.msgNo), ActorRef.noSender)

    // greeting first cts 에 대한 응답인 greeting second stc 가, 쌓여있던 대화방 자료보다 먼저 온다는 보장이 없다.
    // 검사의 편의를 위해 이와 같이 순서를 맞춘다.
    testPublisher1.tell(GreetingFirstCTS.asInstanceOf[WebSocketMessageCTS].toJson.prettyPrint, ActorRef.noSender)
    assertResult(GreetingSecondSTC)(subscriberActorRef1.expectMsgType[String].parseJson.convertTo[WebSocketMessageSTC])

    watch.watch(subscriberActorRef1.ref)
    fold(Put("/logout"))(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
    }
    watch.expectTerminated(subscriberActorRef1.ref)
  }

  test("list room/get room status") {
    val route = Route.seal(
      RouteTemplate.routeLogin(userMasterActorRef) ~
        RouteTemplate.routeWebSocket(userMasterActorRef) ~
        RouteTemplate.routeLogout(userMasterActorRef) ~
        RouteTemplate.routeCreateRoom(userMasterActorRef) ~
        RouteTemplate.routeLeaveRoom(userMasterActorRef) ~
        RouteTemplate.routeVariousRequest(userMasterActorRef) ~
        RouteTemplate.routeEnterRoom(userMasterActorRef) ~
        RouteTemplate.routeGetRoomStatus(roomMasterActorRef) ~
        RouteTemplate.routeListRoom(roomMasterActorRef)
    )
    val cookiePair1 = fold(Post("/login"))(List(addCredentials(BasicHttpCredentials("hi1", "")))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
      header[`Set-Cookie`].map(_.cookie.pair()).get
    }
    fold(
      Get("/room")
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
        assertResult(RoomMaster.ListedRoomSTC(List())) {
          Await.result(
            responseEntity.dataBytes
              .map(_.utf8String.parseJson.convertTo[(Int, String, String)])
              .fold(List[(Int, String, String)]())((l, x) => x :: l)
              .map(RoomMaster.ListedRoomSTC(_))
              .runWith(Sink.head),
            10.seconds
          )
        }
      }
    val createdRoomResult =
      fold(
        Post(
          "/room",
          HttpEntity(
            ContentTypes.`application/json`,
            Source.single(
              RoomMaster.CreateRoomCTS("", "room")
              .asInstanceOf[RoomMaster.RoomMasterRequestCTS]
              .toJson.prettyPrint
            ).map(ByteString(_))
          )
        )
      )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
          assertResult(StatusCodes.OK)(status)
          entityAs[RoomMaster.RoomMasterResponseSTC].asInstanceOf[RoomMaster.CreatedRoomSTC]
        }
    fold(
      Get("/room")
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
        assertResult(RoomMaster.ListedRoomSTC(List())) {
          Await.result(
            responseEntity.dataBytes
              .map(_.utf8String.parseJson.convertTo[(Int, String, String)])
              .fold(List[(Int, String, String)]())((l, x) => x :: l)
              .map(RoomMaster.ListedRoomSTC(_))
              .runWith(Sink.head),
            10.seconds
          )
        }
      }
    val enterResult1 =
      fold(
        Post(
          "/roomAttending",
          HttpEntity(
            ContentTypes.`application/json`,
            Source.single(
              RoomAttendingMaster.EnterRoomCTS(createdRoomResult.roomNo.get)
              .asInstanceOf[RoomAttendingMaster.RoomAttendingMasterRequestCTS]
              .toJson.prettyPrint
            ).map(ByteString(_))
          )
        )
      )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
          assertResult(StatusCodes.OK)(status)
          entityAs[RoomAttendingMaster.RoomAttendingMasterResponseSTC].asInstanceOf[RoomAttendingMaster.EnteredRoomSTC]
        }
    // 너무 빨라서 그런 듯 하다. 약간의 시간차를 줘야 바뀐 대화방목록을 볼 수 있다.
    Thread.sleep(100)
    fold(
      Get("/room")
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
        assertResult(RoomMaster.ListedRoomSTC(List((createdRoomResult.roomNo.get, "hi1", "room")))) {
          //
          Await.result(
            responseEntity.dataBytes
              .map(_.utf8String.parseJson.convertTo[(Int, String, String)])
              .fold(List[(Int, String, String)]())((l, x) => x :: l)
              .map(RoomMaster.ListedRoomSTC(_))
              .runWith(Sink.head),
            10.seconds
          )
        }
      }
    fold(
      Get(s"/room/${createdRoomResult.roomNo.get}")
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
        assertResult(Room.GotRoomStatusSTC(0, "hi1", "room", Map("hi1" -> false))) {
          Await.result(
            Room.fromJson(responseEntity.dataBytes).runWith(Sink.head),
            10.seconds
          ).copy(version = 0) // 부득이하게 이와 같이 version 을 바꾼다. version 까지 예상할 수는 없기 때문이다.
        }
      }
    fold(
      Delete(
        s"/roomAttending/${enterResult1.raaId.get}",
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.LeaveRoomCTS
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      )
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
      }
    fold(
      Get("/room")
    )(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
        assertResult(StatusCodes.OK)(status)
        assertResult(RoomMaster.ListedRoomSTC(List())) {
          Await.result(
            responseEntity.dataBytes
              .map(_.utf8String.parseJson.convertTo[(Int, String, String)])
              .fold(List[(Int, String, String)]())((l, x) => x :: l)
              .map(RoomMaster.ListedRoomSTC(_))
              .runWith(Sink.head),
            10.seconds
          )
        }
      }
    fold(Put("/logout"))(List(addHeader(Cookie(cookiePair1)))) ~> route ~> check {
      assertResult(StatusCodes.OK)(status)
    }
  }

  override protected def afterEach(): Unit = {
    super.afterEach()
    val watch = TestProbe()
    watch.watch(roomMasterActorRef)
    watch.watch(userMasterActorRef)
    watch.watch(authenticateActorRef)
    watch.watch(idLookupActorRef)
    // 하나씩 순서대로 종료해야 한다. 한꺼번에 종료시키면 어떤 것이 먼저 종료하는지 알 수 없다.
    // user master actor 를 먼저 종료요청했지만 authenticate actor 가 먼저 종료할 수 있다.
    system.stop(roomMasterActorRef)
    watch.expectTerminated(roomMasterActorRef)
    system.stop(userMasterActorRef)
    watch.expectTerminated(userMasterActorRef)
    system.stop(authenticateActorRef)
    watch.expectTerminated(authenticateActorRef)
    system.stop(idLookupActorRef)
    watch.expectTerminated(idLookupActorRef)
    idLookupActorRef = ActorRef.noSender
    authenticateActorRef = ActorRef.noSender
    userMasterActorRef = ActorRef.noSender
  }

  override protected def beforeEach(): Unit = {
    idLookupActorRef = TestActorRef[IdLookupActor](Props[IdLookupActor])
    authenticateActorRef = TestActorRef[AuthenticateActor[Unit]](Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(5.seconds)))
    userMasterActorRef = TestActorRef[UserMasterActor](Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 5.seconds, 3.second)))
  }
}
