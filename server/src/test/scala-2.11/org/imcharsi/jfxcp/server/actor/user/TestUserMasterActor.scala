/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.user

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import org.scalatest.FunSuiteLike

import scala.concurrent.duration._

/**
 * Created by i on 7/23/16.
 */
class TestUserMasterActor extends TestKit(ActorSystem()) with FunSuiteLike {
  test("create user worker actor success") {
    val authenticateActorRef = TestProbe("authenticateActor")
    val idLookupActorRef = TestProbe("idLookupActor")
    val roomMasterActorRef = TestProbe("idLookupActor")
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef.ref, idLookupActorRef.ref, roomMasterActorRef.ref, 10.seconds, 10.seconds, 10.seconds)))
    userMasterActorRef.tell(UserMasterActor.CreateUserWorkerActor(1), testActor)
    expectMsgType[UserMasterActor.CreatedUserWorkerActor] match {
      case UserMasterActor.CreatedUserWorkerActor(Some(_)) =>
      case _ => fail()
    }
  }
  test("create user worker actor duplicated fail") {
    val authenticateActorRef = TestProbe("authenticateActor")
    val idLookupActorRef = TestProbe("idLookupActor")
    val roomMasterActorRef = TestProbe("idLookupActor")
    val userMasterActorRef = TestActorRef(Props(new UserMasterActor(authenticateActorRef.ref, idLookupActorRef.ref, roomMasterActorRef.ref, 10.seconds, 10.seconds, 10.seconds)))
    userMasterActorRef.tell(UserMasterActor.CreateUserWorkerActor(1), testActor)
    expectMsgType[UserMasterActor.CreatedUserWorkerActor] match {
      case UserMasterActor.CreatedUserWorkerActor(Some(_)) =>
      case _ => fail()
    }
    userMasterActorRef.tell(UserMasterActor.CreateUserWorkerActor(1), testActor)
    expectMsgType[UserMasterActor.CreatedUserWorkerActor] match {
      case UserMasterActor.CreatedUserWorkerActor(None) =>
      case _ => fail()
    }
  }
}
