/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.server.actor.user

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import org.imcharsi.jfxcp.common.roommaster.RoomMaster
import org.imcharsi.jfxcp.server.actor.auth.AuthenticateActor
import org.scalatest.FunSuiteLike

import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by i on 7/23/16.
 */
class TestUserWorkerActor extends TestKit(ActorSystem()) with FunSuiteLike {
  test("authenticate success") {
    val authenticateActorRef = TestProbe("authenticateActor")
    val idLookupActorRef = TestProbe("idLookupActor")
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userWorkerActorRef = TestActorRef(Props(new UserWorkerActor(authenticateActorRef.ref, idLookupActorRef.ref, roomMasterActorRef.ref, 10.seconds, 10.seconds, 10.seconds)))
    userWorkerActorRef.tell(UserWorkerActor.Authenticate("hi", "", false), testActor)
    authenticateActorRef.expectMsg(AuthenticateActor.Authenticate("hi", "", false))
    userWorkerActorRef.tell(AuthenticateActor.Authenticated(Some("hi".hashCode)), authenticateActorRef.ref)
    expectMsg(UserWorkerActor.Authenticated(true))
  }
  test("authenticate fail") {
    val authenticateActorRef = TestProbe("authenticateActor")
    val idLookupActorRef = TestProbe("idLookupActor")
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userWorkerActorRef = TestActorRef(Props(new UserWorkerActor(authenticateActorRef.ref, idLookupActorRef.ref, roomMasterActorRef.ref, 10.seconds, 10.seconds, 10.seconds)))
    userWorkerActorRef.tell(UserWorkerActor.Authenticate("hi", "", false), testActor)
    authenticateActorRef.expectMsg(AuthenticateActor.Authenticate("hi", "", false))
    watch(userWorkerActorRef)
    userWorkerActorRef.tell(AuthenticateActor.Authenticated(None), authenticateActorRef.ref)
    expectMsg(UserWorkerActor.Authenticated(false))
    expectTerminated(userWorkerActorRef)
  }
  test("init/create web socket actor") {
    val authenticateActorRef = TestProbe("authenticateActor")
    val idLookupActorRef = TestProbe("idLookupActor")
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val routee = TestProbe("routee")
    val userWorkerActorRef = TestActorRef[UserWorkerActor](Props(new UserWorkerActor(authenticateActorRef.ref, idLookupActorRef.ref, roomMasterActorRef.ref, 10.seconds, 10.seconds, 10.seconds)))
    userWorkerActorRef.tell(UserWorkerActor.Authenticate("hi", "", false), testActor)
    authenticateActorRef.expectMsg(AuthenticateActor.Authenticate("hi", "", false))
    userWorkerActorRef.tell(AuthenticateActor.Authenticated(Some("hi".hashCode)), authenticateActorRef.ref)
    expectMsg(UserWorkerActor.Authenticated(true))
    userWorkerActorRef.tell(UserWorkerActor.Init, testActor)
    expectMsg(UserWorkerActor.Inited(true))
    // 초기화 검사 끝.
    assertResult(userWorkerActorRef.underlyingActor.oneTimeMessageActorRef)(
      Some(
        Await.result(
          system.actorSelection(userWorkerActorRef.path / UserWorkerActor.nameOneTimeMessageActor).resolveOne(10.seconds), 10.seconds
        )
      )
    )
    // OneTimeMessageActor 구하기 검사.
    userWorkerActorRef.underlyingActor.broadcaster = userWorkerActorRef.underlyingActor.broadcaster.addRoutee(routee.ref)
    userWorkerActorRef.tell(UserWorkerActor.CreateWebSocketActor, testActor)
    assertResult(true)(expectMsgType[UserWorkerActor.CreatedWebSocketActor].webSocketActorRef.isDefined)
    routee.expectMsgType[UserWorkerActor.BroadcastWebSocketActor]
    // web socket actor 만들기 검사 끝.
    val webSocketActorRef = userWorkerActorRef.underlyingActor.webSocketActorRef.get
    routee.watch(webSocketActorRef)
    // 앞서 web socket actor 가 만들어지면서 BroadcastWebSocketActor 를 받은 onetime message actor 가 보낸 BatchQueueOneTimeMessageOTW 가
    // 대상 web socket actor 로 전달되기 전에, 새 web socket actor 를 만들면서 대상 web socket actor 가 종료되고
    // 따라서 dead-letters 로 가게 되는 문제가 생긴다.
    // 이것은 어쩔 수 없는 문제인데, 대기시간을 걸어놔도 검사를 수행하는 기계의 성능에 따라서 dead-letters 문제가 보일 수도 있고 안 보일 수도 있다.
    // 결론은, 대기시간을 걸지 않고 그냥 놔둔다.
    // room attending master actor/room attending actor 의 구상을 바꾸면서 room attending master actor 도 BatchQueueOneTimeMessageOTW 를 보내게 되었다.
    // 그래서 이 단계에서 dead-letters 가 두개가 보이게 된다.
    userWorkerActorRef.tell(UserWorkerActor.CreateWebSocketActor, testActor)
    //    expectMsg(UserWorkerActor.CreatedWebSocketActor(true))
    assertResult(true)(expectMsgType[UserWorkerActor.CreatedWebSocketActor].webSocketActorRef.isDefined)
    routee.expectMsgType[UserWorkerActor.BroadcastWebSocketActor]
    routee.expectTerminated(webSocketActorRef)
    // web socket actor 중복해서 만들기 검사 끝.
  }
  test("timeout1") {
    val authenticateActorRef = TestProbe("authenticateActor")
    val idLookupActorRef = TestProbe("idLookupActor")
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userWorkerActorRef = TestActorRef(Props(new UserWorkerActor(authenticateActorRef.ref, idLookupActorRef.ref, roomMasterActorRef.ref, 1.seconds, 1.seconds, 1.seconds)))
    userWorkerActorRef.tell(UserWorkerActor.Authenticate("hi", "", false), testActor)
    authenticateActorRef.expectMsg(AuthenticateActor.Authenticate("hi", "", false))
    watch(userWorkerActorRef)
    expectTerminated(userWorkerActorRef)
  }
  test("timeout2") {
    val authenticateActorRef = TestProbe("authenticateActor")
    val idLookupActorRef = TestProbe("idLookupActor")
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userWorkerActorRef = TestActorRef(Props(new UserWorkerActor(authenticateActorRef.ref, idLookupActorRef.ref, roomMasterActorRef.ref, 1.seconds, 1.seconds, 1.seconds)))
    userWorkerActorRef.tell(UserWorkerActor.Authenticate("hi", "", false), testActor)
    authenticateActorRef.expectMsg(AuthenticateActor.Authenticate("hi", "", false))
    userWorkerActorRef.tell(AuthenticateActor.Authenticated(Some("hi".hashCode)), authenticateActorRef.ref)
    expectMsg(UserWorkerActor.Authenticated(true))
    watch(userWorkerActorRef)
    expectTerminated(userWorkerActorRef)
  }
  test("timeout3") {
    val authenticateActorRef = TestProbe("authenticateActor")
    val idLookupActorRef = TestProbe("idLookupActor")
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userWorkerActorRef = TestActorRef(Props(new UserWorkerActor(authenticateActorRef.ref, idLookupActorRef.ref, roomMasterActorRef.ref, 1.seconds, 1.seconds, 1.seconds)))
    userWorkerActorRef.tell(UserWorkerActor.Authenticate("hi", "", false), testActor)
    authenticateActorRef.expectMsg(AuthenticateActor.Authenticate("hi", "", false))
    userWorkerActorRef.tell(AuthenticateActor.Authenticated(Some("hi".hashCode)), authenticateActorRef.ref)
    expectMsg(UserWorkerActor.Authenticated(true))
    userWorkerActorRef.tell(UserWorkerActor.Init, testActor)
    expectMsg(UserWorkerActor.Inited(true))
    watch(userWorkerActorRef)
    expectTerminated(userWorkerActorRef)
  }
  test("refresh timeout") {
    val authenticateActorRef = TestProbe("authenticateActor")
    val idLookupActorRef = TestProbe("idLookupActor")
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userWorkerActorRef = TestActorRef(Props(new UserWorkerActor(authenticateActorRef.ref, idLookupActorRef.ref, roomMasterActorRef.ref, 1.seconds, 1.seconds, 1.seconds)))
    userWorkerActorRef.tell(UserWorkerActor.Authenticate("hi", "", false), testActor)
    authenticateActorRef.expectMsg(AuthenticateActor.Authenticate("hi", "", false))
    userWorkerActorRef.tell(AuthenticateActor.Authenticated(Some("hi".hashCode)), authenticateActorRef.ref)
    expectMsg(UserWorkerActor.Authenticated(true))
    userWorkerActorRef.tell(UserWorkerActor.Init, testActor)
    expectMsg(UserWorkerActor.Inited(true))
    watch(userWorkerActorRef)
    // 1초의 시간초과가 걸려있고, 0.75초의 시간이 지난 후에 갱신을 시도하고 다시 이어서 0.75초의 시간을 기다리면
    // 합쳐서 1.5 의 시간을 기다리게 되어 1초를 넘게 되지만 중간에 갱신을 시도했으므로
    // user worker actor 는 종료하지 않아야 하고
    Thread.sleep(750)
    userWorkerActorRef.tell(UserWorkerActor.RefreshTimeout, testActor)
    Thread.sleep(750)
    // 따라서 1.5초의 시간을 기다린 시점에서는 Terminated 는 도착하지 않아야 한다.
    expectNoMsg(0.second)
    expectTerminated(userWorkerActorRef)
  }
  test("forward create room") {
    val authenticateActorRef = TestProbe("authenticateActor")
    val idLookupActorRef = TestProbe("idLookupActor")
    val roomMasterActorRef = TestProbe("roomMasterActor")
    val userWorkerActorRef = TestActorRef(Props(new UserWorkerActor(authenticateActorRef.ref, idLookupActorRef.ref, roomMasterActorRef.ref, 1.seconds, 1.seconds, 1.seconds)))
    userWorkerActorRef.tell(UserWorkerActor.Authenticate("hi", "", false), testActor)
    authenticateActorRef.expectMsg(AuthenticateActor.Authenticate("hi", "", false))
    userWorkerActorRef.tell(AuthenticateActor.Authenticated(Some("hi".hashCode)), authenticateActorRef.ref)
    expectMsg(UserWorkerActor.Authenticated(true))
    userWorkerActorRef.tell(UserWorkerActor.Init, testActor)
    expectMsg(UserWorkerActor.Inited(true))
    userWorkerActorRef.tell(RoomMaster.CreateRoomCTS("hi", "subject"), testActor)
    roomMasterActorRef.expectMsg(RoomMaster.CreateRoomCTS("hi", "subject"))
    roomMasterActorRef.lastSender.tell(RoomMaster.CreatedRoomSTC(Some(1)), roomMasterActorRef.ref)
    expectMsg(RoomMaster.CreatedRoomSTC(Some(1)))
  }
}
