/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.clientServerIT.it

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.layout.{ Priority, HBox, VBox }
import javafx.stage.Stage

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import org.imcharsi.jfxcp.client.ui.Client.ClientUI
import org.imcharsi.jfxcp.server.ui.Server.ServerUI
import rx.lang.scala.JavaConversions
import rx.lang.scala.schedulers.IOScheduler
import rx.schedulers.JavaFxScheduler

import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by i on 8/10/16.
 */
class IntegratedClientServer extends Application {

  val javaFxScheduler = JavaConversions.javaSchedulerToScalaScheduler(JavaFxScheduler.getInstance())
  val javaFxWorker = javaFxScheduler.createWorker
  val ioScheduler = IOScheduler()
  val ioWorker = ioScheduler.createWorker
  val actorSystemClient = ActorSystem()
  val actorMaterializerClient = ActorMaterializer()(actorSystemClient)
  val http = Http()(actorSystemClient)
  val superPool = http.superPool[Int]()(actorMaterializerClient)
  val clientUI =
    new ClientUI(
      javaFxScheduler, javaFxWorker, ioScheduler, ioWorker,
      http, superPool
    )(actorSystemClient, actorMaterializerClient)
  val serverUI = new ServerUI(javaFxScheduler, javaFxWorker, ioScheduler, ioWorker)

  object vBox extends VBox {
    object hBox extends HBox {
      getChildren.addAll(serverUI.serverName, serverUI.serverPort, serverUI.button)
    }
    getChildren.addAll(hBox, clientUI.root)
    VBox.setVgrow(clientUI.root, Priority.ALWAYS)
  }

  val scene = new Scene(vBox)

  override def start(primaryStage: Stage): Unit = {
    primaryStage.setScene(scene)
    primaryStage.show()
  }

  override def stop(): Unit = {
    super.stop()
    clientUI.stop()
    serverUI.stop()
    Await.ready(actorSystemClient.terminate(), 10.seconds)
  }
}

object IntegratedClientServer {
  def main(args: Array[String]): Unit = Application.launch(classOf[IntegratedClientServer], args: _*)
}
