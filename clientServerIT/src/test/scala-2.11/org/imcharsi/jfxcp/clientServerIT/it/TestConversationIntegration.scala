/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.clientServerIT.it

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model.headers.{ Cookie, `Set-Cookie` }
import akka.http.scaladsl.model.{ HttpRequest, HttpResponse, StatusCodes }
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import com.softwaremill.session.{ SessionConfig, SessionManager, SessionUtil }
import org.imcharsi.jfxcp.client.ui.{ ConversationUtil, PutSubject, WebSocketClientActorBase }
import org.imcharsi.jfxcp.common.onetimemessage.OneTimeMessage.ScheduledOneTimeMessageOTC
import org.imcharsi.jfxcp.common.room.Room
import org.imcharsi.jfxcp.common.websocket.WebSocket
import org.imcharsi.jfxcp.common.websocket.WebSocket.{ OneTimeMessageAckSTC, OneTimeMessageSTC }
import org.imcharsi.jfxcp.server.actor.auth.{ AuthenticateActor, IdLookupActor }
import org.imcharsi.jfxcp.server.actor.room.RoomMasterActor
import org.imcharsi.jfxcp.server.actor.user.UserMasterActor
import org.imcharsi.jfxcp.server.route.RouteTemplate
import org.scalatest.{ BeforeAndAfterEach, FunSuiteLike }
import rx.lang.scala.Subject

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }
import scala.util.{ Success, Try }

/**
 * Created by i on 8/9/16.
 */
class TestConversationIntegration extends TestKit(ActorSystem()) with FunSuiteLike with BeforeAndAfterEach {
  implicit val sessionManager = new SessionManager[Long](SessionConfig.default(SessionUtil.randomServerSecret()))
  implicit val actorMaterializer = ActorMaterializer()
  val http = Http()
  val superPool: Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _] = http.superPool[Int]()
  var idLookupActorRef: ActorRef = ActorRef.noSender
  var authenticateActorRef: ActorRef = ActorRef.noSender
  var userMasterActorRef: ActorRef = ActorRef.noSender
  var roomMasterActorRef: ActorRef = ActorRef.noSender
  var serverBinding: Future[ServerBinding] = Future.failed(new Exception)

  def dummyRetrieveF(correctId: List[String])(id: String, password: String): Future[Option[Unit]] = {
    Future {
      if (correctId.contains(id))
        Some(())
      else
        None
    }(Implicits.global)
  }

  test("integration") {
    // impo 실제로 client 에서 사용되는 server 접속 기능과, 실제로 server 에서 사용되는 route 기능을 섞어서 검사한다. 번거롭지만, 이와 같이 해야 올바른 검사이다.
    // 접속.
    val cookie =
      Await.result(ConversationUtil.loginF(superPool, ("localhost", "8080"), ("hi1", "password")).runWith(Sink.head), 10.seconds) match {
        case Success(HttpResponse(StatusCodes.OK, headers, _, _)) =>
          headers.filter {
            case `Set-Cookie`(_) => true
            case _ => false
          }.headOption match {
            case Some(`Set-Cookie`(cookie)) => Cookie(cookie.pair())
            case _ => fail()
          }
        case _ =>
          fail()
      }

    // web socket 접속.
    val testWebSocketClientActorRef = TestActorRef[TestWebSocketClientActor](Props(new TestWebSocketClientActor))
    Await.result(
      ConversationUtil.connectF(http, superPool, ("localhost", "8080"), Future.successful(testWebSocketClientActorRef), cookie)
        .runWith(Sink.head), 10.seconds
    ) match {
        case Success((response, _)) =>
          assertResult(StatusCodes.SwitchingProtocols)(response.response.status)
        case _ =>
          fail()
      }

    // 대화.
    val (userNo, msgNo) =
      Await.result(ConversationUtil.sendIdChat(superPool, ("localhost", "8080"), ("hi1", "hi"), cookie).runWith(Sink.head), 10.seconds) match {
        case (Success(ScheduledOneTimeMessageOTC(userNo, msgNo)), _) => (userNo, msgNo)
      }
    // 대화에 이어 server 로 부터 받은 자료 검사.
    assertResult(true)(
      List(
        testWebSocketClientActorRef.underlyingActor.testProbe.expectMsgType[Any],
        testWebSocketClientActorRef.underlyingActor.testProbe.expectMsgType[Any]
      )
        // 이와 같이 검사하는 이유는 순서를 정확히 알 수 없기 때문이다. 순서는 아무때나 바뀔 수 있다.
        .exists {
          // 일회용 대화를 보내는 사람으로 받아야 하는 자료도 받고
          case WebSocket.OneTimeMessageAckSTC(userNoX, msgNoX) if userNo == userNoX && msgNo == msgNoX => true
          // 일회용 대화를 받는 사람으로서 받아야 하는 자료도 받는다.
          case WebSocket.OneTimeMessageSTC(_, _, userNoX, msgNoX) if userNo == userNoX && msgNo == msgNoX => true
          case _ => false
        }
    )

    // 대화방 만들기.
    val (roomNo, raaId) =
      Await.result(ConversationUtil.createRoomF(superPool, ("localhost", "8080"), "hi", cookie).runWith(Sink.head), 10.seconds) match {
        case Some((roomNo, raaId, cookie)) => (roomNo, raaId)
        case _ => fail()
      }

    val subject = Subject[WebSocket.WebSocketMessageSTC]
    val testProbeFromSubject = TestProbe("testProbeFromSubject")
    subject.subscribe(testProbeFromSubject.ref.tell(_, ActorRef.noSender))
    // client 단계에서 대화방 subject 등록.
    testWebSocketClientActorRef.tell(PutSubject(raaId, Some(subject)), ActorRef.noSender)
    // 대화방 subject 를 등록했을때만, client 단계에서 server 로부터 자료를 받았음을 확인할 수 있다.
    testProbeFromSubject.expectMsgType[WebSocket.RoomAnnounceSTC]
    testProbeFromSubject.expectMsgType[WebSocket.RoomStatusChangedSTC]

    // 대화.
    assertResult(true)(
      Await.result(ConversationUtil.chatF(superPool, ("localhost", "8080"), raaId, cookie, "hi").runWith(Sink.head), 10.seconds)
    )
    testProbeFromSubject.expectMsgType[WebSocket.RoomChatSTC]
    // 대화방 정보 구하기.
    assertResult(Some(Room.GotRoomStatusSTC(0, "hi1", "hi", Map("hi1" -> false))))(
      Await.result(
        ConversationUtil.roomStatusChangedF(superPool, ("localhost", "8080"), cookie, roomNo, raaId).runWith(Sink.head), 10.seconds
      ).map(_.copy(version = 0)) // 부득이하게 이와 같이 version 을 바꾼다. version 까지 예상할 수는 없기 때문이다.
    )

    // 대화방 나가기.
    assertResult(true)(
      Await.result(ConversationUtil.leaveRoomF(superPool, ("localhost", "8080"), raaId, cookie).runWith(Sink.head), 10.seconds)
    )

    // 접속 끊기.
    watch(testWebSocketClientActorRef)
    Await.result(ConversationUtil.logoutF(superPool, ("localhost", "8080"), cookie).runWith(Sink.head), 10.seconds) match {
      case Success(HttpResponse(StatusCodes.OK, _, _, _)) =>
      case _ => fail()
    }
    // 접속이 끊기면, web socket 도 끊긴다.
    expectTerminated(testWebSocketClientActorRef)
  }

  // TestRouteTemplateWithRoute 에서 발췌했다.
  override protected def afterEach(): Unit = {
    super.afterEach()
    Await.ready(serverBinding.flatMap(_.unbind())(Implicits.global), 10.seconds)
    val watch = TestProbe()
    watch.watch(roomMasterActorRef)
    watch.watch(userMasterActorRef)
    watch.watch(authenticateActorRef)
    watch.watch(idLookupActorRef)
    system.stop(roomMasterActorRef)
    watch.expectTerminated(roomMasterActorRef)
    system.stop(userMasterActorRef)
    watch.expectTerminated(userMasterActorRef)
    system.stop(authenticateActorRef)
    watch.expectTerminated(authenticateActorRef)
    system.stop(idLookupActorRef)
    watch.expectTerminated(idLookupActorRef)
    idLookupActorRef = ActorRef.noSender
    authenticateActorRef = ActorRef.noSender
    roomMasterActorRef = ActorRef.noSender
    userMasterActorRef = ActorRef.noSender
  }

  override protected def beforeEach(): Unit = {
    idLookupActorRef = TestActorRef[IdLookupActor](Props[IdLookupActor])
    authenticateActorRef = TestActorRef[AuthenticateActor[Unit]](Props(new AuthenticateActor[Unit](idLookupActorRef, dummyRetrieveF(List("hi1", "hi2"))(_, _))))
    roomMasterActorRef = TestActorRef[RoomMasterActor](Props(new RoomMasterActor(5.seconds)))
    userMasterActorRef = TestActorRef[UserMasterActor](Props(new UserMasterActor(authenticateActorRef, idLookupActorRef, roomMasterActorRef, 1.second, 5.seconds, 3.second)))
    serverBinding = http.bindAndHandle(
      Route.handlerFlow(
        RouteTemplate
          .finalRoute(userMasterActorRef, roomMasterActorRef)
      ), "localhost", 8080
    )
  }
}

class TestWebSocketClientActor(implicit actorSystem: ActorSystem) extends WebSocketClientActorBase {
  // web socket 으로부터 받은 자료를 쌓아두기 위해 쓴다. 따로 queue 를 만들 필요가 없다. 사실상 queue 처럼 동작한다.
  val testProbe = TestProbe()

  // 왜 이와 같이 하는가. 검사를 편하게 하기 위해서이다.
  override def clearWebSocketClientActorRef(): Unit = {
    super.clearWebSocketClientActorRef()
  }

  override def oneTimeMessageSTC(id: String, chat: String, userNo: Int, msgNo: Int): Unit = {
    super.oneTimeMessageSTC(id, chat, userNo, msgNo)
    testProbe.ref.tell(OneTimeMessageSTC(id, chat, userNo, msgNo), ActorRef.noSender)
  }

  override def oneTimeMessageAckSTC(userNo: Int, msgNo: Int): Unit = {
    super.oneTimeMessageAckSTC(userNo, msgNo)
    testProbe.ref.tell(OneTimeMessageAckSTC(userNo, msgNo), ActorRef.noSender)
  }
}
