/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.client.ui

import javafx.beans.binding.When
import javafx.beans.property._
import javafx.collections.FXCollections
import javafx.geometry.{ Insets, Orientation }
import javafx.scene.control._
import javafx.scene.layout._

import akka.actor.ActorRef
import akka.http.scaladsl.model.headers.Cookie
import com.sun.javafx.scene.control.skin.ListViewSkin
import org.imcharsi.jfxcp.common.util.ObservableUtil
import rx.lang.scala.{ JavaConversions => JConv, Scheduler }
import rx.observables.{ JavaFxObservable => JFO }

/**
 * Created by i on 8/6/16.
 */
object UI {

  class UIHierarchy(javaFxScheduler: Scheduler) extends BorderPane {

    object vBox extends VBox {
      styleProperty().set("-fx-border-color:green;-fx-border-width:5px;")

      object hBoxLogin extends HBox {
        val cookie = new SimpleObjectProperty[Option[Cookie]](None)
        val loggedIn = JFXUtil.customBooleanBinding(cookie)(_.isDefined)
        val temporaryDisable = new SimpleBooleanProperty(false)
        val temporaryWebSocketDisable = new SimpleBooleanProperty(false)
        val webSocketClientActor = new SimpleObjectProperty[Option[ActorRef]](None)
        val connected = JFXUtil.customBooleanBinding(webSocketClientActor)(_.isDefined)
        val observableLogin = JConv.toScalaObservable(JFO.fromActionEvents(buttonLogin))
        val observableId = JConv.toScalaObservable(JFO.fromObservableValue(textFieldId.textProperty()))
        val observablePassword = JConv.toScalaObservable(JFO.fromObservableValue(passwordFieldPassword.textProperty()))
        val observableWebSocket = JConv.toScalaObservable(JFO.fromActionEvents(buttonWebSocket))

        object textFieldServerName extends TextField {
          disableProperty().bind(loggedIn.or(temporaryDisable))
          textProperty().set("localhost")
          prefWidthProperty().set(120)
        }

        object textFieldServerPort extends TextField {
          disableProperty().bind(loggedIn.or(temporaryDisable))
          textProperty().set("8080")
          prefWidthProperty().set(80)
        }

        object textFieldId extends TextField {
          disableProperty().bind(loggedIn.or(temporaryDisable))
          prefWidthProperty().set(80)
        }

        object passwordFieldPassword extends PasswordField {
          disableProperty().bind(loggedIn.or(temporaryDisable))
          prefWidthProperty().set(80)
        }

        object buttonLogin extends ToggleButton {
          textProperty().bind(new When(loggedIn).`then`("logout").otherwise("login"))
          minWidthProperty().set(100)
          disableProperty().bind(
            temporaryDisable
              .or(temporaryWebSocketDisable) // web socket 연결 중 logout 을 시도할 수 있다.
              .or(
                textFieldId.textProperty().length().greaterThan(0)
                  .and(passwordFieldPassword.textProperty().length().greaterThan(0)).not()
              )
          )
        }

        object buttonWebSocket extends ToggleButton {
          textProperty().bind(new When(connected).`then`("disconnect").otherwise("connect"))
          minWidthProperty().set(100)
          disableProperty().bind(loggedIn.not().or(temporaryWebSocketDisable))
        }

        val observableServerNamePort =
          JConv.toScalaObservable(JFO.fromObservableValue(textFieldServerName.textProperty()))
            .combineLatest(JConv.toScalaObservable(JFO.fromObservableValue(textFieldServerPort.textProperty())))

        // scan 을 써도 문제를 풀 수 있었다. 잘 된다. 좀 더 번거로운 점은 있다.
        val observableIdPasswordLogin =
          ObservableUtil.observableABThenB(
            observableLogin,
            observableServerNamePort
              .combineLatest(observableId.combineLatest(observablePassword))
              .combineLatest(JConv.toScalaObservable(JFO.fromObservableValue(loggedIn)))
          ).filterNot(_._2).map(_._1)

        val observableLogout =
          ObservableUtil.observableABOptionCThenBC(
            observableLogin,
            observableServerNamePort,
            JConv.toScalaObservable(JFO.fromObservableValue(cookie))
          )

        val observableWebSocketConnect =
          ObservableUtil.observableABOptionCThenBC(
            JConv.toScalaObservable(JFO.fromActionEvents(buttonWebSocket)),
            observableServerNamePort.combineLatest(
              JConv.toScalaObservable(JFO.fromObservableValue(connected))
            ),
            JConv.toScalaObservable(JFO.fromObservableValue(cookie))
          ).filterNot(_._1._2).map(x => (x._1._1, x._2))

        val observableWebSocketDisconnect =
          ObservableUtil.observableAOptionBThenB(
            JConv.toScalaObservable(JFO.fromActionEvents(buttonWebSocket)),
            JConv.toScalaObservable(JFO.fromObservableValue(webSocketClientActor))
          )

        getChildren.addAll(textFieldServerName, textFieldServerPort, textFieldId, passwordFieldPassword, buttonLogin, buttonWebSocket)
        HBox.setHgrow(textFieldServerName, Priority.ALWAYS)
        HBox.setHgrow(textFieldServerPort, Priority.ALWAYS)
        HBox.setHgrow(textFieldId, Priority.ALWAYS)
        HBox.setHgrow(passwordFieldPassword, Priority.ALWAYS)
      }

      object tabPaneMain extends TabPane {

        object vBoxChat extends VBox {
          styleProperty().set("-fx-border-color:yellow;-fx-border-width:5px;")
          val listChat = FXCollections.observableArrayList[(String, String, StringProperty)]()
          val mapStringProperty = new SimpleObjectProperty[Map[(Int, Int), StringProperty]](Map())

          object hBoxChat extends HBox {
            val temporaryDisable = new SimpleBooleanProperty(false)
            val observableSend =
              JConv.toScalaObservable(JFO.fromActionEvents(buttonSend))
            val observableId =
              JConv.toScalaObservable(JFO.fromObservableValue(textFieldId.textProperty()))
            val observableChat =
              JConv.toScalaObservable(JFO.fromObservableValue(textFieldChat.textProperty()))

            val observableIdChat =
              ObservableUtil.observableABOptionCThenBC(
                observableSend,
                hBoxLogin.observableServerNamePort
                  .combineLatest(observableId.combineLatest(observableChat)),
                JConv.toScalaObservable(JFO.fromObservableValue(hBoxLogin.cookie))
              )

            disableProperty().bind(hBoxLogin.loggedIn.not())

            object textFieldId extends TextField {
              prefWidthProperty().set(100)
            }

            object textFieldChat extends TextField

            object buttonRandomChat extends Button {
              textProperty().set("random chat")
              disableProperty().bind(temporaryDisable.or(textFieldId.textProperty().isEmpty()))
            }

            object buttonSend extends Button {
              textProperty().set("send")
              disableProperty()
                .bind(
                  temporaryDisable.or(
                    textFieldId.textProperty().isNotEmpty
                      .and(textFieldChat.textProperty().isNotEmpty).not()
                  )
                )
            }

            getChildren.addAll(textFieldId, textFieldChat, buttonRandomChat, buttonSend)
            HBox.setHgrow(textFieldChat, Priority.ALWAYS)
          }

          object listViewChat extends ListView[(String, String, StringProperty)] {
            private val anotherSkin = new ListViewSkinEx[(String, String, StringProperty)](this)
            val verticalScrollbar = anotherSkin.vertical
            itemsProperty().set(listChat)
            setCellFactory(JFXUtil.callback(_ => new ChatListCell))

            override def createDefaultSkin(): Skin[_] = anotherSkin

            // impo 아주 억지스럽다. 다른 좋은 방법 없나. 또한 잘 안되기도 하다.
            // 왜 안되는가. Stage.show 를 하기 전까지는 구현 상세인 css 로 정의된 각종 scroll-bar 등은 아직 만들어지지 않기 때문이다.
            // https://bugs.openjdk.java.net/browse/JDK-8096847
            // http://stackoverflow.com/questions/21476244/javafx-listview-scrollbar-eventfilter-and-scrolltotop
          }

          // 처음 구상할 때는, 이 기능이 web socket client actor 의 oneTimeMessageSTC template method 에 있었다.
          // 설명글을 준비하면서 이와 같은 구성이 rx 를 연습하는 취지에 맞지 않음을 알아서
          // 이와 같이 분리하였다.
          val observableOneTimeMessageAdd =
            ObservableUtil.observableABThenB(
              JConv.toScalaObservable(JFO.fromObservableListAdds(listViewChat.itemsProperty().get())),
              JConv.toScalaObservable(JFO.fromObservableValue(listViewChat.verticalScrollbar.valueProperty()))
            )
              .map(_.doubleValue())
              .scan((-1.toDouble, true)) {
                case ((oldScrollPosition, alwaysToBottom), (scrollPosition)) =>
                  if (alwaysToBottom && oldScrollPosition <= scrollPosition)
                    (scrollPosition, true)
                  else
                    (scrollPosition, false)
              }
              .map {
                case (1, _) => true
                case (_, true) => true
                case _ => false
              }

          getChildren.addAll(hBoxChat, listViewChat)
          VBox.setVgrow(listViewChat, Priority.ALWAYS)
        }

        object vBoxRoomList extends VBox {

          object hBoxCreateRoom extends HBox {

            disableProperty().bind(hBoxLogin.loggedIn.not())

            object textFieldRoomName extends TextField {
              prefWidthProperty().set(200)
            }

            object buttonCreateRoom extends Button {
              val temporaryDisable = new SimpleBooleanProperty(false)
              textProperty().set("create room")
              disableProperty().bind(textFieldRoomName.textProperty().isEmpty.or(temporaryDisable))
            }

            object buttonRefreshRoomList extends Button {
              textProperty().set("refresh room list")
            }

            object buttonEnterRoom extends Button {
              textProperty().set("enter room")
              disableProperty().bind(tableViewRoomList.getSelectionModel.selectedItemProperty().isNull)
            }

            val observableCreateRoom =
              ObservableUtil.observableABOptionCThenBC(
                JConv.toScalaObservable(JFO.fromActionEvents(buttonCreateRoom)),
                hBoxLogin.observableServerNamePort.combineLatest(
                  JConv.toScalaObservable(JFO.fromObservableValue(textFieldRoomName.textProperty()))
                ),
                JConv.toScalaObservable(JFO.fromObservableValue(hBoxLogin.cookie))
              )

            val observableEnterRoom =
              ObservableUtil.observableABOptionCThenBC(
                JConv.toScalaObservable(JFO.fromActionEvents(buttonEnterRoom)),
                hBoxLogin.observableServerNamePort
                  .combineLatest(JConv.toScalaObservable((JFO.fromObservableValue(tableViewRoomList.getSelectionModel.selectedItemProperty()))))
                  .combineLatest(JConv.toScalaObservable(JFO.fromObservableValue(hBoxLogin.webSocketClientActor))),
                JConv.toScalaObservable(JFO.fromObservableValue(hBoxLogin.cookie))
              ).map(x => (x._1._1._1, x._1._1._2, x._1._2, x._2))

            val observableRefreshRoomList =
              ObservableUtil.observableABOptionCThenBC(
                JConv.toScalaObservable(JFO.fromActionEvents(buttonRefreshRoomList)),
                hBoxLogin.observableServerNamePort,
                JConv.toScalaObservable(JFO.fromObservableValue(hBoxLogin.cookie))
              )

            getChildren.add(textFieldRoomName)
            getChildren.add(buttonCreateRoom)
            getChildren.add(buttonRefreshRoomList)
            getChildren.add(buttonEnterRoom)
            HBox.setHgrow(textFieldRoomName, Priority.ALWAYS)
          }

          object tableViewRoomList extends TableView[(Int, String, String)] {
            // 아래의 tableViewEnteredRoom 에서 베꼈다.
            val tableColumnRoomNo = new TableColumn[(Int, String, String), Number]("roomNo")
            val tableColumnOwner = new TableColumn[(Int, String, String), String]("owner")
            val tableColumnSubject = new TableColumn[(Int, String, String), String]("subject")
            tableColumnRoomNo.setCellValueFactory(JFXUtil.utilCellValueFactory(x => new SimpleIntegerProperty(x.getValue._1)))
            tableColumnOwner.setCellValueFactory(JFXUtil.utilCellValueFactory(x => new SimpleStringProperty(x.getValue._2)))
            tableColumnSubject.setCellValueFactory(JFXUtil.utilCellValueFactory(x => new SimpleStringProperty(x.getValue._3)))
            tableColumnRoomNo.setCellFactory(
              JFXUtil.utilTableColumnFactory(_ =>
                JFXUtil.utilTableCell((a, b, c) => if (c) a.textProperty().set(null) else a.textProperty().set(b.toString)))
            )
            tableColumnOwner.setCellFactory(
              JFXUtil.utilTableColumnFactory(_ =>
                JFXUtil.utilTableCell((a, b, c) => if (c) a.textProperty().set(null) else a.textProperty().set(b)))
            )
            tableColumnSubject.setCellFactory(
              JFXUtil.utilTableColumnFactory(_ =>
                JFXUtil.utilTableCell((a, b, c) => if (c) a.textProperty().set(null) else a.textProperty().set(b)))
            )

            // http://stackoverflow.com/questions/30288558/make-the-last-column-in-a-javafx-tableview-take-the-remaining-space
            // impo 마지막 열만 table view 의 나머지 공간을 모두 차지하게 하고 싶을 때 이와 같이 하라 한다.
            // 이와 같은 요령은 다른 곳에서도 쓸 수 있다.
            tableColumnSubject
              .prefWidthProperty()
              .bind(
                widthProperty()
                  .subtract(tableColumnRoomNo.widthProperty())
                  .subtract(tableColumnOwner.widthProperty())
              )
            getColumns.addAll(tableColumnRoomNo, tableColumnOwner, tableColumnSubject)

          }

          getChildren.add(hBoxCreateRoom)
          getChildren.add(tableViewRoomList)
          VBox.setVgrow(tableViewRoomList, Priority.ALWAYS)
        }

        object vBoxChatRoom extends VBox {
          styleProperty().set("-fx-border-color:violet;-fx-border-width:5px;")

          object tableViewEnteredRoom extends TableView[ChatRoom] {
            val tableColumnRoomNo = new TableColumn[ChatRoom, Number]("roomNo")
            val tableColumnOwner = new TableColumn[ChatRoom, String]("owner")
            val tableColumnSubject = new TableColumn[ChatRoom, String]("subject")
            val tableColumnWorking = new TableColumn[ChatRoom, java.lang.Boolean]("working")
            tableColumnRoomNo.setCellValueFactory(JFXUtil.utilCellValueFactory(_.getValue.roomNoProperty))
            tableColumnOwner.setCellValueFactory(JFXUtil.utilCellValueFactory(_.getValue.ownerIdProperty))
            tableColumnSubject.setCellValueFactory(JFXUtil.utilCellValueFactory(_.getValue.subjectProperty))
            tableColumnWorking.setCellValueFactory(JFXUtil.utilCellValueFactory(_.getValue.workingProperty))
            // combo box 가 바뀌면 pane 도 바뀌도록 하는 기능은 아래와 같이 하면 된다.
            // 현재 선택되지 않은 대화방에서 대화가 오면 대화방 목록에서 읽지 않은 대화가 있음을 표시하는 기능이 있으면 유용하다.
            // 어떻게 할것인가.
            // 현재 선택된 대화방의 room no 를 나타내는 전역변수를 둔 다음, 모든 대화방에서 이 변수를 참조하여
            // 오는 대화가 있는데 현재 선택된 대화방이 아니면 대화방목록의 표시에 사용되는 문장을 바꾼다.
            // 한편, 준비된 문장을 어떻게 쓸 것인가 문제되는데, bind 와 같은 방법으로 자동으로 바뀌도록 할 수 있다면 좋겠지만
            // 안된다면 아래와 같이 문장을 만들어야 할 필요가 있을 때마다 매번 문장을 만드는 식으로 하는 방법도 있다.
            // combo box 로 한번 선택되어 표시된 이후에는 관련 자료가 바뀌어도 bind 로서 자동으로 바뀌지 않는다.
            // 그래서 구상에 맞는 기능은 아니다. list view 를 쓰고 stack pane 으로 겹친 후 나타났다가 사라졌다가 할 수 있게 하는 것이 좋겠다.
            tableColumnRoomNo.setCellFactory(
              JFXUtil.utilTableColumnFactory(_ =>
                JFXUtil.utilTableCell((a, b, c) => if (c) a.textProperty().set(null) else a.textProperty().set(b.toString)))
            )
            tableColumnOwner.setCellFactory(
              JFXUtil.utilTableColumnFactory(_ =>
                JFXUtil.utilTableCell((a, b, c) => if (c) a.textProperty().set(null) else a.textProperty().set(b)))
            )
            tableColumnSubject.setCellFactory(
              JFXUtil.utilTableColumnFactory(_ =>
                JFXUtil.utilTableCell((a, b, c) => if (c) a.textProperty().set(null) else a.textProperty().set(b)))
            )
            tableColumnWorking.setCellFactory(
              JFXUtil.utilTableColumnFactory(_ =>
                JFXUtil.utilTableCell((a, b, c) => if (c) a.textProperty().set(null) else a.textProperty().set(b.toString)))
            )

            getColumns.addAll(tableColumnRoomNo, tableColumnOwner, tableColumnSubject, tableColumnWorking)
            // 이 기능을 사용하면 낭비가 약간 있지만, 각 대화방 자료의 list 내 위치가 수시로 바뀌는 상황에서 삭제를 쉽게 할 수 있다.
            // 대화방 번호를 제외한 모든 자료를 전부 대화방 object 에 넣어두는 것이 좋겠다.
            //              itemsProperty().get().removeIf()

            // http://stackoverflow.com/questions/26298337/tableview-adjust-number-of-visible-rows
            // table view 의 보이는 행 수를 조절하는 기능은 없다고 한다.
            minHeightProperty().set(150)
            maxHeightProperty().set(150)
            tableColumnSubject.prefWidthProperty().bind(
              widthProperty()
                .subtract(tableColumnRoomNo.widthProperty())
                .subtract(tableColumnOwner.widthProperty())
                .subtract(tableColumnWorking.widthProperty())
            )
          }

          // http://stackoverflow.com/questions/32212560/javafx-8-maxwidth-ignored
          // Pane 으로 해봤는데, 나머지 공간을 전부 채우기와 같은 기능이 동작하지 않는다.
          // StackPane 으로 하니깐 잘 된다.
          object paneChatRoom extends StackPane

          getChildren.addAll(tableViewEnteredRoom, paneChatRoom)
          VBox.setVgrow(paneChatRoom, Priority.ALWAYS)
        }

        object tabChat extends Tab {
          textProperty().set("onetime message")
          contentProperty().set(vBoxChat)
          closableProperty().set(false)
        }

        object tabRoomList extends Tab {
          textProperty().set("room list")
          contentProperty().set(vBoxRoomList)
          closableProperty().set(false)
        }

        object tabChatRoom extends Tab {
          textProperty().set("chat room")
          contentProperty().set(vBoxChatRoom)
          closableProperty().set(false)
        }

        getTabs.add(tabChat)
        getTabs.add(tabRoomList)
        getTabs.add(tabChatRoom)
        styleProperty().set("-fx-border-color:red;-fx-border-width:5px;")
      }

      VBox.setVgrow(tabPaneMain, Priority.ALWAYS)
      getChildren.addAll(hBoxLogin, tabPaneMain)
    }

    centerProperty().set(vBox)
  }

  class ChatListCell extends ListCell[(String, String, StringProperty)] {

    object hBox extends HBox {

      object id extends Label {
        prefWidthProperty().set(80)
        paddingProperty().set(new Insets(0, 5, 0, 5))
      }

      object chat extends Label

      object receiveStatus extends Label {
        prefWidthProperty().set(60)
        paddingProperty().set(new Insets(0, 5, 0, 5))
      }

      getChildren.addAll(id, chat, receiveStatus)
      HBox.setHgrow(chat, Priority.ALWAYS)
    }

    override def updateItem(item: (String, String, StringProperty), empty: Boolean): Unit = {
      super.updateItem(item, empty)
      if (empty) {
        setGraphic(null)
      } else {
        hBox.id.textProperty().set(item._1)
        hBox.chat.textProperty().set(item._2)
        hBox.receiveStatus.textProperty().bind(item._3)
        setGraphic(hBox)
      }
    }
  }

  // http://stackoverflow.com/questions/27886650/tableview-scrollbar-policy
  class ListViewSkinEx[A](listView: ListView[A]) extends ListViewSkin[A](listView) {

    import scala.collection.JavaConverters._

    private val result =
      flow.getChildrenUnmodifiable.asScala.toStream
        .filter(_.isInstanceOf[ScrollBar])
        .map(_.asInstanceOf[ScrollBar]).partition(_.getOrientation == Orientation.VERTICAL)
    val vertical = result._1.head
    val horizontal = result._2.head
  }

  class RoomListCell extends ListCell[(Int, String, String)] {

  }
}
