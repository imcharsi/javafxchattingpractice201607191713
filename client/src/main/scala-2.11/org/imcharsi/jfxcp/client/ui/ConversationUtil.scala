/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.client.ui

import javafx.collections.{ FXCollections, ObservableList }

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{ Authorization, BasicHttpCredentials, Cookie }
import akka.http.scaladsl.model.ws.{ Message, TextMessage, WebSocketRequest, WebSocketUpgradeResponse }
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.stream.actor.{ ActorPublisher, ActorSubscriber }
import akka.stream.scaladsl.{ Flow, Keep, Sink, Source }
import akka.util.ByteString
import org.imcharsi.jfxcp.common.onetimemessage.OneTimeMessage
import org.imcharsi.jfxcp.common.onetimemessage.OneTimeMessage.OneTimeMessageRequestCTO
import org.imcharsi.jfxcp.common.room.Room
import org.imcharsi.jfxcp.common.roomattending.RoomAttending
import org.imcharsi.jfxcp.common.roomattendingmaster.RoomAttendingMaster
import org.imcharsi.jfxcp.common.roommaster.RoomMaster
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 8/6/16.
 */
object ConversationUtil {
  // 이와 같이 Source 가 결과가 되도록 해야 검사하기가 편하다.
  // akka stream 의 결과인 future 를 rx observable 로 포장한 다음에
  // rx test subscriber 로 결과를 확인하고자 하면 모든 단계를 거치는 데 걸리는 시간이 길어서
  // rx test subscriber 가 먼저 실행을 마치게 되고
  // 따라서 rx test subscriber 는 아무것도 못보게 된다.
  def loginF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    idPassword:     (String, String)
  )(implicit actorMaterializer: ActorMaterializer): Source[Try[HttpResponse], _] = {
    Source.single(
      (HttpRequest(
        HttpMethods.POST,
        s"http://${serverNamePort._1}:${serverNamePort._2}/login",
        // 요청번호가 문제되는 경우가 있을 수 있다. 대표적인 예가 먼저 출발한 요청이 늦게 도작하는 경우이다.
        // 완전히 다른 stream 에서 만든 요청의 응답이 이 stream 에 섞여 들 수는 없지만,
        // 같은 stream 에서 만든 요청의 응답 순서가 달라지는 경우는 있을 수 있다.
        // 문제되는 것은, 한 개의 stream 에서 여러 유형의 요청과 응답이 섞여서,
        // 보낸 순서와 다르게 응답이 왔을 때 이 응답이 언제 보낸 요청유형에 대한 응답인지 구분할 수 없는 점이다.
        // stream 마다 한 가지 유형의 요청만 만든다면 아무 문제없다.
        List(Authorization(BasicHttpCredentials(idPassword._1, idPassword._2)))
      ), 1)
    )
      .via(superPool)
      .map(_._1)
  }

  def logoutF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    cookie:         Cookie
  )(implicit actorMaterializer: ActorMaterializer): Source[Try[HttpResponse], _] = {
    // todo delete 로 바꿔보기.
    Source.single((HttpRequest(HttpMethods.PUT, s"http://${serverNamePort._1}:${serverNamePort._2}/logout", List(cookie)), 1))
      .via(superPool)
      .map(_._1)
  }

  def connectF(
    http:                       HttpExt,
    superPool:                  Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort:             (String, String),
    futureWebSocketClientActor: Future[ActorRef],
    cookie:                     Cookie
  )(implicit actorSystem: ActorSystem, actorMaterializer: ActorMaterializer): Source[Try[(WebSocketUpgradeResponse, ActorRef)], _] = {
    Source.fromFuture(
      futureWebSocketClientActor
        .flatMap { webSocketClientActor =>
          Try {
            val flow = Flow.fromSinkAndSource(
              Flow[Message].flatMapConcat {
                case TextMessage.Streamed(stream) => stream
                case TextMessage.Strict(string) => Source.single(string)
                case _ => Source.empty
              }.to(Sink.fromSubscriber(ActorSubscriber[String](webSocketClientActor))),
              Source.fromPublisher(ActorPublisher[Message](webSocketClientActor))
            )
            http.singleWebSocketRequest(WebSocketRequest(s"ws://${serverNamePort._1}:${serverNamePort._2}/webSocket", List(cookie)), flow)._1
              .map(Success(_, webSocketClientActor))(Implicits.global)
              .recover {
                case x =>
                  Console.err.println(s"recover ${x}")
                  actorSystem.stop(webSocketClientActor)
                  Failure(x)
              }(Implicits.global)
          } match {
            case Success(x) => x
            case Failure(ex) =>
              // impo 경로가 틀리는 것은 상관없는데, scheme 을 틀리게 쓰면 그 다음부터 동작을 하지 않는다.
              // 왜냐하면 scheme 을 틀릴 때 생기는 예외는 future 안에서 생기는 예외가 아니고
              // method 안에서 future 를 만들기 전에 생기는 예외이기 때문이다.
              // 그래서 이와 같이 예외를 잡아야 한다.
              actorSystem.stop(webSocketClientActor)
              Future.failed(ex)
          }
          // impo 아주 이상한 경우인데, web socket 연결이 실패하는 경우 singleWebSocketRequest 의 결과인 future 는 완료하지 않는다.
          // impo https://github.com/akka/akka/issues/21051
          // 개발자들도 문제있다는 걸 인지하고 대안도 준비해놨다. 다음 version 에 병합시켜놨다 한다.
          // 그래서 2.4.9 version 을 전제하고, 아무것도 할 필요없다.
        }(Implicits.global)
    )
  }

  def disconnectF(webSocketClientActorRef: ActorRef)(implicit actorMaterializer: ActorMaterializer): Source[Try[Unit], _] = {
    Source.fromFuture(
      webSocketClientActorRef.ask(StopWebSocketClientActor)(10.seconds)
        .map(_ => Success(()))(Implicits.global)
        .recover { case ex => Failure(ex) }(Implicits.global)
    )
  }

  def sendIdChat(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    idChat:         (String, String),
    cookie:         Cookie
  )(implicit actorMaterializer: ActorMaterializer): Source[(Try[OneTimeMessage.OneTimeMessageResponseOTC], OneTimeMessage.OneTimeMessageRequestCTO), _] = {
    val (id, chat) = idChat
    val oneTimeMessageCTO = OneTimeMessage.OneTimeMessageCTO(id, chat)
    def parseResponse(t: Try[HttpResponse]): Future[Try[OneTimeMessage.OneTimeMessageResponseOTC]] = {
      t match {
        case Success(response) =>
          response.entity.dataBytes
            .map(x => Try(x.utf8String.parseJson.convertTo[OneTimeMessage.OneTimeMessageResponseOTC]))
            .toMat(Sink.head)(Keep.right)
            .run()
        case Failure(ex) => Future.failed(ex)
      }
    }
    Source.single((
      HttpRequest(
        HttpMethods.POST,
        s"http://${serverNamePort._1}:${serverNamePort._2}/oneTimeMessage",
        List(cookie),
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(oneTimeMessageCTO.asInstanceOf[OneTimeMessageRequestCTO].toJson.prettyPrint)
            .map(ByteString(_))
        )
      ), 2
    ))
      .via(superPool)
      .flatMapConcat(x => Source.fromFuture(parseResponse(x._1)))
      .map((_, oneTimeMessageCTO))
  }

  def createRoomF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    //    javaFxScheduler: Scheduler,
    //    ioScheduler:     Scheduler,
    subject: String, cookie: Cookie
  )(implicit actorMaterializer: ActorMaterializer): Source[Option[(Int, Int, Cookie)], _] = {
    val createRoomCTS = RoomMaster.CreateRoomCTS("", subject)
    Source.single((
      HttpRequest(
        HttpMethods.POST,
        s"http://${serverNamePort._1}:${serverNamePort._2}/room",
        List(cookie),
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(createRoomCTS.asInstanceOf[RoomMaster.RoomMasterRequestCTS].toJson.prettyPrint)
            .map(ByteString(_))
        )
      ), 3
    ))
      .via(superPool)
      .flatMapConcat {
        case (Success(HttpResponse(StatusCodes.OK, _, entity, _)), 3) =>
          entity.dataBytes
            .map(_.utf8String.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC])
      }
      .flatMapConcat {
        case RoomMaster.CreatedRoomSTC(Some(roomNo)) =>
          commonEnterRoomF(superPool, serverNamePort, roomNo, cookie)
      }
  }

  def listRoomF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    cookie:         Cookie
  )(implicit actorMaterializer: ActorMaterializer): Source[ObservableList[(Int, String, String)], _] = {
    Source.single((
      HttpRequest(
        HttpMethods.GET,
        s"http://${serverNamePort._1}:${serverNamePort._2}/room",
        List(cookie)
      ), 6
    ))
      .via(superPool)
      .flatMapConcat {
        case (Success(HttpResponse(StatusCodes.OK, _, entity, _)), 6) =>
          entity.dataBytes
            .map(_.utf8String.parseJson.convertTo[(Int, String, String)])
            .fold(FXCollections.observableArrayList[(Int, String, String)]()) { (l, x) => l.add(x); l }
      }
  }

  def commonEnterRoomF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    roomNo:         Int,
    cookie:         Cookie
  )(implicit actorMaterializer: ActorMaterializer): Source[Option[(Int, Int, Cookie)], _] = {
    Source.single((
      HttpRequest(
        HttpMethods.POST,
        s"http://${serverNamePort._1}:${serverNamePort._2}/roomAttending",
        List(cookie),
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttendingMaster.EnterRoomCTS(roomNo)
            .asInstanceOf[RoomAttendingMaster.RoomAttendingMasterRequestCTS]
            .toJson.prettyPrint
          )
            .map(ByteString(_))
        )
      ), 5
    ))
      .via(superPool)
      .flatMapConcat {
        case (Success(HttpResponse(StatusCodes.OK, _, entity, _)), 5) =>
          entity.dataBytes
            .map(_.utf8String.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC])
            .map {
              case RoomAttendingMaster.EnteredRoomSTC(Some(raaId)) => Some((roomNo, raaId, cookie))
              case _ => None
            }
        case _ => Source.single(None)
      }
  }

  def enterRoomF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    roomNo:         Int,
    cookie:         Cookie
  )(implicit actorMaterializer: ActorMaterializer): Source[Option[(Int, Int, Cookie)], _] = {
    commonEnterRoomF(superPool, serverNamePort, roomNo, cookie)
  }

  def roomStatusChangedF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    cookie:         Cookie,
    roomNo:         Int,
    raaId:          Int
  )(implicit actorMaterializer: ActorMaterializer): Source[Option[(Room.GotRoomStatusSTC /*, ObservableList[(Option[RoomAttenderType], String)]*/ )], _] = {
    Source.single((
      HttpRequest(
        HttpMethods.GET,
        s"http://${serverNamePort._1}:${serverNamePort._2}/roomAttending/${raaId}/ping",
        List(cookie)
      ), 9
    ))
      .via(superPool)
      .flatMapConcat {
        case ((Success(HttpResponse(StatusCodes.OK, _, _, _)), 9)) =>
          Source.single((
            HttpRequest(
              HttpMethods.GET,
              s"http://${serverNamePort._1}:${serverNamePort._2}/room/${roomNo}",
              List(cookie)
            ), 7
          ))
            .via(superPool)
            .flatMapConcat {
              case (Success(HttpResponse(StatusCodes.OK, _, entity, _)), 7) =>
                Room.fromJson(entity.dataBytes).map(Some(_))
              // 이 단계에서 알 수 없다면, 대화방이 없거나 접속이 끊겼거나 등의 이유이고
              case _ => Source.single(None)
            }
        // 이 단계에서 알 수 없다면 대화방에서 퇴장했거나 등의 이유이다.
        case _ => Source.single(None)
      }
  }

  def chatF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    raaId:          Int,
    cookie:         Cookie,
    s:              String
  )(implicit actorMaterializer: ActorMaterializer): Source[Boolean, _] = {
    Source.single((
      HttpRequest(
        HttpMethods.POST,
        s"http://${serverNamePort._1}:${serverNamePort._2}/roomAttending/${raaId}",
        List(cookie),
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.ChatCTS(s)
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      ), 8
    ))
      .via(superPool)
      .map {
        case (Success(HttpResponse(StatusCodes.OK, _, entity, _)), 8) =>
          true
        case x =>
          Console.err.println(x)
          false
      }
  }

  def grantOperatorF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    raaId:          Int,
    cookie:         Cookie,
    granteeId:      String
  )(implicit actorMaterializer: ActorMaterializer): Source[Boolean, _] = {
    Source.single((
      HttpRequest(
        HttpMethods.POST,
        s"http://${serverNamePort._1}:${serverNamePort._2}/roomAttending/${raaId}",
        List(cookie),
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.GrantOperatorCTS(granteeId)
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      ), 8
    ))
      .via(superPool)
      .map {
        case (Success(HttpResponse(StatusCodes.OK, _, entity, _)), 8) =>
          true
        case x =>
          Console.err.println(x)
          false
      }
  }

  def revokeOperatorF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    raaId:          Int,
    cookie:         Cookie,
    granteeId:      String
  )(implicit actorMaterializer: ActorMaterializer): Source[Boolean, _] = {
    Source.single((
      HttpRequest(
        HttpMethods.POST,
        s"http://${serverNamePort._1}:${serverNamePort._2}/roomAttending/${raaId}",
        List(cookie),
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.RevokeOperatorCTS(granteeId)
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      ), 8
    ))
      .via(superPool)
      .map {
        case (Success(HttpResponse(StatusCodes.OK, _, entity, _)), 8) =>
          true
        case x =>
          Console.err.println(x)
          false
      }
  }

  def leaveRoomF(
    superPool:      Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    serverNamePort: (String, String),
    raaId:          Int,
    cookie:         Cookie
  )(implicit actorMaterializer: ActorMaterializer): Source[Boolean, _] = {
    Source.single((
      HttpRequest(
        HttpMethods.DELETE,
        s"http://${serverNamePort._1}:${serverNamePort._2}/roomAttending/${raaId}",
        List(cookie),
        HttpEntity(
          ContentTypes.`application/json`,
          Source.single(
            RoomAttending.LeaveRoomCTS
            .asInstanceOf[RoomAttending.RoomAttendingRequestCTS]
            .toJson.prettyPrint
          ).map(ByteString(_))
        )
      ), 5
    ))
      .via(superPool)
      .flatMapConcat {
        case (Success(HttpResponse(StatusCodes.OK, _, entity, _)), 5) =>
          entity.dataBytes
            .map(_.utf8String.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC])
            .map {
              case RoomAttending.LeavedRoomSTC(true) => true
              case _ => false
            }
        case x =>
          Console.err.println(x)
          Source.single(false)
      }
  }
}
