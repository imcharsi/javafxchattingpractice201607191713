/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.client.ui

import javafx.beans.property.{ BooleanProperty, ObjectProperty, SimpleStringProperty, StringProperty }
import javafx.scene.control.{ ListView, ScrollBar, ToggleButton }

import akka.actor.{ ActorRef, Cancellable, ReceiveTimeout }
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.stream.actor.ActorPublisherMessage.{ Cancel, Request }
import akka.stream.actor.ActorSubscriberMessage.{ OnComplete, OnError, OnNext }
import akka.stream.actor.{ ActorPublisher, ActorSubscriber, RequestStrategy, WatermarkRequestStrategy }
import org.imcharsi.jfxcp.common.websocket.WebSocket
import rx.lang.scala.{ Subject, Worker }
import spray.json._

import scala.collection.immutable.Queue
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 8/6/16.
 */
class WebSocketClientActorBase extends ActorPublisher[Message] with ActorSubscriber {
  private var queue: Queue[WebSocket.WebSocketMessageCTS] = Queue()
  private var cancellableTimeout: Option[Cancellable] = None
  // ChatRoom 에서 subject 를 전달받으면 관련 대화방의 자료를 전부 전달하기.
  private var roomSubjectMap: Map[Int, Option[Subject[WebSocket.WebSocketMessageSTC]]] = Map()
  // server 에서 오는 대화방 자료는 전부 쌓아놓기.
  private var roomMessageQueue: Map[Int, Queue[WebSocket.WebSocketMessageSTC]] = Map()

  override protected def requestStrategy: RequestStrategy = WatermarkRequestStrategy(16)

  def receiveRoomMessage(x: WebSocket.WebSocketMessageSTC, raaId: Int, msgNo: Int): Unit = {
    queue = queue.enqueue(WebSocket.TransferredRoomMessageCTS(raaId, msgNo))
    processQueue()
    roomSubjectMap.get(raaId) match {
      case Some(Some(subject)) => subject.onNext(x)
      case Some(None) =>
      case None =>
        roomMessageQueue.get(raaId) match {
          case Some(queue) => roomMessageQueue = roomMessageQueue + (((raaId, queue.enqueue(x))))
          case None => roomMessageQueue = roomMessageQueue + (((raaId, Queue(x))))
        }
    }
  }
  override def preStart(): Unit = {
    super.preStart()
    cancellableTimeout = Some(context.system.scheduler.scheduleOnce(5.seconds, self, ReceiveTimeout)(Implicits.global))
    queue = queue.enqueue(WebSocket.GreetingFirstCTS)
  }

  override def postStop(): Unit = {
    super.postStop()
    cancellableTimeout.foreach(_.cancel())
    cancellableTimeout = None
    // web socket actor 의 stop 은, 대응하는 web socket 의 종료이므로,
    // 이와 같이 하면 화면을 자동으로 갱신할 수 있어서 편리하다.
    clearWebSocketClientActorRef()
  }

  // 왜 이와 같이 하는가. 검사를 편하게 하기 위해서이다.
  def clearWebSocketClientActorRef(): Unit = {}

  private def processQueue(): Unit = {
    while (totalDemand > 0 && !queue.isEmpty) {
      val (webSocketMessage, newQueue) = queue.dequeue
      queue = newQueue
      Try(webSocketMessage.toJson.prettyPrint) match {
        case Success(s) => onNext(TextMessage.Strict(s))
        case Failure(ex) =>
          onError(ex)
          context.stop(self)
      }
    }
  }

  def oneTimeMessageSTC(id: String, chat: String, userNo: Int, msgNo: Int): Unit = {}

  def oneTimeMessageAckSTC(userNo: Int, msgNo: Int): Unit = {}

  private def processIncoming(s: String): Unit = {
    Try(s.parseJson.convertTo[WebSocket.WebSocketMessageSTC]) match {
      case Success(WebSocket.GreetingSecondSTC) =>
        cancellableTimeout.foreach(_.cancel())
        cancellableTimeout = None
      case Success(x @ WebSocket.OneTimeMessageSTC(id, chat, userNo, msgNo)) =>
        oneTimeMessageSTC(id, chat, userNo, msgNo)
        queue = queue.enqueue(WebSocket.TransferredOneTimeMessageCTS(userNo, msgNo))
        processQueue()
      case Success(x @ WebSocket.OneTimeMessageAckSTC(userNo, msgNo)) =>
        queue = queue.enqueue(WebSocket.TransferredOneTimeMessageAckCTS(userNo, msgNo))
        oneTimeMessageAckSTC(userNo, msgNo)
        processQueue()
      case Success(x @ WebSocket.RoomAnnounceSTC(raaId, msgNo, _)) =>
        receiveRoomMessage(x, raaId, msgNo)
      case Success(x @ WebSocket.RoomChatSTC(raaId, msgNo, _, _)) =>
        receiveRoomMessage(x, raaId, msgNo)
      case Success(x @ WebSocket.RoomStatusChangedSTC(raaId, msgNo)) =>
        receiveRoomMessage(x, raaId, msgNo)
      case _ =>
        onError(new Exception)
        context.stop(self)
    }
  }

  private def receiveAsPublisher: Receive = {
    case Request(n) =>
      processQueue()
    case Cancel =>
      context.stop(self)
  }

  private def receiveAsSubscriber: Receive = {
    case OnNext(s: String) =>
      processIncoming(s)
    case OnError(ex) =>
      onError(ex)
      // onErrorThenStop() 이라 하니, postStop() 이 될 때도 있고 안 될때도 있다.
      context.stop(self)
    case OnComplete =>
      onComplete()
      context.stop(self)
  }

  private def receiveAsWebSocketClient: Receive = {
    case ReceiveTimeout =>
      cancellableTimeout = None
      onError(new Exception)
      context.stop(self)
    case StopWebSocketClientActor =>
      cancellableTimeout.foreach(_.cancel())
      cancellableTimeout = None
      sender().tell((), self)
      onComplete()
      context.stop(self)
    case PutSubject(raaId, subject) =>
      roomSubjectMap = roomSubjectMap + (((raaId, subject)))
      roomSubjectMap.get(raaId) match {
        case Some(Some(subject)) =>
          roomMessageQueue.get(raaId).foreach(_.foreach(subject.onNext(_)))
          roomMessageQueue = roomMessageQueue - raaId
        case Some(None) =>
          roomMessageQueue = roomMessageQueue - raaId
      }
    case RemoveSubject(roomNo) =>
      roomSubjectMap = roomSubjectMap - roomNo
  }

  override def receive: Receive =
    receiveAsPublisher
      .orElse(receiveAsSubscriber)
      .orElse(receiveAsWebSocketClient)
}

class WebSocketClientActor(
    webSocketClientActorRef:   ObjectProperty[Option[ActorRef]],
    buttonWebSocket:           ToggleButton,
    temporaryWebSocketDisable: BooleanProperty,
    mapStringProperty:         ObjectProperty[Map[(Int, Int), StringProperty]],
    verticalScrollbar:         ScrollBar,
    listView:                  ListView[(String, String, StringProperty)],
    javaFxWorker:              Worker
) extends WebSocketClientActorBase {
  // 이와 같이 나눈 이유는, 검사를 편하게 하기 위해서이다.
  // 구체적으로, 검사단계에서는 java fx Application class 를 사용하지 않기 때문에
  // java fx UI 도 만들 수 없다.
  // java fx 기능을 사용하지 않는 web socket client actor 가 있어야 검사를 쉽게 할 수 있다.

  override def oneTimeMessageSTC(id: String, chat: String, userNo: Int, msgNo: Int): Unit = {
    javaFxWorker.schedule {
      // 이 단계는 대화 상대방이 대화를 받는 단계이기 때문에 map 에 StringProperty 를 둘 필요가 없다.
      listView.itemsProperty().get().add((id, chat, new SimpleStringProperty("Rcvd")))
      ()
    }
  }

  override def oneTimeMessageAckSTC(userNo: Int, msgNo: Int): Unit = {
    javaFxWorker.schedule {
      mapStringProperty.get().get((userNo, msgNo)).foreach(_.set("Okay"))
      // 상대방이 받았음을 알았으면 더 이상 map 에 남겨둘 필요 없다.
      mapStringProperty.set(mapStringProperty.get() - ((userNo, msgNo)))
      ()
    }
  }

  override def clearWebSocketClientActorRef(): Unit = {
    javaFxWorker.schedule {
      webSocketClientActorRef.set(None)
      buttonWebSocket.selectedProperty().set(false)
    }
  }
}

case object StopWebSocketClientActor

case class PutSubject(raaId: Int, subject: Option[Subject[WebSocket.WebSocketMessageSTC]])

// 왜 아래와 같은 기능이 또 필요한가.
// 나갔던 대화방을 다시 들어가면 앞서 같은 대화방 번호로 map 에 자료가 있어서
// 새로 대화방에 들어가면서 받는 각종 자료를 놓치게 된다.
// 따라서, 대화방 입장에 앞서서 이와 같이 지워주고 시작하는 것이 필요하다.
case class RemoveSubject(raaId: Int)

// 응답이 필요하다. 가능성은 높지 않지만, actor 가 map 정리를 시도하기 전에 server 로부터 자료를 받을 수 있다.
// 바뀐 구상에 따르면 응답은 필요없다.
//case object RemovedSubject
