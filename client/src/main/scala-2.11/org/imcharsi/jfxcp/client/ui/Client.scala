/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.client.ui

import javafx.beans.property._
import javafx.collections.ObservableList

import akka.actor._
import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.model.ws._
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Flow, Sink }
import org.imcharsi.jfxcp.client.ui.UI.UIHierarchy
import org.imcharsi.jfxcp.common.onetimemessage.OneTimeMessage
import rx.lang.scala.{ JavaConversions => JConv, Observable, Worker }
import rx.observables.{ JavaFxObservable => JFO }

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }
import scala.util.{ Failure, Random, Success, Try }
import scala.collection.JavaConverters._

/**
 * Created by i on 7/29/16.
 */
object Client {

  class ClientUI(
      javaFxScheduler: rx.lang.scala.Scheduler,
      javaFxWorker:    Worker,
      ioScheduler:     rx.lang.scala.Scheduler,
      ioWorker:        Worker,
      http:            HttpExt,
      superPool:       Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _]
  )(implicit actorSystem: ActorSystem, actorMaterializer: ActorMaterializer) {
    val root = new UIHierarchy(javaFxScheduler)

    def loginSubscribeF(result: Try[HttpResponse]): Unit = {
      result match {
        case Success(r @ HttpResponse(StatusCodes.OK, _, _, _)) if r.header[`Set-Cookie`].isDefined =>
          val cookie = Cookie(r.header[`Set-Cookie`].get.cookie.pair())
          root.vBox.hBoxLogin.cookie.set(Some(cookie))
          root.vBox.hBoxLogin.temporaryDisable.set(false)
          root.vBox.hBoxLogin.buttonWebSocket.fire()
        case x =>
          Console.err.println(x)
          root.vBox.hBoxLogin.cookie.set(None)
          root.vBox.hBoxLogin.temporaryDisable.set(false)
          root.vBox.hBoxLogin.buttonLogin.selectedProperty().set(false)
      }
    }

    def logoutSubscribeF(result: Try[HttpResponse]): Unit = {
      result match {
        case Success(r @ HttpResponse(StatusCodes.OK, _, _, _)) =>
        case x =>
          Console.err.println(s"logout ${x}")
      }
      root.vBox.hBoxLogin.cookie.set(None)
      root.vBox.hBoxLogin.temporaryDisable.set(false)
    }

    def connectSubscribeF(t: Try[(WebSocketUpgradeResponse, ActorRef)]): Unit = {
      t match {
        case Success((response, webSocketClientActor)) =>
          Console.err.println(webSocketClientActor)
          response.response match {
            case HttpResponse(StatusCodes.SwitchingProtocols, _, _, _) =>
              root.vBox.hBoxLogin.webSocketClientActor.set(Some(webSocketClientActor))
            case _ =>
              root.vBox.hBoxLogin.buttonWebSocket.selectedProperty().set(false)
          }
        case Failure(ex) =>
          root.vBox.hBoxLogin.buttonWebSocket.selectedProperty().set(false)
      }
      root.vBox.hBoxLogin.temporaryWebSocketDisable.set(false)
    }

    def disconnectSubscribeF(t: Try[Unit]): Unit = {
      t match {
        case Success(_) =>
        case Failure(ex) =>
          Console.err.println(ex)
      }
      root.vBox.hBoxLogin.webSocketClientActor.set(None)
      root.vBox.hBoxLogin.temporaryWebSocketDisable.set(false)
      root.vBox.hBoxLogin.buttonWebSocket.selectedProperty().set(false)
    }

    def oneTimeMessageAddSubscribeF(goToBottom: Boolean): Unit = {
      Console.err.println(goToBottom)
      if (goToBottom)
        root.vBox.tabPaneMain.vBoxChat.listViewChat.scrollTo(
          root.vBox.tabPaneMain.vBoxChat.listViewChat.itemsProperty().get().size() - 1
        )
    }

    def sendIdChatSubscribeF(result: (Try[OneTimeMessage.OneTimeMessageResponseOTC], OneTimeMessage.OneTimeMessageRequestCTO)): Unit = {
      result match {
        case (Success(OneTimeMessage.ScheduledOneTimeMessageOTC(userNo, msgNo)), OneTimeMessage.OneTimeMessageCTO(id, chat)) =>
          Console.err.println("completed 1")
          val resultStringProperty = new SimpleStringProperty("....")
          root.vBox.tabPaneMain.vBoxChat.mapStringProperty
            .set(root.vBox.tabPaneMain.vBoxChat.mapStringProperty.get() + (((userNo, msgNo), resultStringProperty)))
          root.vBox.tabPaneMain.vBoxChat.listChat.add((id, chat, resultStringProperty))
          if (root.vBox.tabPaneMain.vBoxChat.listViewChat.verticalScrollbar.valueProperty().get() == 1)
            root.vBox.tabPaneMain.vBoxChat.listViewChat.scrollTo(root.vBox.tabPaneMain.vBoxChat.listChat.size())
          Console.err.println(userNo, msgNo)
        case x =>
          Console.err.println(s"send fail ${x}")
      }
    }

    def commonEnterRoomSubscribeF(roomNo: Int, raaId: Int, cookie: Cookie): Unit = {
      val chatRoom = new ChatRoom(
        roomNo, raaId, cookie, superPool,
        root.vBox.hBoxLogin.webSocketClientActor,
        javaFxScheduler, ioScheduler,
        root.vBox.tabPaneMain.vBoxChatRoom.tableViewEnteredRoom.itemsProperty().get(),
        root.vBox.hBoxLogin.cookie, root.vBox.hBoxLogin.observableServerNamePort
      )
      chatRoom.workingProperty.set(true)
      root.vBox.tabPaneMain.vBoxChatRoom.tableViewEnteredRoom.itemsProperty().get().add(chatRoom)
      root.vBox.tabPaneMain.vBoxChatRoom.tableViewEnteredRoom.getSelectionModel.select(chatRoom)
      root.vBox.tabPaneMain.getSelectionModel.select(root.vBox.tabPaneMain.tabChatRoom)
      chatRoom.hBoxChatInput.textFieldChat.requestFocus()
    }

    def createRoomSubscribeF(x: Option[(Int, Int, Cookie)]): Unit = {
      // fixme 실패의 원인을 알려야 한다면 이와 같이 해서는 안 된다.
      x.foreach { xx => commonEnterRoomSubscribeF(xx._1, xx._2, xx._3) }
      root.vBox.tabPaneMain.vBoxRoomList.hBoxCreateRoom.buttonCreateRoom.temporaryDisable.set(false)
    }

    def listRoomSubscribeF(r: ObservableList[(Int, String, String)]): Unit = {
      root.vBox.tabPaneMain.vBoxRoomList.tableViewRoomList.itemsProperty().set(r)
    }

    def enterRoomSubscribeF(x: Option[(Int, Int, Cookie)]): Unit = {
      // fixme createRoomSubscribeF 와 같이, 실패의 이유를 나타낸다면 이와 같이 해서는 안 된다.
      x.foreach { xx => commonEnterRoomSubscribeF(xx._1, xx._2, xx._3) }
    }

    def createWebSocketClientActor(javaFxWorker: Worker, borderPane: UI.UIHierarchy)(implicit actorSystem: ActorSystem): Future[ActorRef] =
      Future(
        actorSystem.actorOf(
          Props(
            new WebSocketClientActor(
              borderPane.vBox.hBoxLogin.webSocketClientActor,
              borderPane.vBox.hBoxLogin.buttonWebSocket,
              borderPane.vBox.hBoxLogin.temporaryWebSocketDisable,
              borderPane.vBox.tabPaneMain.vBoxChat.mapStringProperty,
              borderPane.vBox.tabPaneMain.vBoxChat.listViewChat.verticalScrollbar,
              borderPane.vBox.tabPaneMain.vBoxChat.listViewChat,
              javaFxWorker
            )
          )
        )
      )(Implicits.global)

    // button 을 누르면 무조건 비활성화시킨다.
    //      borderPane.vBox.hBoxLogin.observableLogin
    //        .subscribe(_ => borderPane.vBox.hBoxLogin.temporaryDisable.set(true))
    root.vBox.hBoxLogin.observableWebSocket
      .subscribe(_ => root.vBox.hBoxLogin.temporaryWebSocketDisable.set(true))

    // 아래의 처리가 끝나면 활성화시킨다.
    root.vBox.hBoxLogin.observableIdPasswordLogin
      .doOnNext(_ => root.vBox.hBoxLogin.temporaryDisable.set(true))
      .observeOn(ioScheduler)
      .flatMap(x => Observable.from(ConversationUtil.loginF(superPool, x._1, x._2).runWith(Sink.head))(Implicits.global))
      .observeOn(javaFxScheduler)
      .subscribe(loginSubscribeF(_))

    root.vBox.hBoxLogin.observableLogout
      .observeOn(ioScheduler)
      .map(x => ConversationUtil.logoutF(superPool, x._1, x._2).runWith(Sink.head))
      .flatMap(Observable.from(_)(Implicits.global))
      .observeOn(javaFxScheduler)
      .subscribe(logoutSubscribeF(_))

    root.vBox.hBoxLogin.observableWebSocketConnect
      .doOnNext(_ => root.vBox.hBoxLogin.temporaryWebSocketDisable.set(true))
      .observeOn(ioScheduler)
      .map(x => ConversationUtil.connectF(http, superPool, x._1, createWebSocketClientActor(javaFxWorker, root), x._2).runWith(Sink.head))
      .flatMap(Observable.from(_)(Implicits.global))
      .observeOn(javaFxScheduler)
      .subscribe(connectSubscribeF(_))

    root.vBox.hBoxLogin.observableWebSocketDisconnect
      .observeOn(ioScheduler)
      .map(ConversationUtil.disconnectF(_).runWith(Sink.head))
      .flatMap(Observable.from(_)(Implicits.global))
      .observeOn(javaFxScheduler)
      .subscribe(disconnectSubscribeF(_))

    root.vBox.tabPaneMain.vBoxChat.hBoxChat.observableIdChat
      .observeOn(ioScheduler)
      .map(x => ConversationUtil.sendIdChat(superPool, x._1._1, x._1._2, x._2).runWith(Sink.head))
      .flatMap(Observable.from(_)(Implicits.global))
      .observeOn(javaFxScheduler)
      .subscribe(sendIdChatSubscribeF(_))

    JConv.toScalaObservable(JFO.fromActionEvents(root.vBox.tabPaneMain.vBoxChat.hBoxChat.buttonRandomChat))
      .observeOn(javaFxScheduler)
      .subscribe { _ =>
        root.vBox.tabPaneMain.vBoxChat.hBoxChat.textFieldChat.textProperty().set(Random.nextLong().toString)
        root.vBox.tabPaneMain.vBoxChat.hBoxChat.buttonSend.fire()
      }

    root.vBox.tabPaneMain.vBoxChat.observableOneTimeMessageAdd
      .subscribe(oneTimeMessageAddSubscribeF(_))

    root.vBox.tabPaneMain.vBoxRoomList.hBoxCreateRoom.observableRefreshRoomList
      .observeOn(ioScheduler)
      .map(x => ConversationUtil.listRoomF(superPool, x._1, x._2).runWith(Sink.head))
      .flatMap(Observable.from(_)(Implicits.global))
      .observeOn(javaFxScheduler)
      .subscribe(listRoomSubscribeF(_))

    root.vBox.tabPaneMain.vBoxRoomList.hBoxCreateRoom.observableCreateRoom
      .doOnNext(_ => root.vBox.tabPaneMain.vBoxRoomList.hBoxCreateRoom.buttonCreateRoom.temporaryDisable.set(true))
      .observeOn(ioScheduler)
      .map(x => ConversationUtil.createRoomF(superPool, x._1._1, x._1._2, x._2).runWith(Sink.head))
      .flatMap(Observable.from(_)(Implicits.global))
      .observeOn(javaFxScheduler)
      .subscribe(createRoomSubscribeF(_), Console.err.println(_))

    JConv.toScalaObservable(JFO.fromObservableValueChanges(root.vBox.tabPaneMain.vBoxChatRoom.tableViewEnteredRoom.getSelectionModel.selectedItemProperty()))
      .observeOn(javaFxScheduler)
      .subscribe { x =>
        // 참가대화방 선택이 풀리면 아무것도 표시되지 않아야 한다.
        if (x.getNewVal == null) {
          root.vBox.tabPaneMain.vBoxChatRoom.paneChatRoom.getChildren.clear()
        } else {
          root.vBox.tabPaneMain.vBoxChatRoom.paneChatRoom.getChildren.setAll(x.getNewVal)
        }
      }

    root.vBox.tabPaneMain.vBoxRoomList.hBoxCreateRoom.observableEnterRoom
      .observeOn(ioScheduler)
      .map {
        // 최초의 구상과는 다르게, 퇴장 후 동일한 대화방에 입장할 때
        // 앞서 입장하면서 web socket client actor 로 등록한 subject 를 지우지 않는다.
        // 구상에 따르면, web socket 연결 여부와 상관없이 대화방에 입장할 수 있어야 한다.
        case ((serverNamePort, (roomNo, _, _), _, cookie)) => (serverNamePort, roomNo, cookie)
      }
      .map(x => ConversationUtil.enterRoomF(superPool, x._1, x._2, x._3).runWith(Sink.head))
      .flatMap(Observable.from(_)(Implicits.global))
      .observeOn(javaFxScheduler)
      .subscribe(enterRoomSubscribeF(_))

    def stop(): Unit = {
      Await.ready(http.shutdownAllConnectionPools(), 10.seconds)
    }
  }

}

