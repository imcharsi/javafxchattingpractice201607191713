/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.client.ui

import javafx.beans.property._
import javafx.collections.{ FXCollections, ObservableList }
import javafx.geometry.Insets
import javafx.scene.control._
import javafx.scene.input.{ KeyCode, KeyEvent }
import javafx.scene.layout._

import akka.actor.ActorRef
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Cookie
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Flow, Sink }
import org.imcharsi.jfxcp.client.ui.UI.ListViewSkinEx
import org.imcharsi.jfxcp.common.room.Room
import org.imcharsi.jfxcp.common.util.ObservableUtil
import org.imcharsi.jfxcp.common.websocket.WebSocket
import org.imcharsi.jfxcp.common.websocket.WebSocket.WebSocketMessageSTC
import rx.lang.scala.{ JavaConversions => JC, Observable, Scheduler, Subject }
import rx.observables.{ JavaFxObservable => JFO }

import scala.concurrent.ExecutionContext.Implicits
import scala.util.{ Random, Try }

/**
 * Created by i on 8/6/16.
 */
class ChatRoom(
    roomNo:                  Int,
    raaId:                   Int,
    cookie:                  Cookie,
    superPool:               Flow[(HttpRequest, Int), (Try[HttpResponse], Int), _],
    webSocketClientActorRef: ObjectProperty[Option[ActorRef]],
    javaFxScheduler:         Scheduler,
    ioScheduler:             Scheduler,
    tableViewItemsProperty:  ObservableList[ChatRoom],
    cookieProperty:          ObjectProperty[Option[Cookie]],
    serverNamePort:          Observable[(String, String)]
)(
    implicit
    actorMaterializer: ActorMaterializer
) extends VBox {
  val roomNoProperty: IntegerProperty = new SimpleIntegerProperty(roomNo)
  val ownerIdProperty: StringProperty = new SimpleStringProperty()
  val subjectProperty: StringProperty = new SimpleStringProperty()
  val workingProperty: BooleanProperty = new SimpleBooleanProperty(false)
  val subjectWebSocketMessageSTC = Subject[WebSocket.WebSocketMessageSTC]()
  var gotRoomStatusSTCVersion: IntegerProperty = new SimpleIntegerProperty(-1)

  object hBoxChatOutput extends BorderPane {

    object vBoxLeft extends VBox {

      object listViewUsers extends ListView[(List[RoomAttenderType], String)] {
        prefWidthProperty().setValue(100)
        cellFactoryProperty().setValue(JFXUtil.callback(_ => new UserCell))
      }

      object hBoxBottom extends HBox {

        object buttonGrant extends Button {
          textProperty().set("grant")
          disableProperty().bind(
            listViewUsers.getSelectionModel.selectedItemProperty().isNull
              .or(workingProperty.not())
          )
        }

        object buttonRevoke extends Button {
          textProperty().set("revoke")
          disableProperty().bind(
            listViewUsers.getSelectionModel.selectedItemProperty().isNull
              .or(workingProperty.not())
          )
        }

        getChildren.addAll(buttonGrant, buttonRevoke)
      }

      getChildren.addAll(listViewUsers, hBoxBottom)
      VBox.setVgrow(listViewUsers, Priority.ALWAYS)
    }

    object listViewChat extends ListView[WebSocket.WebSocketMessageSTC] {
      private val anotherSkin = new ListViewSkinEx[WebSocket.WebSocketMessageSTC](this)
      val verticalScrollbar = anotherSkin.vertical
      override def createDefaultSkin(): Skin[_] = anotherSkin
      cellFactoryProperty().set(JFXUtil.callback(_ => new ChatCell))
    }

    leftProperty().set(vBoxLeft)
    centerProperty().set(listViewChat)
  }

  object hBoxChatInput extends HBox {

    object textFieldChat extends TextField {
      disableProperty().bind(workingProperty.not())
    }

    object buttonSend extends Button {
      textProperty().set("send")
      disableProperty().bind(textFieldChat.textProperty().isEmpty.or(workingProperty.not()))
    }

    object buttonRandomSend extends Button {
      textProperty().set("random send")
      disableProperty().bind(workingProperty.not())
    }

    object buttonLeave extends Button {
      textProperty().set("leave")
      val temporaryDisable = new SimpleBooleanProperty(false)
      disableProperty().bind(workingProperty.not().or(temporaryDisable))
    }

    object buttonDelete extends Button {
      textProperty().set("delete")
      disableProperty().bind(workingProperty)
    }

    getChildren.addAll(textFieldChat, buttonSend, buttonRandomSend, buttonLeave, buttonDelete)
    // http://dammsebastian.blogspot.kr/2012/03/layout-panes-in-javafx-20-hbox-and-vbox.html
    // child 의 폭이나 높이를 바꾸고 싶을 때.
    HBox.setHgrow(textFieldChat, Priority.ALWAYS)
  }

  // 최초로 대화방을 만들면 server 로부터 room status changed stc 를 받기 때문에 결국 대화방 정보를 구하게 된다.
  def webSocketMessageSTCSubjectF(x: WebSocket.WebSocketMessageSTC): Unit = {
    x match {
      case a @ WebSocket.RoomAnnounceSTC(_, _, _) =>
        hBoxChatOutput.listViewChat.itemsProperty().get().add(a)
      case a @ WebSocket.RoomChatSTC(_, _, _, _) =>
        hBoxChatOutput.listViewChat.itemsProperty().get().add(a)
      case _ =>
    }
  }

  def roomStatusChangedSubscribeF(x: (Option[(Room.GotRoomStatusSTC, ObservableList[(List[RoomAttenderType], String)])], Option[ActorRef], Int)): Unit = {
    x match {
      case (Some((roomStatus, list)), _, version) =>
        if (version < roomStatus.version) {
          gotRoomStatusSTCVersion.set(roomStatus.version)
          ownerIdProperty.set(roomStatus.ownerId)
          subjectProperty.set(roomStatus.subject)
          hBoxChatOutput.vBoxLeft.listViewUsers.itemsProperty().set(list)
        }
      case (None, actorRef, _) =>
        // fixme 이 구절은 다시 써야 한다. http traffic 자체가 실패한 경우는 재시도를 했을 때 성공할 수 있기 때문에 대화방을 닫으면 안 된다.
        workingProperty.set(false)
    }
  }

  def scrollToBottomSubscribeF(x: (Double, Boolean)): Unit = {
    val n = hBoxChatOutput.listViewChat.itemsProperty().get().size()
    //        hBoxChatOutput.listViewChat.verticalScrollbar.setValue(1)
    // http://stackoverflow.com/questions/30683685/why-do-i-get-this-weird-warning-when-i-scroll-to-the-end-of-a-listview-in-javafx
    // 이유를 정확히 모르겠다.
    if (n > 0)
      hBoxChatOutput.listViewChat.scrollTo(n - 1)
  }

  JC.toScalaObservable(JFO.fromNodeEvents(hBoxChatInput.textFieldChat, KeyEvent.KEY_PRESSED))
    .subscribe { x => if (x.getCode == KeyCode.ENTER) hBoxChatInput.buttonSend.fire() }

  JC.toScalaObservable(JFO.fromActionEvents(hBoxChatInput.buttonRandomSend))
    .subscribe { _ =>
      hBoxChatInput.textFieldChat.textProperty().set(Random.nextInt().toString)
      hBoxChatInput.buttonSend.fire()
    }
  // 이 단계를 퇴장할 때 구독해제 되도록 하면 안 된다.
  // 대화방을 퇴장할 때 구독 해제되도록 하면, 퇴장 후 늦게 도착하는 web socket 자료를 놓치게 된다.
  // 또한, web socket client actor 로 subject 를 등록하기에 앞서 미리 구독을 시작해야 web socket 를 놓치지 않을 수 있다.
  val subscriptionChatRoomSubject =
    subjectWebSocketMessageSTC
      .observeOn(javaFxScheduler)
      .subscribe(webSocketMessageSTCSubjectF(_))

  ObservableUtil.observableABThenB(
    JC.toScalaObservable(JFO.fromActionEvents(hBoxChatOutput.vBoxLeft.hBoxBottom.buttonGrant)),
    serverNamePort.combineLatest(
      JC.toScalaObservable(JFO.fromObservableValue(hBoxChatOutput.vBoxLeft.listViewUsers.getSelectionModel.selectedItemProperty()))
        .filter(_ != null)
    )
  )
    .observeOn(ioScheduler)
    .map(x => ConversationUtil.grantOperatorF(superPool, x._1, raaId, cookie, x._2._2).runWith(Sink.head))
    .flatMap(Observable.from(_)(Implicits.global))
    .observeOn(javaFxScheduler)
    .subscribe { x => Console.err.println(x) }

  ObservableUtil.observableABThenB(
    JC.toScalaObservable(JFO.fromActionEvents(hBoxChatOutput.vBoxLeft.hBoxBottom.buttonRevoke)),
    serverNamePort.combineLatest(
      JC.toScalaObservable(JFO.fromObservableValue(hBoxChatOutput.vBoxLeft.listViewUsers.getSelectionModel.selectedItemProperty()))
        .filter(_ != null)
    )
  )
    .observeOn(ioScheduler)
    .map(x => ConversationUtil.revokeOperatorF(superPool, x._1, raaId, cookie, x._2._2).runWith(Sink.head))
    .flatMap(Observable.from(_)(Implicits.global))
    .observeOn(javaFxScheduler)
    .subscribe { x => Console.err.println(x) }

  // 아래와 같이 구독을 명시적으로 푸는 이유는, 구독의 대상이 외부에 있기 때문이다.
  // 이 대화방이 더 이상 사용되지 않는다 해도 구독을 풀지 않는 이상 구독은 계속된다.
  // 더 이상 듣는 이 없는 구독에 자료를 보내는 데 따른 비용도 문제지만
  // 더 문제되는 것은, 아마도 객체참조가 지워지지 않고 계속 남게 되는것 아닌가 한다.
  val listSubscription =
    List(
      ObservableUtil.observableABThenB(
        JC.toScalaObservable(JFO.fromActionEvents(hBoxChatInput.buttonSend)),
        serverNamePort.combineLatest(
          JC.toScalaObservable(JFO.fromObservableValue(hBoxChatInput.textFieldChat.textProperty()))
        )
      )
        .observeOn(ioScheduler)
        .map(x => ConversationUtil.chatF(superPool, x._1, raaId, cookie, x._2).runWith(Sink.head))
        .flatMap(Observable.from(_)(Implicits.global))
        .observeOn(javaFxScheduler)
        .subscribe { _ => hBoxChatInput.textFieldChat.textProperty().set("") },
      ObservableUtil.observableABThenAB(
        subjectWebSocketMessageSTC,
        serverNamePort
          .combineLatest(JC.toScalaObservable(JFO.fromObservableValue(webSocketClientActorRef)))
          .combineLatest(
            JC.toScalaObservable(JFO.fromObservableValue(gotRoomStatusSTCVersion))
              .map(_.intValue())
          )
      )
        .observeOn(ioScheduler)
        .flatMap {
          case (WebSocket.RoomStatusChangedSTC(raaId, _), ((serverNamePort, actorRef), version)) =>
            Observable.from(
              ConversationUtil.roomStatusChangedF(superPool, serverNamePort, cookie, roomNo, raaId)
                .map {
                  case Some(gotRoomStatusSTC) =>
                    Some((
                      gotRoomStatusSTC,
                      gotRoomStatusSTC.list
                      .foldLeft(FXCollections.observableArrayList[(List[RoomAttenderType], String)]()) { (l, x) =>
                        l.add(
                          (List(
                            List(RoomOwner).filter(_ => x._1 == gotRoomStatusSTC.ownerId),
                            List(RoomOperator).filter(_ => x._2)
                          ).flatten, x._1)
                        )
                        l
                      }
                    ))
                }
                .map((_, actorRef, version)).runWith(Sink.head)
            )(Implicits.global)
          case _ => Observable.empty
        }
        .observeOn(javaFxScheduler)
        .subscribe(roomStatusChangedSubscribeF(_)),
      ObservableUtil.observableABThenB(
        JC.toScalaObservable(JFO.fromActionEvents(hBoxChatInput.buttonLeave)),
        serverNamePort.combineLatest(
          JC.toScalaObservable(JFO.fromObservableValue(webSocketClientActorRef))
        )
      )
        .observeOn(javaFxScheduler)
        .doOnNext(_ => hBoxChatInput.buttonLeave.temporaryDisable.set(true))
        .observeOn(ioScheduler)
        .flatMap {
          case (serverNamePort, actorRef) =>
            Observable.from(
              ConversationUtil.leaveRoomF(superPool, serverNamePort, raaId, cookie)
                .map((_, actorRef)).runWith(Sink.head)
            )(Implicits.global)
        }
        .observeOn(javaFxScheduler)
        .subscribe { x =>
          workingProperty.set(false)
          hBoxChatInput.buttonLeave.temporaryDisable.set(false)
        },
      // logout 을 했는데, 참가대화방에는 동작중으로 나타난다. 아래와 같이 접속상태를 나타내는 observable 를 듣는다.
      JC.toScalaObservable(JFO.fromObservableValue(cookieProperty))
        .filter {
          case Some(_) => false
          case None => true
        }
        .subscribe(_ => workingProperty.set(false))
    )

  // 아래 두개의 구독은 구독의 대상이 이 대화방 안에 있기 때문에, 대화방에 대한 참조가 없어지면 구독도 같이 없어진다.
  JC.toScalaObservable(JFO.fromObservableValue(workingProperty))
    .map(_.booleanValue())
    .scan((None.asInstanceOf[Option[Boolean]], None.asInstanceOf[Option[Boolean]])) {
      case ((_, None), false) => (None, None)
      case ((_, None), true) => (None, Some(true))
      case ((_, old @ Some(_)), false) => (old, Some(false)) // 앞의 상태가 뭐였든 false 로 바뀌면 false
      case ((_, old @ Some(false)), _) => (Some(false), Some(false)) // 현재 상태가 false 이면 무조건 false
    }
    .filter(x => x._1 != x._2)
    .map(_._2)
    .subscribe { x =>
      x match {
        case Some(false) =>
          unsubscribe()
        case _ =>
      }
    }

  // 이와 같이 web socket client actor ref stream 을 chat room 이 목록에서 지워질 때까지
  // 구독을 유지해야, web socket 이 끊긴 후 퇴장했다가 다시 연결했을 때 늦게 도착하는 대화방 관련 자료를 이어서 받을 수 있다.
  // web socket client actor 로 subject 를 등록하는 단계를 chat room 이 자신의 subject 를 구독시작하는 단계보다 뒤에 둬야
  // web socket 에서 오는 자료를 놓치는 문제를 피할 수 있다.
  val subscriptionWebSocketClientActorRef =
    JC.toScalaObservable(JFO.fromObservableValue(webSocketClientActorRef))
      .observeOn(ioScheduler)
      .subscribe(_.foreach(_.tell(PutSubject(raaId, Some(subjectWebSocketMessageSTC)), ActorRef.noSender)))

  ObservableUtil.observableAOptionBThenB(
    JC.toScalaObservable(JFO.fromActionEvents(hBoxChatInput.buttonDelete)),
    JC.toScalaObservable(JFO.fromObservableValue(webSocketClientActorRef))
  )
    .subscribe { webSocketClientActorRef =>
      tableViewItemsProperty.remove(this)
      webSocketClientActorRef.tell(RemoveSubject(raaId), ActorRef.noSender)
      subscriptionChatRoomSubject.unsubscribe()
      subscriptionWebSocketClientActorRef.unsubscribe()
    }

  // 최초의 scroll 에서는 자료가 올 때마다 바닥으로 내려가야 한다.
  // scroll 을 마우스로 움직이는 것과 scrollTo 로 움직이는 것을 구분할 방법을 모르겠다.
  // 따라서, scroll 이 반대방향으로 이루어지면 수동으로 scroll 을 시도했음을 표시하여
  // 더 이상 무조건 바닥으로 내리기를 하지 않도록 하는 것이다.
  val observableAlwaysToBottomUntilManuallyChange =
    JC.toScalaObservable(JFO.fromObservableValue(hBoxChatOutput.listViewChat.verticalScrollbar.valueProperty()))
      .scan((-1.toDouble, true)) {
        case ((oldScrollPosition, true), newScrollPosition) =>
          (newScrollPosition.doubleValue(), oldScrollPosition <= newScrollPosition.doubleValue())
        case ((_, false), newScrollPosition) =>
          (newScrollPosition.doubleValue(), false)
      }

  ObservableUtil.observableABThenB(
    JC.toScalaObservable(JFO.fromObservableListAdds(hBoxChatOutput.listViewChat.itemsProperty().get())),
    observableAlwaysToBottomUntilManuallyChange
  )
    .filter(x => x._2 || x._1 == 1) // scroll 이 거꾸로 움직인 적이 없었거나, 바닥에 있거나.
    .subscribe(scrollToBottomSubscribeF(_))

  JC.toScalaObservable(JFO.fromObservableValue(hBoxChatOutput.listViewChat.verticalScrollbar.visibleAmountProperty()))
    .subscribe(Console.err.println(_))

  getChildren.addAll(hBoxChatOutput, hBoxChatInput)
  VBox.setVgrow(hBoxChatOutput, Priority.ALWAYS)

  def unsubscribe(): Unit = listSubscription.foreach(_.unsubscribe())
}

class ChatCell extends ListCell[WebSocket.WebSocketMessageSTC] {
  override def updateItem(item: WebSocketMessageSTC, empty: Boolean): Unit = {
    super.updateItem(item, empty)
    if (empty)
      setGraphic(null)
    else {
      val result =
        item match {
          case WebSocket.RoomAnnounceSTC(_, _, s) =>
            val label = new Label(s)
            // list view 의 각 행의 높이는 다를 수 있음을 보이기 위한 예제이다.
            label.styleProperty().set("-fx-font-size:25")
            // wrap 은 효과가 없는 듯 하다.
            label.wrapTextProperty().set(true)
            label
          case WebSocket.RoomChatSTC(_, _, userId, chat) =>
            // list view 의 각 행의 내용이 다를 수 있음을 보이기 위한 예제이다.
            object hBox extends HBox {

              object labelUserId extends Label {
                prefWidthProperty().set(80)
                textProperty().set(userId)
              }

              object labelChat extends Label {
                textProperty().set(chat)
                // wrap 은 효과가 없는 듯 하다.
                wrapTextProperty().set(true)
              }

              getChildren.addAll(labelUserId, labelChat)
              HBox.setHgrow(labelChat, Priority.ALWAYS)
            }
            hBox
        }
      setGraphic(result)
    }
  }
}

class UserCell extends ListCell[(List[RoomAttenderType], String)] {
  override def updateItem(item: (List[RoomAttenderType], String), empty: Boolean): Unit = {
    super.updateItem(item, empty)
    if (empty)
      setGraphic(null)
    else {
      val userId = new Label(item._2)
      userId.paddingProperty().set(new Insets(0, 5, 0, 5))
      val hBox = new HBox(userId)
      item._1.foldLeft(hBox) {
        case (hBox, RoomOwner) =>
          val label = new Label("Ow")
          label.paddingProperty().set(new Insets(0, 5, 0, 5))
          hBox.getChildren.add(label)
          hBox
        case (hBox, RoomOperator) =>
          val label = new Label("Op")
          label.paddingProperty().set(new Insets(0, 5, 0, 5))
          hBox.getChildren.add(label)
          hBox
      }
      setGraphic(hBox)
    }
  }
}

sealed trait RoomAttenderType

// 이 기능은 client 에서만 쓴다. server 에서도 쓸 일 있으면 그 때가서 바꿔도 된다.
case object RoomOwner extends RoomAttenderType

case object RoomOperator extends RoomAttenderType

