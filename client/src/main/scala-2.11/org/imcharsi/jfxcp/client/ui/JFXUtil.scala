/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.client.ui

import javafx.beans.binding.{ BooleanBinding, IntegerBinding }
import javafx.beans.value.{ ObservableObjectValue, ObservableValue }
import javafx.scene.control.TableColumn.CellDataFeatures
import javafx.scene.control.{ TableCell, TableColumn }
import javafx.util.Callback

import rx.lang.scala.{ Observable, Subscription }

/**
 * Created by i on 8/6/16.
 */
object JFXUtil {
  def likeCombine[A, B](observableA: Observable[A], observableB: Observable[B]): Observable[Option[B]] =
    Observable.create[Option[B]] { x =>
      var state: Option[B] = None
      def onNextA(a: A): Unit = x.onNext(state)
      def onNextB(b: B): Unit = state = Some(b)
      val subscriptionA = observableA.subscribe(onNextA(_), x.onError(_), x.onCompleted)
      val subscriptionB = observableB.subscribe(onNextB(_), x.onError(_), x.onCompleted)
      Subscription {
        subscriptionA.unsubscribe()
        subscriptionB.unsubscribe()
      }
    }

  def callback[A, B](f: A => B): Callback[A, B] =
    new Callback[A, B] {
      override def call(param: A): B = f(param)
    }

  // binding 을 이렇게 할 수도 있다.
  def customBooleanBinding[A](observableValue: ObservableObjectValue[A])(f: A => Boolean): BooleanBinding =
    new BooleanBinding {
      bind(observableValue)

      override def dispose(): Unit = {
        super.dispose()
        unbind(observableValue)
      }

      override def computeValue(): Boolean = f(observableValue.get())
    }

  def customIntBinding[A](observableValue: ObservableObjectValue[A])(f: A => Int): IntegerBinding =
    new IntegerBinding {
      bind(observableValue)

      override def dispose(): Unit = {
        super.dispose()
        unbind(observableValue)
      }

      override def computeValue(): Int = f(observableValue.get())
    }

  def utilCellFactory[S, T](tableCell: TableCell[S, T]): Callback[TableColumn[S, T], TableCell[S, T]] = {
    new Callback[TableColumn[S, T], TableCell[S, T]] {
      override def call(param: TableColumn[S, T]): TableCell[S, T] = tableCell
    }
  }

  // http://stackoverflow.com/questions/29589219/javafx-tableview-column-value-dependent-on-other-columns
  // 열의 자료를 임의로 만들 때 쓴다.
  def utilCellValueFactory[S, T](f: CellDataFeatures[S, T] => ObservableValue[T]): Callback[CellDataFeatures[S, T], ObservableValue[T]] =
    new Callback[CellDataFeatures[S, T], ObservableValue[T]] {
      override def call(param: CellDataFeatures[S, T]): ObservableValue[T] = f(param)
    }

  // impo 아래에서 그냥 TableCell instance 를 인자로 받으면 제대로 동작하지 않는다.
  // 매 호출마다 새로운 TableCell 을 만들어서 써야 하는데, 이미 다 만들어진 instance 를 전달하면
  // 항상 주어진 instance 가 똑같이 사용되면서 안 되는 듯 하다.
  // scala 문법인 by-name parameter 라는 것을 써도 된다.
  def utilTableColumnFactory[S, T](f: TableColumn[S, T] => TableCell[S, T]): Callback[TableColumn[S, T], TableCell[S, T]] =
    new Callback[TableColumn[S, T], TableCell[S, T]] {
      override def call(param: TableColumn[S, T]): TableCell[S, T] = f(param)
    }

  def utilTableCell[S, T](f: (TableCell[S, T], T, Boolean) => Unit): TableCell[S, T] = {
    new TableCell[S, T] {
      override def updateItem(item: T, empty: Boolean): Unit = {
        super.updateItem(item, empty)
        f(this, item, empty)
      }
    }
  }

}
