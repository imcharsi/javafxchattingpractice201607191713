import com.typesafe.sbt.SbtScalariform.ScalariformKeys
import de.heikoseeberger.sbtheader.license.Apache2_0

import scalariform.formatter.preferences.AlignParameters

name := "JavaFXChattingPractice201607191713"

version := "1.0"

val akkaVersion = "2.4.8"

val commonSettings =
  Seq(
    scalaVersion := "2.11.8",
    libraryDependencies += "org.scala-lang" % "scala-reflect" % "2.11.8",
    libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.4",
    // https://mvnrepository.com/artifact/org.scalatest/scalatest_2.11
    libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6" % Test,
    //   https://mvnrepository.com/artifact/com.lihaoyi/fastparse_2.11
    //  libraryDependencies += "com.lihaoyi" %% "fastparse" % "0.3.7",
    // https://mvnrepository.com/artifact/io.spray/spray-json_2.11
    libraryDependencies += "io.spray" % "spray-json_2.11" % "1.3.2",
    libraryDependencies += "com.typesafe.akka" % "akka-http-spray-json-experimental_2.11" % "2.4.8",
    // https://mvnrepository.com/artifact/io.reactivex/rxjava
    libraryDependencies += "io.reactivex" % "rxjava" % "1.1.9",
    // https://mvnrepository.com/artifact/io.reactivex/rxjavafx
    libraryDependencies += "io.reactivex" % "rxjavafx" % "0.1.1",
    // https://mvnrepository.com/artifact/io.reactivex/rxscala_2.11
    libraryDependencies += "io.reactivex" %% "rxscala" % "0.26.2",
    headers := Map("scala" -> Apache2_0("2016", "Kang Woo, Lee (imcharsi@hotmail.com)"))
  ) ++
    // https://github.com/sbt/sbt-scalariform
    SbtScalariform.scalariformSettings ++
    Seq(
      // https://github.com/scala-ide/scalariform#alignparameters
      ScalariformKeys.preferences := ScalariformKeys.preferences.value.setPreference(AlignParameters, true)
    )

val akkaDependencies = Seq(
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-actor_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-testkit_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-stream_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-stream-testkit_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-http-core_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-http-core" % akkaVersion,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-http-experimental_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-http-experimental" % akkaVersion,
  libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % akkaVersion % Test,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-http-spray-json-experimental_2.11
  libraryDependencies += "com.typesafe.akka" % "akka-http-spray-json-experimental_2.11" % "2.4.8"
)

val akkaHttpSessionDependencies = Seq(
  // https://mvnrepository.com/artifact/com.softwaremill.akka-http-session/core_2.11
  libraryDependencies += "com.softwaremill.akka-http-session" %% "core" % "0.2.6"
)

lazy val commonProject =
  (project in file("common"))
    .settings(commonSettings)
    .settings(akkaDependencies)
    .enablePlugins(AutomateHeaderPlugin)

lazy val clientProject =
  (project in file("client"))
    .settings(commonSettings)
    .settings(akkaDependencies)
    .dependsOn(commonProject)
    .enablePlugins(AutomateHeaderPlugin)

lazy val serverProject =
  (project in file("server"))
    .settings(commonSettings)
    .settings(akkaDependencies)
    .settings(akkaHttpSessionDependencies)
    .dependsOn(commonProject)
    .enablePlugins(AutomateHeaderPlugin)

lazy val clientServerITProject =
  (project in file("clientServerIT"))
    .settings(commonSettings)
    .settings(akkaDependencies)
    .settings(akkaHttpSessionDependencies)
    .settings(
      // 아래 설정이 없으면 sbt 에서 실행이 되지 않는다.
      unmanagedJars in Runtime += Attributed.blank(file(System.getenv("JAVA_HOME") + "/jre/lib/ext/jfxrt.jar"))
      // 아래 설정이 없으면 sbt 에서 반복실행을 할 수 없다. 그런데 interactive 에서 set fork in run := true 를 할 수 있으므로 생략.
//      fork in run := true,
    )
    .dependsOn(commonProject, clientProject, serverProject)
    .enablePlugins(AutomateHeaderPlugin)

lazy val rootProject =
  (project in file("."))
    .settings(commonSettings)
    .aggregate(commonProject, clientProject, serverProject)
