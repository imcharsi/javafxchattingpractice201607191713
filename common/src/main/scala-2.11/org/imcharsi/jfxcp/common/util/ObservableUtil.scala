/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.util

import rx.lang.scala.Observable

/**
 * Created by i on 8/7/16.
 */
object ObservableUtil {

  def observableABOptionCThenBC[A, B, C](
    observableA: Observable[A],
    observableB: Observable[B],
    observableC: Observable[Option[C]]
  ): Observable[(B, C)] = {
    observableA.zipWithIndex
      .combineLatest(observableB)
      .combineLatest(observableC)
      .scan((None.asInstanceOf[Option[(Int, B, Option[C])]], None.asInstanceOf[Option[(Int, B, Option[C])]])) {
        case ((_, old), (((_, n), b), c)) => (old, Some((n, b, c)))
      }
      .filter {
        case (Some((oldN, _, _)), Some((newN, _, Some(_)))) => oldN != newN
        case (None, Some((_, _, Some(_)))) => true
        case _ => false
      }
      .map {
        case (_, Some((_, b, Some(c)))) => (b, c)
      }
  }

  def observableAOptionBThenB[A, B](
    observableA: Observable[A],
    observableB: Observable[Option[B]]
  ): Observable[B] = {
    observableA.zipWithIndex
      .combineLatest(observableB)
      .scan((None.asInstanceOf[Option[(Int, Option[B])]], None.asInstanceOf[Option[(Int, Option[B])]])) {
        case ((_, old), ((_, n), b)) => (old, Some((n, b)))
      }
      .filter {
        case (Some((oldN, _)), Some((newN, Some(_)))) => oldN != newN
        case (None, Some((_, Some(_)))) => true
        case _ => false
      }.map {
        case (_, Some((_, Some(b)))) => b
      }
  }

  def observableABThenB[A, B](
    observableA: Observable[A],
    observableB: Observable[B]
  ): Observable[B] =
    observableA.zipWithIndex
      .combineLatest(observableB)
      .scan((None.asInstanceOf[Option[(Int, B)]], None.asInstanceOf[Option[(Int, B)]])) {
        case ((_, old), ((_, n), b)) => (old, Some((n, b)))
      }
      .filter {
        case (None, Some((_, _))) => true
        case (Some((oldN, _)), Some((newN, _))) => oldN != newN
        case _ => false
      }
      .map {
        case (_, Some((_, b))) => b
      }

  def observableABThenAB[A, B](
    observableA: Observable[A],
    observableB: Observable[B]
  ): Observable[(A, B)] =
    observableA.zipWithIndex
      .combineLatest(observableB)
      .scan((None.asInstanceOf[Option[(A, Int, B)]], None.asInstanceOf[Option[(A, Int, B)]])) {
        case ((_, old), ((a, n), b)) => (old, Some((a, n, b)))
      }
      .filter {
        case (None, Some(_)) => true
        case (Some((_, oldN, _)), Some((_, newN, _))) => oldN != newN
        case _ => false
      }.map {
        case (_, Some((a, _, b))) => (a, b)
      }

}
