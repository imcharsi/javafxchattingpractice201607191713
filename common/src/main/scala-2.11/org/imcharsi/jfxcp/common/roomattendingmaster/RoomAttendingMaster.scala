/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.roomattendingmaster

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

/**
 * Created by i on 8/1/16.
 */
object RoomAttendingMaster extends DefaultJsonProtocol with SprayJsonSupport {

  sealed trait RoomAttendingMasterRequestCTS

  sealed trait RoomAttendingMasterResponseSTC

  case class EnterRoomCTS(roomNo: Int) extends RoomAttendingMasterRequestCTS

  case class EnteredRoomSTC(raaId: Option[Int]) extends RoomAttendingMasterResponseSTC

  implicit val parserEnterRoomCTS = jsonFormat1(EnterRoomCTS)
  implicit val parserEnteredRoomSTC = jsonFormat1(EnteredRoomSTC)

  implicit object requestParser extends RootJsonFormat[RoomAttendingMasterRequestCTS] {
    override def write(obj: RoomAttendingMasterRequestCTS): JsValue =
      obj match {
        case a @ EnterRoomCTS(_) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(EnterRoomCTS.toString())))))
      }

    override def read(json: JsValue): RoomAttendingMasterRequestCTS =
      json match {
        case JsObject(fields) if fields.get("clazz").contains(JsString(EnterRoomCTS.toString())) =>
          JsObject(fields - "clazz").convertTo[EnterRoomCTS]
        case _ =>
          deserializationError("")
      }
  }

  implicit object responseParser extends RootJsonFormat[RoomAttendingMasterResponseSTC] {
    override def write(obj: RoomAttendingMasterResponseSTC): JsValue =
      obj match {
        case a @ EnteredRoomSTC(_) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(EnteredRoomSTC.toString())))))
      }

    override def read(json: JsValue): RoomAttendingMasterResponseSTC =
      json match {
        case JsObject(fields) if fields.get("clazz").contains(JsString(EnteredRoomSTC.toString())) =>
          JsObject(fields - "clazz").convertTo[EnteredRoomSTC]
        case _ =>
          deserializationError("")
      }
  }

}
