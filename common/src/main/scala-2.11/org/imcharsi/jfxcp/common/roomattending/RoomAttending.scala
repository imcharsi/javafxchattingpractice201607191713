/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.roomattending

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

/**
 * Created by i on 8/2/16.
 */
object RoomAttending extends DefaultJsonProtocol with SprayJsonSupport {

  implicit val leavedRoomSTC = jsonFormat1(LeavedRoomSTC)
  implicit val chatCTS = jsonFormat1(ChatCTS)
  implicit val grantOperatorCTS = jsonFormat1(GrantOperatorCTS)
  implicit val grantedOperatorSTC = jsonFormat1(GrantedOperatorSTC)
  implicit val revokeOperatorCTS = jsonFormat1(RevokeOperatorCTS)
  implicit val revokedOperatorCTS = jsonFormat1(RevokedOperatorSTC)

  sealed trait RoomAttendingRequestCTS

  sealed trait RoomAttendingResponseSTC

  case object LeaveRoomCTS extends RoomAttendingRequestCTS

  case class LeavedRoomSTC(result: Boolean) extends RoomAttendingResponseSTC

  case class ChatCTS(chat: String) extends RoomAttendingRequestCTS

  case class GrantOperatorCTS(granteeId: String) extends RoomAttendingRequestCTS

  case class GrantedOperatorSTC(result: Boolean) extends RoomAttendingResponseSTC

  case class RevokeOperatorCTS(granteeId: String) extends RoomAttendingRequestCTS

  case class RevokedOperatorSTC(result: Boolean) extends RoomAttendingResponseSTC

  implicit object requestParser extends RootJsonFormat[RoomAttendingRequestCTS] {
    override def write(obj: RoomAttendingRequestCTS): JsValue = {
      obj match {
        case a @ LeaveRoomCTS =>
          JsObject(Map(("clazz", JsString(LeaveRoomCTS.toString))))
        case a @ ChatCTS(_) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(ChatCTS.toString())))))
        case a @ GrantOperatorCTS(_) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(GrantOperatorCTS.toString())))))
        case a @ RevokeOperatorCTS(_) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(RevokeOperatorCTS.toString())))))
      }
    }

    override def read(json: JsValue): RoomAttendingRequestCTS = {
      json.asJsObject.fields.get("clazz") match {
        case Some(JsString(clazz)) if clazz == LeaveRoomCTS.toString =>
          LeaveRoomCTS
        case Some(JsString(clazz)) if clazz == ChatCTS.toString =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[ChatCTS]
        case Some(JsString(clazz)) if clazz == GrantOperatorCTS.toString =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[GrantOperatorCTS]
        case Some(JsString(clazz)) if clazz == RevokeOperatorCTS.toString =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[RevokeOperatorCTS]
        case _ =>
          deserializationError("")
      }
    }
  }

  implicit object responseParser extends RootJsonFormat[RoomAttendingResponseSTC] {
    override def write(obj: RoomAttendingResponseSTC): JsValue = {
      obj match {
        case a @ LeavedRoomSTC(_) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(LeavedRoomSTC.toString())))))
        case a @ GrantedOperatorSTC(_) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(GrantedOperatorSTC.toString())))))
        case a @ RevokedOperatorSTC(_) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(RevokedOperatorSTC.toString())))))
      }
    }

    override def read(json: JsValue): RoomAttendingResponseSTC = {
      json.asJsObject.fields.get("clazz") match {
        case Some(JsString(clazz)) if clazz == LeavedRoomSTC.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[LeavedRoomSTC]
        case Some(JsString(clazz)) if clazz == GrantedOperatorSTC.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[GrantedOperatorSTC]
        case Some(JsString(clazz)) if clazz == RevokedOperatorSTC.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[RevokedOperatorSTC]
        case _ =>
          deserializationError("")
      }
    }
  }

}
