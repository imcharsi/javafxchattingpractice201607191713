/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.onetimemessage

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol._
import spray.json._

/**
 * Created by i on 7/26/16.
 */
object OneTimeMessage extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val oneTimeMessageCTO = jsonFormat2(OneTimeMessageCTO)
  implicit val scheduledOneTimeMessageOTC = jsonFormat2(ScheduledOneTimeMessageOTC)

  sealed trait OneTimeMessageRequestCTO

  sealed trait OneTimeMessageResponseOTC

  case class OneTimeMessageCTO(id: String, chat: String) extends OneTimeMessageRequestCTO

  case class ScheduledOneTimeMessageOTC(userNo: Int, msgNo: Int) extends OneTimeMessageResponseOTC

  case object FailedOneTimeMessageOTC extends OneTimeMessageResponseOTC

  implicit object OneTimeMessageRequestCTOJsonProtocol extends RootJsonFormat[OneTimeMessageRequestCTO] {
    override def read(json: JsValue): OneTimeMessageRequestCTO = {
      val fields = json.asJsObject.fields
      fields.get("clazz") match {
        case Some(JsString(s)) if s == OneTimeMessageCTO.toString() =>
          JsObject(fields - "clazz").convertTo[OneTimeMessageCTO]
        case x =>
          deserializationError(x.toString)
      }
    }

    override def write(obj: OneTimeMessageRequestCTO): JsValue = {
      obj match {
        case a @ OneTimeMessageCTO(_, _) =>
          JsObject(a.toJson.asJsObject.fields + (("clazz", JsString(OneTimeMessageCTO.toString()))))
        case x =>
          serializationError(x.getClass.toString)
      }
    }
  }

  implicit object OneTimeMessageResponseOTCJsonProtocol extends RootJsonFormat[OneTimeMessageResponseOTC] {
    override def read(json: JsValue): OneTimeMessageResponseOTC = {
      val fields = json.asJsObject.fields
      fields.get("clazz") match {
        case Some(JsString(s)) if s == ScheduledOneTimeMessageOTC.toString() =>
          JsObject(fields - "clazz").convertTo[ScheduledOneTimeMessageOTC]
        case Some(JsString(s)) if s == FailedOneTimeMessageOTC.toString() =>
          FailedOneTimeMessageOTC
        case x =>
          deserializationError(x.toString)
      }
    }

    override def write(obj: OneTimeMessageResponseOTC): JsValue = {
      obj match {
        case a @ ScheduledOneTimeMessageOTC(_, _) =>
          JsObject(a.toJson.asJsObject.fields + (("clazz", JsString(ScheduledOneTimeMessageOTC.toString()))))
        case FailedOneTimeMessageOTC =>
          JsObject(Map(("clazz", JsString(FailedOneTimeMessageOTC.toString()))))
        case x =>
          serializationError(x.getClass.toString)
      }
    }
  }

}
