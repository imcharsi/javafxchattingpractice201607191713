/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.roommaster

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

/**
 * Created by i on 8/1/16.
 */
object RoomMaster extends DefaultJsonProtocol with SprayJsonSupport {

  sealed trait RoomMasterRequestCTS

  sealed trait RoomMasterResponseSTC

  // 대화방 개설에 개설자 id 는 client 가 정하는 것이 아니고, server 에서 session-key 에 따라 정한다.
  case class CreateRoomCTS(userId: String, subject: String) extends RoomMasterRequestCTS

  case class CreatedRoomSTC(roomNo: Option[Int]) extends RoomMasterResponseSTC

  case object ListRoomCTS extends RoomMasterRequestCTS

  case class ListedRoomSTC(list: List[(Int, String, String)]) extends RoomMasterResponseSTC

  implicit val parserCreateRoomCTS = jsonFormat2(CreateRoomCTS)
  implicit val parserCreatedRoomSTC = jsonFormat1(CreatedRoomSTC)
  implicit val listedRoomSTC = jsonFormat1(ListedRoomSTC)

  implicit object requestParser extends RootJsonFormat[RoomMasterRequestCTS] {
    override def write(obj: RoomMasterRequestCTS): JsValue = {
      obj match {
        case a @ CreateRoomCTS(_, _) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(CreateRoomCTS.toString())))))
        case ListRoomCTS =>
          JsObject(Map(("clazz", JsString(ListRoomCTS.toString))))
      }
    }

    override def read(json: JsValue): RoomMasterRequestCTS = {
      json match {
        case JsObject(fields) if fields.get("clazz").contains(JsString(CreateRoomCTS.toString())) =>
          JsObject(fields - "clazz").convertTo[CreateRoomCTS]
        case JsObject(fields) if fields.get("clazz").contains(JsString(ListRoomCTS.toString)) =>
          ListRoomCTS
        case _ =>
          deserializationError("")
      }
    }
  }

  implicit object responseParser extends RootJsonFormat[RoomMasterResponseSTC] {
    override def write(obj: RoomMasterResponseSTC): JsValue = {
      obj match {
        case a @ CreatedRoomSTC(_) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(CreatedRoomSTC.toString())))))
        case a @ ListedRoomSTC(_) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(ListedRoomSTC.toString())))))
      }
    }

    override def read(json: JsValue): RoomMasterResponseSTC = {
      json match {
        case JsObject(fields) if fields.get("clazz").contains(JsString(CreatedRoomSTC.toString())) =>
          JsObject(fields - "clazz").convertTo[CreatedRoomSTC]
        case JsObject(fields) if fields.get("clazz").contains(JsString(ListedRoomSTC.toString())) =>
          JsObject(fields - "clazz").convertTo[ListedRoomSTC]
        case _ =>
          deserializationError("")
      }
    }
  }

}
