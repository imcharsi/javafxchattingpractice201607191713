/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.websocket

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol._
import spray.json._

/**
 * Created by i on 7/24/16.
 */
object WebSocket extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val oneTimeMessageSTC = jsonFormat4(OneTimeMessageSTC)
  implicit val oneTimeMessageAckSTC = jsonFormat2(OneTimeMessageAckSTC)
  implicit val transferredOneTimeMessageCTS = jsonFormat2(TransferredOneTimeMessageCTS)
  implicit val transferredOneTimeMessageAckCTS = jsonFormat2(TransferredOneTimeMessageAckCTS)
  implicit val roomAnnounceSTC = jsonFormat3(RoomAnnounceSTC)
  implicit val roomChatSTC = jsonFormat4(RoomChatSTC)
  implicit val roomStatusChangedSTC = jsonFormat2(RoomStatusChangedSTC)
  implicit val transferredRoomMessageCTS = jsonFormat2(TransferredRoomMessageCTS)

  // STC = Server To Client
  sealed trait WebSocketMessageSTC

  // CTS = Client To Server
  sealed trait WebSocketMessageCTS

  case class OneTimeMessageSTC(id: String, chat: String, userNo: Int, msgNo: Int) extends WebSocketMessageSTC

  case class OneTimeMessageAckSTC(userNo: Int, msgNo: Int) extends WebSocketMessageSTC

  case class TransferredOneTimeMessageCTS(userNo: Int, msgNo: Int) extends WebSocketMessageCTS

  case class TransferredOneTimeMessageAckCTS(userNo: Int, msgNo: Int) extends WebSocketMessageCTS

  case object GreetingFirstCTS extends WebSocketMessageCTS

  case object GreetingSecondSTC extends WebSocketMessageSTC

  case class RoomAnnounceSTC(raaId: Int, msgNo: Int, s: String) extends WebSocketMessageSTC

  case class RoomChatSTC(raaId: Int, msgNo: Int, userId: String, s: String) extends WebSocketMessageSTC

  case class RoomStatusChangedSTC(raaId: Int, msgNo: Int) extends WebSocketMessageSTC

  case class TransferredRoomMessageCTS(raaId: Int, msgNo: Int) extends WebSocketMessageCTS

  implicit object WebSocketMessageSTCJsonProtocol extends RootJsonFormat[WebSocketMessageSTC] {
    override def read(json: JsValue): WebSocketMessageSTC = {
      json.asJsObject.fields.get("clazz") match {
        case Some(JsString(s)) if s == OneTimeMessageSTC.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[OneTimeMessageSTC]
        case Some(JsString(s)) if s == OneTimeMessageAckSTC.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[OneTimeMessageAckSTC]
        case Some(JsString(s)) if s == GreetingSecondSTC.toString() =>
          GreetingSecondSTC
        case Some(JsString(s)) if s == RoomAnnounceSTC.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[RoomAnnounceSTC]
        case Some(JsString(s)) if s == RoomChatSTC.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[RoomChatSTC]
        case Some(JsString(s)) if s == RoomStatusChangedSTC.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[RoomStatusChangedSTC]
        case x =>
          deserializationError(s"clazz expected. ${x}")
      }
    }

    override def write(obj: WebSocketMessageSTC): JsValue = {
      obj match {
        case a @ OneTimeMessageSTC(_, _, _, _) =>
          JsObject(a.toJson.asJsObject.fields + (("clazz", JsString(OneTimeMessageSTC.toString()))))
        case a @ OneTimeMessageAckSTC(_, _) =>
          JsObject(a.toJson.asJsObject.fields + (("clazz", JsString(OneTimeMessageAckSTC.toString()))))
        case GreetingSecondSTC =>
          JsObject(Map(("clazz", JsString(GreetingSecondSTC.toString))))
        case a @ RoomAnnounceSTC(_, _, _) =>
          JsObject(a.toJson.asJsObject.fields + (("clazz", JsString(RoomAnnounceSTC.toString()))))
        case a @ RoomChatSTC(_, _, _, _) =>
          JsObject(a.toJson.asJsObject.fields + (("clazz", JsString(RoomChatSTC.toString()))))
        case a @ RoomStatusChangedSTC(_, _) =>
          JsObject(a.toJson.asJsObject.fields + (("clazz", JsString(RoomStatusChangedSTC.toString()))))
      }
    }
  }

  implicit object WebSocketMessageCTSJsonProtocol extends RootJsonFormat[WebSocketMessageCTS] {
    override def read(json: JsValue): WebSocketMessageCTS = {
      json.asJsObject.fields.get("clazz") match {
        case Some(JsString(s)) if s == TransferredOneTimeMessageCTS.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[TransferredOneTimeMessageCTS]
        case Some(JsString(s)) if s == TransferredOneTimeMessageAckCTS.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[TransferredOneTimeMessageAckCTS]
        case Some(JsString(s)) if s == GreetingFirstCTS.toString =>
          GreetingFirstCTS
        case Some(JsString(s)) if s == TransferredRoomMessageCTS.toString() =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[TransferredRoomMessageCTS]
        case x =>
          deserializationError(s"clazz expected. ${x}")
      }
    }

    override def write(obj: WebSocketMessageCTS): JsValue = {
      obj match {
        case a @ TransferredOneTimeMessageCTS(_, _) =>
          JsObject(a.toJson.asJsObject.fields + (("clazz", JsString(TransferredOneTimeMessageCTS.toString()))))
        case a @ TransferredOneTimeMessageAckCTS(_, _) =>
          JsObject(a.toJson.asJsObject.fields + (("clazz", JsString(TransferredOneTimeMessageAckCTS.toString()))))
        case GreetingFirstCTS =>
          JsObject(Map(("clazz", JsString(GreetingFirstCTS.toString))))
        case a @ TransferredRoomMessageCTS(_, _) =>
          JsObject(a.toJson.asJsObject.fields + (("clazz", JsString(TransferredRoomMessageCTS.toString()))))
      }
    }
  }

}
