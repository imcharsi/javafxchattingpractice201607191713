/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.util

/**
 * Created by i on 7/21/16.
 */
object State {
  type State[S, A] = S => (S, A)

  def unit[S, A](a: A): State[S, A] = (s => (s, a))

  def bind[S, A, B](f: A => State[S, B])(stateA: State[S, A]): State[S, B] =
    s => {
      val (newS, a) = stateA(s)
      f(a)(newS)
    }

  def map[S, A, B](f: A => B)(stateA: State[S, A]): State[S, B] =
    bind[S, A, B](a => unit[S, B](f(a)))(stateA)

  def apply[S, A, B](f: State[S, A => B])(stateA: State[S, A]): State[S, B] =
    bind[S, A => B, B](map(_)(stateA))(f)
}

