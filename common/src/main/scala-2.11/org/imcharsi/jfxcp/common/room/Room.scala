/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.room

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.HttpEntity.ChunkStreamPart
import akka.stream.scaladsl.Source
import akka.util.ByteString
import spray.json._

/**
 * Created by i on 8/4/16.
 */
object Room extends DefaultJsonProtocol with SprayJsonSupport {

  // 대화방 참가자 목록은 참가자만 해당 대화방의 참가자 목록을 구할 수 있는 것이 아니라
  // /room/n 경로를 거쳐서 참가자/비참가자 구분없이 누구나 참가자 목록을 구할 수 있도록 하기.
  sealed trait RoomRequestCTS

  sealed trait RoomResponseSTC

  case object GetRoomStatusCTS extends RoomRequestCTS

  // 매 대화방상태변경 때마다 대화방상태를 구하는 요청을 보내게 되는데 먼저 보낸 요청에 대한 응답이 나중에 보낸 요청에 대한 응답보다 늦게 도착할 수도 있다.
  // 그래서 먼저 도착한 더 최근의 대화방상태를 나중에 도착한 더 오래된 대화방상태가 덮어 쓰지 않도록 하려면 version 기능이 필요하다.
  case class GotRoomStatusSTC(version: Int, ownerId: String, subject: String, list: Map[String, Boolean]) extends RoomResponseSTC

  implicit val gotRoomStatusSTC = jsonFormat4(GotRoomStatusSTC)

  implicit object requestParser extends RootJsonFormat[RoomRequestCTS] {
    override def read(json: JsValue): RoomRequestCTS = {
      json.asJsObject.fields.get("clazz") match {
        case Some(JsString(clazz)) if clazz == GetRoomStatusCTS.toString => GetRoomStatusCTS
        case _ => deserializationError("")
      }
    }

    override def write(obj: RoomRequestCTS): JsValue = {
      obj match {
        case GetRoomStatusCTS => JsObject(Map(("clazz", JsString(GetRoomStatusCTS.toString))))
      }
    }
  }

  implicit object responseParser extends RootJsonFormat[RoomResponseSTC] {
    override def read(json: JsValue): RoomResponseSTC = {
      json.asJsObject.fields.get("clazz") match {
        case Some(JsString(clazz)) if clazz == GotRoomStatusSTC.toString =>
          JsObject(json.asJsObject.fields - "clazz").convertTo[GotRoomStatusSTC]
        case _ => deserializationError("")
      }
    }

    override def write(obj: RoomResponseSTC): JsValue = {
      obj match {
        case a @ GotRoomStatusSTC(_, _, _, _) =>
          JsObject(a.toJson.asJsObject.fields + ((("clazz", JsString(GotRoomStatusSTC.toString())))))
      }
    }
  }

  sealed trait GotRoomStatusSTCUtil

  case object GotRoomStatusSTCUtil1 extends GotRoomStatusSTCUtil

  case class GotRoomStatusSTCUtil2(version: Int) extends GotRoomStatusSTCUtil

  case class GotRoomStatusSTCUtil3(version: Int, ownerId: String) extends GotRoomStatusSTCUtil

  case class GotRoomStatusSTCUtil4(version: Int, ownerId: String, subject: String) extends GotRoomStatusSTCUtil

  case class GotRoomStatusSTCUtil5(version: Int, ownerId: String, subject: String, userList: Map[String, Boolean]) extends GotRoomStatusSTCUtil

  def toJson(src: GotRoomStatusSTC): Source[ByteString, _] = {
    Source.single(src.version.toJson.compactPrint)
      .concat(Source.single(src.ownerId.toJson.compactPrint))
      .concat(Source.single(src.subject.toJson.compactPrint))
      .map(ByteString(_))
      // 아래와 같이 해야 한다. ownerId 와 subject 는 string 인데, list 의 각 항목은 (string,boolean) 이라서
      // 공통의 자료형인 serializable 이 되어 json parser 가 이 인식할 수 없다.
      .concat(
        Source(src.list)
          .map(_.toJson.compactPrint)
          .map(ByteString(_))
      )
  }

  // 아래의 기능은, 검사와 client 기능에 같은 내용으로 중복되어 있어서, 여기로 뒀다.
  def fromJson(src: Source[ByteString, _]): Source[GotRoomStatusSTC, _] = {
    src.map(_.utf8String.parseJson)
      .fold(Room.GotRoomStatusSTCUtil1.asInstanceOf[Room.GotRoomStatusSTCUtil]) {
        case (Room.GotRoomStatusSTCUtil1, JsNumber(version)) =>
          Room.GotRoomStatusSTCUtil2(version.toInt)
        case (Room.GotRoomStatusSTCUtil2(version), JsString(ownerId)) =>
          Room.GotRoomStatusSTCUtil3(version, ownerId)
        case (Room.GotRoomStatusSTCUtil3(version, ownerId), JsString(subject)) =>
          Room.GotRoomStatusSTCUtil4(version, ownerId, subject)
        case (Room.GotRoomStatusSTCUtil4(version, ownerId, subject), JsArray(Seq(JsString(userId), JsBoolean(boolean)))) =>
          Room.GotRoomStatusSTCUtil5(version, ownerId, subject, Map(userId -> boolean))
        case (Room.GotRoomStatusSTCUtil5(version, ownerId, subject, userList), JsArray(Seq(JsString(userId), JsBoolean(boolean)))) =>
          Room.GotRoomStatusSTCUtil5(version, ownerId, subject, userList + (userId -> boolean))
        case _ => ???
      }
      .map {
        case Room.GotRoomStatusSTCUtil5(version, ownerId, subject, userList) =>
          Room.GotRoomStatusSTC(version, ownerId, subject, userList)
      }
  }
}
