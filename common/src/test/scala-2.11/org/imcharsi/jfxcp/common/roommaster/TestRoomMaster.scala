/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.roommaster

import org.scalatest.FunSuite
import spray.json._

import org.imcharsi.jfxcp.common.roommaster.RoomMaster._

/**
 * Created by i on 8/1/16.
 */
class TestRoomMaster extends FunSuite {
  test("CreateRoomCTS") {
    val source: RoomMaster.RoomMasterRequestCTS = CreateRoomCTS("hi", "there")
    val returned = source.toJson.prettyPrint.parseJson.convertTo[RoomMaster.RoomMasterRequestCTS]
    assertResult(source)(returned)
  }
  test("CreatedRoomSTC") {
    val source1: RoomMaster.RoomMasterResponseSTC = CreatedRoomSTC(Some(1))
    val returned1 = source1.toJson.prettyPrint.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC]
    assertResult(source1)(returned1)
    val source2: RoomMaster.RoomMasterResponseSTC = CreatedRoomSTC(None)
    val returned2 = source2.toJson.prettyPrint.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC]
    assertResult(source2)(returned2)
  }
  test("ListRoomCTS") {
    val source: RoomMaster.RoomMasterRequestCTS = ListRoomCTS
    val target = source.toJson.prettyPrint.parseJson.convertTo[RoomMaster.RoomMasterRequestCTS]
    assertResult(source)(target)
  }
  test("ListedRoomSTC") {
    val source: RoomMaster.RoomMasterResponseSTC = ListedRoomSTC(List((1, "1", "2"), (2, "2", "3"), (3, "3", "4")))
    val target = source.toJson.prettyPrint.parseJson.convertTo[RoomMaster.RoomMasterResponseSTC]
    assertResult(source)(target)
  }
}
