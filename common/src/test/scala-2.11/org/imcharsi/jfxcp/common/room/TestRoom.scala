/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.room

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import akka.testkit.TestKit
import org.imcharsi.jfxcp.common.room.Room._
import org.scalatest.FunSuiteLike
import spray.json._

import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by i on 8/5/16.
 */
class TestRoom extends TestKit(ActorSystem()) with FunSuiteLike {
  implicit val actorMaterializer = ActorMaterializer()
  test("GetRoomStatusCTS") {
    val source: Room.RoomRequestCTS = Room.GetRoomStatusCTS
    val target = source.toJson.prettyPrint.parseJson.convertTo[Room.RoomRequestCTS]
    assertResult(source)(target)
  }
  test("GotRoomStatusSTC") {
    val source: Room.RoomResponseSTC = Room.GotRoomStatusSTC(0, "1", "2", Map("3" -> false, "4" -> true, "5" -> false))
    val target = source.toJson.prettyPrint.parseJson.convertTo[Room.RoomResponseSTC]
    assertResult(source)(target)
  }
  test("toJson/fromJson") {
    val source = Room.GotRoomStatusSTC(0, "1", "2", Map("3" -> false, "4" -> true, "5" -> false))
    assertResult(source)(Await.result(fromJson(toJson(source)).runWith(Sink.head), 10.seconds))
  }

}
