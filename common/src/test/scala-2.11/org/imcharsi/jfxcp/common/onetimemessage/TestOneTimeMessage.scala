/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.onetimemessage

import org.imcharsi.jfxcp.common.onetimemessage.OneTimeMessage._
import org.scalatest.FunSuite
import spray.json._

import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 7/27/16.
 */
class TestOneTimeMessage extends FunSuite {
  test("OneTimeMessageCTO") {
    val oneTimeMessageSTC: OneTimeMessage.OneTimeMessageRequestCTO = OneTimeMessage.OneTimeMessageCTO("hi", "there")
    val parseResult = oneTimeMessageSTC.toJson.prettyPrint.parseJson.convertTo[OneTimeMessage.OneTimeMessageRequestCTO]
    assertResult(oneTimeMessageSTC)(parseResult)
  }

  test("ScheduledOneTimeMessageOTC") {
    val scheduledOneTimeMessageOTC: OneTimeMessage.OneTimeMessageResponseOTC = OneTimeMessage.ScheduledOneTimeMessageOTC(1, 2)
    val parseResult = scheduledOneTimeMessageOTC.toJson.prettyPrint.parseJson.convertTo[OneTimeMessage.OneTimeMessageResponseOTC]
    assertResult(scheduledOneTimeMessageOTC)(parseResult)

  }

  test("FailedOneTimeMessageOTC") {
    val failedOneTimeMessageOTC: OneTimeMessage.OneTimeMessageResponseOTC = OneTimeMessage.FailedOneTimeMessageOTC
    val parseResult = failedOneTimeMessageOTC.toJson.prettyPrint.parseJson.convertTo[OneTimeMessage.OneTimeMessageResponseOTC]
    assertResult(failedOneTimeMessageOTC)(parseResult)
  }

  test("several fail") {
    val oneTimeMessageResponseOTC: OneTimeMessage.OneTimeMessageResponseOTC = OneTimeMessage.ScheduledOneTimeMessageOTC(1, 2)
    Try {
      oneTimeMessageResponseOTC.toJson.prettyPrint.parseJson.convertTo[OneTimeMessage.OneTimeMessageRequestCTO]
    } match {
      case Success(_) => fail()
      case Failure(_) =>
    }
    val oneTimeMessageRequestCTO: OneTimeMessage.OneTimeMessageRequestCTO = OneTimeMessage.OneTimeMessageCTO("hi", "hi1")
    Try {
      oneTimeMessageRequestCTO.toJson.prettyPrint.parseJson.convertTo[OneTimeMessage.OneTimeMessageResponseOTC]
    } match {
      case Success(_) => fail()
      case Failure(_) =>
    }
  }

}
