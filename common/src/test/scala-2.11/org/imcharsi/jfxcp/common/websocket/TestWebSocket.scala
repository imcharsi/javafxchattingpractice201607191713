/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.websocket

import org.scalatest.FunSuite
import spray.json._

import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 7/25/16.
 */
class TestWebSocket extends FunSuite {
  test("OneTimeMessageSTC") {
    val oneTimeMessageSTC: WebSocket.WebSocketMessageSTC = WebSocket.OneTimeMessageSTC("hi", "there", 1, 2)
    val parseResult = oneTimeMessageSTC.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageSTC]
    assertResult(oneTimeMessageSTC)(parseResult)
  }

  test("OneTimeMessageAckSTC") {
    val oneTimeMessageAckSTC: WebSocket.WebSocketMessageSTC = WebSocket.OneTimeMessageAckSTC(1, 2)
    val parseResult = oneTimeMessageAckSTC.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageSTC]
    assertResult(oneTimeMessageAckSTC)(parseResult)
  }

  test("TransferredOneTimeMessageCTS") {
    val transferredOneTimeMessageCTS: WebSocket.WebSocketMessageCTS = WebSocket.TransferredOneTimeMessageCTS(1, 2)
    val parseResult = transferredOneTimeMessageCTS.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageCTS]
    assertResult(transferredOneTimeMessageCTS)(parseResult)

  }

  test("TransferredOneTimeMessageAckCTS") {
    val transferredOneTimeMessageAckCTS: WebSocket.WebSocketMessageCTS = WebSocket.TransferredOneTimeMessageAckCTS(1, 2)
    val parseResult = transferredOneTimeMessageAckCTS.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageCTS]
    assertResult(transferredOneTimeMessageAckCTS)(parseResult)
  }

  test("GreetingFirstCTS") {
    val greetingFirstCTS: WebSocket.WebSocketMessageCTS = WebSocket.GreetingFirstCTS
    val parseResult = greetingFirstCTS.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageCTS]
    assertResult(greetingFirstCTS)(parseResult)
  }

  test("GreetingSecondSTC") {
    val greetingSecondSTC: WebSocket.WebSocketMessageSTC = WebSocket.GreetingSecondSTC
    val parseResult = greetingSecondSTC.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageSTC]
    assertResult(greetingSecondSTC)(parseResult)
  }

  test("RoomAnnounceSTC") {
    val roomAnnounceSTC: WebSocket.WebSocketMessageSTC = WebSocket.RoomAnnounceSTC(1, 2, "3")
    val parseResult = roomAnnounceSTC.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageSTC]
    assertResult(roomAnnounceSTC)(parseResult)
  }

  test("RoomChatSTC") {
    val roomChatSTC: WebSocket.WebSocketMessageSTC = WebSocket.RoomChatSTC(1, 2, "3", "4")
    val parseResult = roomChatSTC.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageSTC]
    assertResult(roomChatSTC)(parseResult)
  }

  test("RoomStatusChangedSTC") {
    val roomStatusChangedSTC: WebSocket.WebSocketMessageSTC = WebSocket.RoomStatusChangedSTC(1, 2)
    val parseResult = roomStatusChangedSTC.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageSTC]
    assertResult(roomStatusChangedSTC)(parseResult)
  }

  test("TransferredRoomMessageCTS") {
    val transferredRoomMessageCTS: WebSocket.WebSocketMessageCTS = WebSocket.TransferredRoomMessageCTS(1, 2)
    val parseResult = transferredRoomMessageCTS.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageCTS]
    assertResult(transferredRoomMessageCTS)(parseResult)
  }

  test("several fail") {
    val oneTimeMessageSTC: WebSocket.WebSocketMessageSTC = WebSocket.OneTimeMessageSTC("hi", "there", 1, 2)
    Try {
      oneTimeMessageSTC.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageCTS]
    } match {
      case Success(_) => fail()
      case Failure(_) =>
    }
    val transferredOneTimeMessageCTS: WebSocket.WebSocketMessageCTS = WebSocket.TransferredOneTimeMessageCTS(1, 2)
    Try {
      transferredOneTimeMessageCTS.toJson.prettyPrint.parseJson.convertTo[WebSocket.WebSocketMessageSTC]
    } match {
      case Success(_) => fail()
      case Failure(_) =>
    }
  }

}
