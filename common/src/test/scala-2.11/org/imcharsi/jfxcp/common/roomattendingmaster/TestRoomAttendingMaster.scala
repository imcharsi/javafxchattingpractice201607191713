/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.roomattendingmaster

import org.imcharsi.jfxcp.common.roomattendingmaster.RoomAttendingMaster.EnterRoomCTS
import org.scalatest.FunSuite
import spray.json._
import RoomAttendingMaster._

/**
 * Created by i on 8/1/16.
 */
class TestRoomAttendingMaster extends FunSuite {
  test("EnterRoomCTS") {
    val source: RoomAttendingMaster.RoomAttendingMasterRequestCTS = EnterRoomCTS(1)
    val returned = source.toJson.prettyPrint.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterRequestCTS]
    assertResult(source)(returned)
  }
  test("EnteredRoomCTS") {
    val source: RoomAttendingMaster.RoomAttendingMasterResponseSTC = EnteredRoomSTC(Some(2))
    val returned = source.toJson.prettyPrint.parseJson.convertTo[RoomAttendingMaster.RoomAttendingMasterResponseSTC]
    assertResult(source)(returned)
  }
}
