/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.roomattending

import org.scalatest.FunSuite
import spray.json._
import RoomAttending._

/**
 * Created by i on 8/2/16.
 */
class TestRoomAttending extends FunSuite {
  test("LeaveRoomCTS") {
    val source: RoomAttending.RoomAttendingRequestCTS = RoomAttending.LeaveRoomCTS
    val target = source.toJson.prettyPrint.parseJson.convertTo[RoomAttending.RoomAttendingRequestCTS]
    assertResult(source)(target)
  }
  test("LeavedRoomSTC") {
    val source: RoomAttending.RoomAttendingResponseSTC = RoomAttending.LeavedRoomSTC(true)
    val target = source.toJson.prettyPrint.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC]
    assertResult(source)(target)
  }
  test("ChatCTS") {
    val source: RoomAttending.RoomAttendingRequestCTS = RoomAttending.ChatCTS("hi")
    val target = source.toJson.prettyPrint.parseJson.convertTo[RoomAttending.RoomAttendingRequestCTS]
    assertResult(source)(target)
  }
  test("GrantOperatorCTS") {
    val source: RoomAttending.RoomAttendingRequestCTS = RoomAttending.GrantOperatorCTS("hi")
    val target = source.toJson.prettyPrint.parseJson.convertTo[RoomAttending.RoomAttendingRequestCTS]
    assertResult(source)(target)
  }
  test("RevokeOperatorCTS") {
    val source: RoomAttending.RoomAttendingRequestCTS = RoomAttending.RevokeOperatorCTS("hi")
    val target = source.toJson.prettyPrint.parseJson.convertTo[RoomAttending.RoomAttendingRequestCTS]
    assertResult(source)(target)
  }
  test("GrantedOperatorSTC") {
    val source: RoomAttending.RoomAttendingResponseSTC = RoomAttending.GrantedOperatorSTC(true)
    val target = source.toJson.prettyPrint.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC]
    assertResult(source)(target)
  }
  test("RevokedOperatorSTC") {
    val source: RoomAttending.RoomAttendingResponseSTC = RoomAttending.RevokedOperatorSTC(false)
    val target = source.toJson.prettyPrint.parseJson.convertTo[RoomAttending.RoomAttendingResponseSTC]
    assertResult(source)(target)
  }
}
