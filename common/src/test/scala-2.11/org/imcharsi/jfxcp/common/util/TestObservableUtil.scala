/*
 * Copyright 2016 Kang Woo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.jfxcp.common.util

import org.scalatest.FunSuite
import rx.lang.scala.Subject
import rx.lang.scala.observers.TestSubscriber
import rx.lang.scala.subjects.BehaviorSubject
import rx.observers
import scala.reflect.runtime.universe._

/**
 * Created by i on 8/9/16.
 */
class TestObservableUtil extends FunSuite {
  val mirror = scala.reflect.runtime.currentMirror

  // 이와 같이 검사를 바꾸는 시점에서 RxJava 1.1.9 로 바뀌면서 assertValuesAndClear 라는 기능은 만들어졌지만
  // RxScala 에는 여전히 기능이 없다. 그래서 일단 reflection 으로 한다.
  def reflect[A](testSubscriber: TestSubscriber[A]): observers.TestSubscriber[A] = {
    val termSymbol =
      mirror.classSymbol(classOf[TestSubscriber[(Int, Int)]]).toType.members.filter {
        case m: TermSymbol => m.name.toString == "jTestSubscriber"
        case _ => false
      }.head.asInstanceOf[TermSymbol]
    mirror.reflect(testSubscriber)
      .reflectField(termSymbol).get
      .asInstanceOf[observers.TestSubscriber[A]]
  }

  test("observableABOptionCThenBC") {
    val subscriber = TestSubscriber[(Int, Int)]
    val jTestSubscriber = reflect(subscriber)
    val subjectA = Subject[Int]()
    val subjectB = BehaviorSubject[Int](1)
    val subjectOptionC = BehaviorSubject[Option[Int]](None)
    ObservableUtil.observableABOptionCThenBC(subjectA, subjectB, subjectOptionC)
      .subscribe(subscriber)
    subscriber.assertNoValues()
    subjectA.onNext(1)
    subscriber.assertNoValues()
    subjectOptionC.onNext(Some(1))
    subscriber.assertNoValues()
    subjectA.onNext(1)
    jTestSubscriber.assertValuesAndClear((1, 1))
    subjectB.onNext(2)
    subscriber.assertNoValues()
    subjectA.onNext(1)
    subscriber.assertValue((2, 1))
  }

  test("observableAOptionBThenB") {
    val subscriber = TestSubscriber[(Int)]
    val jTestSubscriber = reflect(subscriber)
    val subjectA = Subject[Int]()
    val subjectOptionB = BehaviorSubject[Option[Int]](None)
    ObservableUtil.observableAOptionBThenB(subjectA, subjectOptionB)
      .subscribe(subscriber)
    subscriber.assertNoValues()
    subjectA.onNext(1)
    subscriber.assertNoValues()
    subjectOptionB.onNext(Some(1))
    subscriber.assertNoValues()
    subjectA.onNext(1)
    jTestSubscriber.assertValuesAndClear(1)
    subjectOptionB.onNext(None)
    subscriber.assertNoValues()
    subjectA.onNext(1)
    subscriber.assertNoValues()
  }

  test("observableABThenB") {
    val subscriber = TestSubscriber[(Int)]
    val jTestSubscriber = reflect(subscriber)
    val subjectA = Subject[Int]()
    val subjectOptionB = BehaviorSubject[Int](1)
    ObservableUtil.observableABThenB(subjectA, subjectOptionB)
      .subscribe(subscriber)
    subscriber.assertNoValues()
    subjectA.onNext(1)
    jTestSubscriber.assertValuesAndClear(1)
    subjectOptionB.onNext(2)
    subscriber.assertNoValues()
    subjectA.onNext(1)
    subscriber.assertValue(2)
  }
  test("observableABThenAB") {
    val subscriber = TestSubscriber[(Int, Int)]
    val jTestSubscriber = reflect(subscriber)
    val subjectA = Subject[Int]()
    val subjectOptionB = BehaviorSubject[Int](1)
    ObservableUtil.observableABThenAB(subjectA, subjectOptionB)
      .subscribe(subscriber)
    subscriber.assertNoValues()
    subjectA.onNext(1)
    jTestSubscriber.assertValuesAndClear((1, 1))
    subjectOptionB.onNext(2)
    subscriber.assertNoValues()
    subjectA.onNext(2)
    subscriber.assertValue((2, 2))
  }
}
